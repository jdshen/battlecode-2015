package team027.computation;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import team027.controllers.Controller;

/**
 * Created by jdshen on 1/27/15.
 */
public class MapSize extends Computation {
    public int width;
    public int height;

    public MapSize(Controller c) {
        super(c);
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        if (limit < 500) {
            return false;
        }

        int start = Clock.getBytecodeNum();

        int minX = c.teamhq.x;
        int maxX = c.teamhq.x;
        int minY = c.teamhq.y;
        int maxY = c.teamhq.y;

        {
            MapLocation loc = c.enemyhq;
            if (loc.x < minX) {
                minX = loc.x;
            } else if (loc.x > maxX) {
                maxX = loc.x;
            }

            if (loc.y < minY) {
                minY = loc.y;
            } else if (loc.y > maxY) {
                maxY = loc.y;
            }
        }

        MapLocation[] towers = rc.senseTowerLocations();
        MapLocation[] eTowers = rc.senseEnemyTowerLocations();

        for (int i = towers.length - 1; i >= 0; i--) {
            MapLocation loc = towers[i];
            if (loc.x < minX) {
                minX = loc.x;
            } else if (loc.x > maxX) {
                maxX = loc.x;
            }

            if (loc.y < minY) {
                minY = loc.y;
            } else if (loc.y > maxY) {
                maxY = loc.y;
            }
        }

        for (int i = eTowers.length - 1; i >= 0; i--) {
            MapLocation loc = eTowers[i];
            if (loc.x < minX) {
                minX = loc.x;
            } else if (loc.x > maxX) {
                maxX = loc.x;
            }

            if (loc.y < minY) {
                minY = loc.y;
            } else if (loc.y > maxY) {
                maxY = loc.y;
            }
        }

        width = maxX - minX;
        height = maxY - minY;

        return true;
    }
}
