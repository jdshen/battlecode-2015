package team027.actors;

import battlecode.common.*;
import team027.OtherGameConstants;
import team027.controllers.Controller;
import team027.movement.Bugger;
import team027.movement.Mover;
import team027.util.AttackerLookups;

public class Miner extends Actor {
    public final double fraction;
    public final double maxOre;
    public final int maxMineRate;
    //MAGIC NUMBER - How many times better the mining rate has to be to move
    private final double MINING_IMPROVEMENT_FACTOR = 2;

    private int turnsUnsatisfied = 0; // how many turns we have not found good ore for

    //MAGIC NUMBER - number of turns to not move in a retreat direction after seeing an enemy
    private final int COWARDICE = 2;

    private GenericAttacker desperation;
    private boolean onlyDrones;

    // stuff for retreating
    public int seenEnemyCoolDown = 0;
    public Direction lastEnemyDir = Direction.OMNI;
    private Direction[] intToDirs = Direction.values();
    private Direction lastTowerDir = Direction.OMNI;

    // for bugging
    private boolean updated = false;
    private double targetOreRate;
    private boolean bugging;
    public Bugger bugger;
    private Mover mover;

    private AttackerLookups lookups;

    public Miner(Controller c) {
        super(c);
        this.lookups = new AttackerLookups();
        this.bugger = new Bugger(c);
        if (c.type == RobotType.BEAVER) {
            fraction = 1.0 / GameConstants.BEAVER_MINE_RATE;
            maxOre = GameConstants.BEAVER_MINE_MAX;
            maxMineRate = GameConstants.BEAVER_MINE_RATE;
        } else {
            fraction = 1.0/ GameConstants.MINER_MINE_RATE;
            maxOre = GameConstants.MINER_MINE_MAX;
            maxMineRate = GameConstants.MINER_MINE_RATE;
        }
        this.desperation = new GenericAttacker(c);
        this.mover = new Mover(c);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move, attack), false);
    }

    public boolean makeMove(boolean move, boolean attack) throws GameActionException {
        MapLocation here = rc.getLocation();

        RobotController rc = this.rc;
        Controller c = this.c;
        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared, c.enemy);
        if (!move) {
            //Look farther out for resource targets on off turns
            // Detect enemies
            if (enemies.length != 0) {
                if(updateLastEnemyDir(enemies)){
                    updated = false;
                    return false; //don't bother bugging when enemy is around
                }
            }
            if(!updated) {
                updateBug();
                updated = true;
            }
            return false;
        }
        updated = false;

        boolean[] canMove = getCanMove();
        //If enemies, try to run away
        if (enemies.length != 0) {
            if(updateLastEnemyDir(enemies)) {
                if(onlyDrones){
                    if(enemies.length < 2) {
                        MapLocation droneLoc = enemies[0].location;
                        if(here.distanceSquaredTo(droneLoc) <= c.attackRadiusSquared){
                            if (rc.senseNearbyRobots(droneLoc, c.attackRadiusSquared, c.team).length > 2){
                                return desperation.act(move, attack).attacked;
                            }
                        }
                    }
                }
                if (mover.makeSafeMove(lastEnemyDir.opposite(), canMove, true)) {
                    bugging = false;
                    return true;
                }
                desperation.act(move, attack);
                return true;
            }
            // if their miners and stuff update canMove for enemies
            //
            Direction[] attacked;
            RobotInfo en;
            MapLocation there;
            int idx = 0;
            int hereX = here.x;
            int hereY = here.y;
            for (int i = enemies.length - 1; i >= 0; i --){
                en = enemies[i];
                there = en.location;
                int attackRadius = en.type.attackRadiusSquared;
                attacked = lookups.getDirs(attackRadius, there.x - hereX, there.y - hereY);

                for(int j = attacked.length - 1; j >= 0; j --){
                    idx = attacked[j].ordinal();
                    canMove[idx] = false;
                }
            }
        }
        seenEnemyCoolDown = seenEnemyCoolDown - 1;

        //Check towers and hq range and canMoves

        // Mining/movement behavior
        Direction bestDir = Direction.OMNI;
        double bestOre = rc.senseOre(here);
        double bestRate = bestOre*fraction*MINING_IMPROVEMENT_FACTOR;

        if (bestRate  > maxOre) { // contains the improvement factor to discourage moving
            rc.mine();
            return true;
        }

        // search for higher rates
        // MAGIC CONSTANT = 5
        Direction next;
        for (Direction dir = Direction.NORTH; (next = dir.rotateRight()) != Direction.NORTH; dir = next) {
            if(seenEnemyCoolDown > 0){
                if(dir == lastEnemyDir){
                    continue;
                }
                if(dir == lastEnemyDir.rotateLeft()){
                    continue;
                }
                if(dir == lastEnemyDir.rotateRight()){
                    continue;
                }
            }
            MapLocation loc = here.add(dir);
            if (canMove[dir.ordinal()]) {
                double curOre = rc.senseOre(loc);
                double curRate = curOre *fraction;
                if(curRate > maxOre){
                    bestDir = dir;
                    break;
                }
                if (curRate > bestRate) {
                    bestDir = dir;
                    bestRate = curRate;
                }
            }
        }
        // We were bugging. Keep bugging unless an adjacent place is good
        if(bugging){
            // stop bugging if we found something better, or close enough
            if(bestRate >= targetOreRate || here.isAdjacentTo(bugger.to)){
                bugging = false;
                bugger.endBug();
                if(bestDir == Direction.OMNI) {
                    rc.mine();
                    return true;
                }
                //checked canMove earlier?
                rc.move(bestDir);
                return true;
            }
            // get move directions
            bestDir = bugger.bug(canMove);
            if(rc.canMove(bestDir)){
                rc.move(bestDir);
                return true;
            }
            rc.mine();
            return true;
        }

        Direction dir = bestDir;
        if (dir == Direction.OMNI) {
            if (rc.senseOre(here) < 2) {
                dir = c.teamhq.directionTo(c.enemyhq);
                for (int i = 7; i >= 0; i--) {
                    if (canMove[dir.ordinal()]) {
                        rc.move(dir);
                        return true;
                    }
                    dir = dir.rotateLeft();
                }
            }
            rc.mine();
            return true;
        } else if (canMove[dir.ordinal()]) {
            rc.move(dir);
            return true;
        } else {
            if (rc.senseOre(here) < 2) {
                dir = c.teamhq.directionTo(c.enemyhq);
                for (int i = 7; i >= 0; i--) {
                    if (canMove[dir.ordinal()]) {
                        rc.move(dir);
                        return true;
                    }
                    dir = dir.rotateLeft();
                }
            }
            rc.mine();
            return true;
        }
    }

    //Check to see if we should start bugging to some location and do so
    public void updateBug() throws GameActionException {
        //exit if already bugging

//        rc.setIndicatorString(0,"Bugging to " + bugger.to);
        if(bugging){
            return;
        }

        // localize
        RobotController rc = this.rc;
        Direction NORTH = Direction.NORTH;
        double maxOre = this.maxOre;
        double MINING_IMPROVEMENT_FACTOR = this.MINING_IMPROVEMENT_FACTOR;
        double fraction = this.fraction;

        boolean[] directionCheck = new boolean[Direction.values().length];

        if (seenEnemyCoolDown > 0) {
            Direction lastEnemyDir = this.lastEnemyDir;
            directionCheck[lastEnemyDir.ordinal()] = true;
            directionCheck[lastEnemyDir.rotateRight().ordinal()] = true;
            directionCheck[lastEnemyDir.rotateLeft().ordinal()] = true;
        }

        directionCheck[lastTowerDir.ordinal()] = true;

        //check nearby ore and see if bugging farther out is worth
        MapLocation here = rc.getLocation();
        double bestOre = rc.senseOre(here);
        double bestRate = bestOre*fraction*MINING_IMPROVEMENT_FACTOR;

        Direction next;
        boolean shouldBug = false;
        MapLocation bugLocation = here;
        for (Direction dir = NORTH; (next = dir.rotateRight()) != NORTH; dir = next) {
            if (directionCheck[dir.ordinal()]) {
                continue;
            }

            MapLocation loc = here.add(dir);
            double curOre = rc.senseOre(loc);
            double curRate = curOre *fraction;
            if(curRate > maxOre){
                shouldBug = false;
                bestOre = curOre;
                break;
            }
            if (curRate > bestRate) {
                bestRate = curRate;
                bestOre = curOre;
                shouldBug = false;
            }

            loc = loc.add(dir);
            if (rc.isLocationOccupied(loc)) {
                curOre = rc.senseOre(loc);
                curRate = curOre * fraction;
                if(curRate > maxOre){
                    curRate = maxOre;
                }
                curRate = curRate/MINING_IMPROVEMENT_FACTOR;
                if (curRate > bestRate) {
                    shouldBug = true;
                    bugLocation = loc;
                    bestRate = curRate;
                    bestOre = curOre;
                }
            }
            loc = loc.add(dir);
            if (rc.isLocationOccupied(loc)) {
                curOre = rc.senseOre(loc);
                curRate = curOre * fraction;
                if(curRate > maxOre){
                    curRate = maxOre;
                }
                curRate = curRate/MINING_IMPROVEMENT_FACTOR/MINING_IMPROVEMENT_FACTOR;
                if (curRate > bestRate) {
                    shouldBug = true;
                    bugLocation = loc;
                    bestRate = curRate;
                    bestOre = curOre;
                }
            }
        }
        // not ideal
        double oreThreshold = maxOre * maxMineRate;
        boolean done = false;

        int byteCode = Clock.getBytecodeNum();
//        rc.setIndicatorString(0,"Best ore found = " + bestOre + " compared to " + oreThreshold);
        if(bestOre < oreThreshold/2) {
            turnsUnsatisfied += 3;
            if(turnsUnsatisfied > 12){
                turnsUnsatisfied = 0;
            }
//            rc.setIndicatorString(1,"Turns unsatisfied " + turnsUnsatisfied);
            int end = 15 + turnsUnsatisfied;
            for(int i = turnsUnsatisfied; i < end; i += 2) {
                for (Direction dir = NORTH; (next = dir.rotateRight()) != NORTH; dir = next) {
                    if (directionCheck[dir.ordinal()]) {
                        continue;
                    }
                    MapLocation loc = here.add(dir, i);
                    if (rc.senseOre(loc) >= oreThreshold/2 && rc.isPathable(c.type, loc)) {
                        bugLocation = loc;
                        bestOre = rc.senseOre(loc);
                        shouldBug = true;
                        done = true;
                        turnsUnsatisfied = 0;
                        break;
                    }
                }

                if (done) {
                    break;
                }
            }
        } else{
            turnsUnsatisfied = 0;
        }
        rc.setIndicatorString(2, "The loops : " + (Clock.getBytecodeNum() - byteCode));

        if(shouldBug){ //if we should bug, start bugging
            targetOreRate = bestOre*fraction;
            if(targetOreRate > maxOre){
                targetOreRate = maxOre;
            }
            targetOreRate = targetOreRate / MINING_IMPROVEMENT_FACTOR;
            bugging = true;
            bugger.startBug(bugLocation);
        }
    }

    // returns whether we actually set a direction
    public boolean updateLastEnemyDir(RobotInfo[] enemies){
        MapLocation here = rc.getLocation();
        int dirsLength = intToDirs.length;
        int[] moveOptions = new int[dirsLength];
        Direction dir = Direction.OMNI;
        RobotInfo en;
        int idx;
        for (int i = enemies.length -1; i >= 0; i--) {
            en = enemies[i];
            switch(en.type){
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case COMPUTER:
                    continue;
                case BEAVER:
                case MINER:
                    if(here.distanceSquaredTo(en.location) > 5){
                        continue;
                    }
                    break;
                case DRONE:
                    break;
                default:
                    onlyDrones = false;
                    break;
            }
            dir = here.directionTo(en.location);
            idx = dir.ordinal();
            moveOptions[idx] = moveOptions[idx] + 3;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] + 2;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] + 2;
        }
        idx = 0;
        for (int i = dirsLength - 1; i >= 0; i--){
            if(moveOptions[i] > idx){
                idx = moveOptions[i];
                dir = intToDirs[i];
            }
        }
        if(idx == 0){ // no enemies of note
            return false;
        }

        seenEnemyCoolDown = COWARDICE;
        // if best option is 0, then return false
        lastEnemyDir = dir;
        return true;
    }

    public boolean[] getCanMove(){
        MapLocation here = rc.getLocation();
        int dirsLength = intToDirs.length;
        MapLocation tmpLoc;
        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for(int i = numTowers - 1; i >= 0; i--){
            tmpLoc = towers[i];
            if(here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE){
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers ++;
                lastTowerDir = here.directionTo(tmpLoc);
            }
        }
        boolean[] canMove = new boolean[dirsLength];

        for(int i = dirsLength - 1; i >= 0; i--){
            Direction dir = intToDirs[i];
            if(!rc.canMove(dir)){
                continue;
            }
            if(seenEnemyCoolDown > 0){ //enemy cd
                if(dir == lastEnemyDir){
                    continue;
                }
                if(dir == lastEnemyDir.rotateLeft()){
                    continue;
                }
                if(dir == lastEnemyDir.rotateRight()){
                    continue;
                }
            }
            tmpLoc = here.add(dir);
            switch (numTowers) {
                case 6:
                case 5:
                    int thereX = c.enemyhq.x;
                    int thereY = c.enemyhq.y;
                    int delX = tmpLoc.x - thereX;
                    if(delX < 0){
                        delX = -delX;
                    }
                    int delY = tmpLoc.y - thereY;
                    if(delY < 0){
                        delY = -delY;
                    }
                    if(delX < 7 && delY < 7 && (delX + delY) < 11){
                        continue;
                    }
                    break; // technically out of sight range of the hq at this point so. ...
                case 4:
                case 3:
                case 2:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                        continue;
                    }
                    break;
                default:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                        continue;
                    }
            }
            canMove[i] = true;
            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    canMove[i] = false;
                    break;
                }
            }
        }
        return canMove;
    }
}
