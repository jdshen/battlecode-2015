package team027.controllers;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import team027.actors.Action;
import team027.actors.Actor;
import team027.actors.SupplyActor;
import team027.computation.BFSWrapper;
import team027.computation.Computation;
import team027.messaging.MessageType;
import team027.messaging.MessagingConstants;
import team027.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class ComputerController extends Controller {
    public Computation comp;

    private RobotMessager in;
    private final RobotMessager out;
    private MessageType objective;
    private Actor supplyActor;

    public ComputerController(RobotController rc) {
        super(rc);
        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        out = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
        supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action;

        // TODO - group around HQ

        if (in.readMsg()) {
            MessageType msg = in.lastMsg;
            rc.setIndicatorString(0, msg +"");

            if (msg != objective) {
                switch (msg) {
                    case BFS_1:
                        comp = new BFSWrapper(this, MessagingConstants.BFS_1_CHANNEL_START, out);
                        objective = msg;
                        break;
                    case BFS_2:
                        comp = new BFSWrapper(this, MessagingConstants.BFS_2_CHANNEL_START, out);
                        objective = msg;
                        break;
                    case STOP_ACTION:
                        comp = null;
                        objective = null;
                        break;
                    default:
                        // ignore msg
                        // TODO what to do when no msg
                        break;
                }
            }

            if (objective != null && comp != null) {
                // MAGIC CONSTANT
                comp.compute(Clock.getBytecodesLeft() - 1500);
            }
        } else {
            comp = null;
            objective = null;
        }

        action = supplyActor.act(move, attack);
    }
}
