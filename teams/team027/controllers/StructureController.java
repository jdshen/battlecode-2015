package team027.controllers;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import team027.actors.Action;
import team027.actors.Actor;
import team027.actors.ObjectiveListener;
import team027.actors.StructureSupplyActor;

/**
 * Initial controller for all structures, replace with finer tuned ones
 * if necessary
 */
public class StructureController extends Controller {
    private Actor supplyActor;
    private ObjectiveListener objectives;

    public StructureController(RobotController rc) {
        super(rc);
        objectives = new ObjectiveListener(this);
        supplyActor = new StructureSupplyActor(this, 1500); // dont go over on computation
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        Action action;
        action = objectives.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
