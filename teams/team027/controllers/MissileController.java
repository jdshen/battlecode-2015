package team027.controllers;

import battlecode.common.*;
import team027.actors.Action;

/**
 * Created by jdshen on 1/6/15.
 */
public class MissileController {

    private RobotController rc;
    private Team enemy;
    private Team team;
    private int turn;


    private int lastDistance = 25;


    public MissileController(RobotController rc) {
        this.rc = rc;
        this.team = rc.getTeam();
        this.enemy = team.opponent();
        this.turn = 0;
    }

    public void run() throws GameActionException {
        RobotController rc = this.rc;
        RobotType missile = RobotType.MISSILE;
        RobotType drone = RobotType.DRONE;
        Direction omni = Direction.OMNI;
        boolean move = rc.isCoreReady();
        turn ++;
        if(!move){
            return;
        }
        MapLocation here = rc.getLocation();
        //Normal operation
        if(turn != GameConstants.MISSILE_LIFESPAN){
            RobotInfo[] enemies = rc.senseNearbyRobots(lastDistance, enemy);
            int numEnemies = enemies.length;
            Direction bestDir = omni;
            // no enemies in sight
            if(numEnemies == 0){
                RobotInfo[] allies = rc.senseNearbyRobots(8, team);
                if(allies.length == 0){
                    return;
                }
                for (int i = allies.length - 1; i >= 0; i--){
                    bestDir = allies[i].location.directionTo(here);
                    if(allies[i].type == RobotType.LAUNCHER){
                        makeMove(bestDir);
                        return;
                    }
                }
                makeMove(bestDir);
                return;
            }

            int adjCount = 0;
            MapLocation there;
            int bestDistance = lastDistance + 1;
            int currentDistance;
            RobotInfo en;
            boolean missileOnly = true;
            RobotType enType;
            int calcCount = 0;

            for (int i = numEnemies - 1; i >= 0; i--) {
                en = enemies[i];
                there = en.location;
                enType = en.type;
                if(enType == missile){
                    if(bestDir == omni){
                        bestDir = here.directionTo(there);
                    }
                    continue;
                } if (enType == drone) {
                    if(bestDir == omni || missileOnly){
                        bestDir = here.directionTo(there);
                        missileOnly = false;
                    }
                    if(here.isAdjacentTo(there)){
                        adjCount += 3;
                    }

                    continue;
                }
                currentDistance = here.distanceSquaredTo(there);
                if(currentDistance < bestDistance){
                    bestDistance = currentDistance;
                    bestDir = here.directionTo(there);
                }
                if(currentDistance <= 2){
                    adjCount += 3;
                }

                calcCount ++;
                if(calcCount > 2){
                    break;
                }
            }
            lastDistance = bestDistance;

            //Make explode decisionss
            if(adjCount > 0) {
                if(adjCount > 5){
                    rc.explode();
                }
                RobotInfo[] allies = rc.senseNearbyRobots(2, team);
                int numAllies = allies.length * 3;
                for (int i = allies.length - 1; i >= 0; i--) {
                    if(allies[i].type == RobotType.MISSILE){
                        numAllies --;
                    }
                }
                if (numAllies < adjCount){
                    rc.explode();
                    return;
                }
            }

            makeMove(bestDir);
            return;
        }

        // SPECIAL LAST TURN LOGIC
        // last turn. move to optimal location
        RobotInfo[] enemies = rc.senseNearbyRobots(8, enemy);
        int numEnemies = enemies.length;
        Direction bestDir = Direction.OMNI;
        // no enemies in sight
        if(numEnemies == 0){
            RobotInfo[] allies = rc.senseNearbyRobots(8, team);
            if(allies.length == 0){
                return;
            }
            for (int i = allies.length - 1; i >= 0; i--){
                bestDir = allies[i].location.directionTo(here);
                if(allies[i].type == RobotType.LAUNCHER){
                    makeMove(bestDir);
                    return;
                }
            }
            makeMove(bestDir);
            return;
        }
        // too many enemies to compute?
        if(numEnemies > 3){
            MapLocation there;
            for (int i = numEnemies - 1; i >= 0; i--) {
                there = enemies[i].location;
                if(enemies[i].type == RobotType.MISSILE){
                    continue;
                }
                Direction dir = here.directionTo(there);
                if (rc.canMove(dir)) {
                    rc.move(dir);
                    return;
                }

                Direction left = dir.rotateLeft();
                if (rc.canMove(left)) {
                    rc.move(left);
                    return;
                }

                Direction right = dir.rotateRight();
                if (rc.canMove(right)) {
                    rc.move(right);
                    return;
                }
            }
            return;
        }

        int adjCount = 0;
        MapLocation there;


        int northInt = Direction.NORTH.ordinal();
        int northWestInt = Direction.NORTH_WEST.ordinal();
        int northEastInt = Direction.NORTH_EAST.ordinal();
        int eastInt = Direction.EAST.ordinal();
        int westInt = Direction.WEST.ordinal();
        int southEastInt = Direction.SOUTH_EAST.ordinal();
        int southInt = Direction.SOUTH.ordinal();
        int southWestInt = Direction.SOUTH_WEST.ordinal();

        Direction[] intToDir = Direction.values();
        int numDirs = intToDir.length;
        int[] lastTurnMove = new int[numDirs];
        lastTurnMove[Direction.NONE.ordinal()] = -30;
        lastTurnMove[Direction.OMNI.ordinal()] = -30;
        RobotInfo en;

        for (int i = numEnemies - 1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            if(en.type == RobotType.MISSILE){
                continue;
            }
            int distanceX = there.x - here.x;
            int distanceY = there.y - here.y;
            switch(distanceX){
                case -2:
                    switch(distanceY){
                        case -2:
                            lastTurnMove[northWestInt] += 1;
                            continue;
                        case -1:
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[westInt] += 1;
                            continue;
                        case 0:
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[westInt] += 1;
                            lastTurnMove[southWestInt] += 1;
                            continue;
                        case 1:
                            lastTurnMove[southWestInt] += 1;
                            lastTurnMove[westInt] += 1;
                            continue;
                        case 2:
                            lastTurnMove[southWestInt] += 1;
                            continue;
                        default: continue;
                    }
                case -1:
                    switch(distanceY){
                        case -2:
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[northInt] += 1;
                            continue;
                        case -1:
                            lastTurnMove[northWestInt] = -100;
                            adjCount ++;
                            lastTurnMove[westInt] += 1;
                            lastTurnMove[northInt] += 1;
                            continue;
                        case 0:
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[westInt] = -100;
                            lastTurnMove[southWestInt] += 1;
                            adjCount ++;
                            continue;
                        case 1:
                            lastTurnMove[southWestInt] = -100;
                            lastTurnMove[westInt] += 1;
                            lastTurnMove[southInt] += 1;
                            adjCount ++;
                            continue;
                        case 2:
                            lastTurnMove[southWestInt] += 1;
                            lastTurnMove[southInt] += 1;
                            continue;
                        default: continue;
                    }
                case 0:
                    switch(distanceY){
                        case -2:
                            lastTurnMove[northInt] += 1;
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[northEastInt] += 1;
                            continue;
                        case -1:
                            lastTurnMove[northInt] = -100;
                            adjCount ++;
                            lastTurnMove[northWestInt] += 1;
                            lastTurnMove[northEastInt] += 1;
                            continue;
                        case 0:
                            continue;
                        case 1:
                            lastTurnMove[southInt] = -100;
                            lastTurnMove[southWestInt] += 1;
                            lastTurnMove[southEastInt] += 1;
                            adjCount ++;
                            continue;
                        case 2:
                            lastTurnMove[southWestInt] += 1;
                            lastTurnMove[southInt] += 1;
                            lastTurnMove[southEastInt] += 1;
                            continue;
                        default: continue;
                    }
                case 1:
                    switch(distanceY){
                        case -2:
                            lastTurnMove[northEastInt] += 1;
                            lastTurnMove[northInt] += 1;
                            continue;
                        case -1:
                            lastTurnMove[northEastInt] = -100;
                            adjCount ++;
                            lastTurnMove[eastInt] += 1;
                            lastTurnMove[northInt] += 1;
                            continue;
                        case 0:
                            lastTurnMove[northEastInt] += 1;
                            lastTurnMove[eastInt] = -100;
                            lastTurnMove[southEastInt] += 1;
                            adjCount ++;
                            continue;
                        case 1:
                            lastTurnMove[southEastInt] = -100;
                            lastTurnMove[eastInt] += 1;
                            lastTurnMove[southInt] += 1;
                            adjCount ++;
                            continue;
                        case 2:
                            lastTurnMove[southEastInt] += 1;
                            lastTurnMove[southInt] += 1;
                            continue;
                        default: continue;
                    }
                case 2:
                    switch(distanceY){
                        case -2:
                            lastTurnMove[northEastInt] += 1;
                            continue;
                        case -1:
                            lastTurnMove[northEastInt] += 1;
                            lastTurnMove[eastInt] += 1;
                            continue;
                        case 0:
                            lastTurnMove[northEastInt] += 1;
                            lastTurnMove[eastInt] += 1;
                            lastTurnMove[southEastInt] += 1;
                            continue;
                        case 1:
                            lastTurnMove[southEastInt] += 1;
                            lastTurnMove[eastInt] += 1;
                            continue;
                        case 2:
                            lastTurnMove[southEastInt] += 1;
                            continue;
                        default: continue;
                    }
                default: continue;
            }
        }
        // check directions with enemies in lastTurnMove
        Direction dir;
        for (int i = numDirs - 1; i >= 0; i--) {
            numEnemies = lastTurnMove[i];
            if (numEnemies > adjCount) {
                dir = intToDir[i];
                if(rc.canMove(dir)){
                    bestDir = dir;
                    adjCount = numEnemies;
                }
            }
        }
        // see if any greater than adjCount, if can move, take it as cur best.
        if(bestDir != Direction.OMNI){
            rc.move(bestDir);
        }
        return;
    }

    public void makeMove(Direction dir) throws GameActionException {
        if (rc.canMove(dir)) {
            rc.move(dir);
            return;
        }

        Direction left = dir.rotateLeft();
        if (rc.canMove(left)) {
            rc.move(left);
            return;
        }

        Direction right = dir.rotateRight();
        if (rc.canMove(right)) {
            rc.move(right);
            return;
        }
        if(!dir.isDiagonal()){
            left = left.rotateLeft();
            if (rc.canMove(left)) {
                rc.move(left);
                return;
            }
            right = right.rotateRight();
            if (rc.canMove(right)) {
                rc.move(right);
                return;
            }
        }
    }
}
