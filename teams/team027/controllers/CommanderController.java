package team027.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import team027.actors.Action;
import team027.actors.CommanderFighter;
import team027.actors.SupplyActor;
import team027.messaging.MessagingConstants;
import team027.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class CommanderController extends Controller {

    private CommanderFighter actor;
    private SupplyActor supplyActor;
    private RobotMessager in;

    public CommanderController(RobotController rc) {
        super(rc);
        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        this.actor = new CommanderFighter(this, this.enemyhq);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        if (in.readMsg()) {
//            actor.setTarget(in.lastLoc);
            actor.setHuntTarget(in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    actor.ignoreTower(this.teamhq);
                    actor.setAvoidTowers(true);
                    actor.setAvoidHQ(true);
                    break;
                case ASSAULT:
                    actor.ignoreTower(in.lastLoc);
                    actor.setAvoidTowers(true);
                    actor.setAvoidHQ(true);
                    break;
                case DESTROY:
                    actor.ignoreTower(this.teamhq);
                    actor.setAvoidTowers(false);
                    actor.setAvoidHQ(false);
                    break;
            }
        }

        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action = actor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;


        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
