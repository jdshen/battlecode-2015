package team027.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import team027.actors.Action;
import team027.actors.BasherActor;
import team027.actors.LauncherActor;
import team027.actors.SupplyActor;
import team027.messaging.MessagingConstants;
import team027.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class LauncherController extends Controller {
    private final SupplyActor supplyActor;
    private final RobotMessager in;
    private LauncherActor actor;

    public LauncherController(RobotController rc) {
        super(rc);
        this.actor = new LauncherActor(this, enemyhq);

        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        if (in.readMsg()) {
            actor.setTarget(in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    actor.setAlwaysBug(false);
                    break;
                case ASSAULT:
                    actor.setAlwaysBug(true);
                    break;
                case DESTROY:
                    actor.setAlwaysBug(true);
                    break;
            }
        }

        Action action = actor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved && ! action.attacked;
        attack = attack && !action.moved && ! action.attacked;
    }
}
