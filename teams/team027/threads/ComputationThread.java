package team027.threads;

import battlecode.common.*;
import team027.controllers.Controller;
import team027.messaging.MessageType;
import team027.messaging.MessagingConstants;
import team027.messaging.RobotMessager;

/**
 * Created by jdshen on 1/24/15.
 */
public class ComputationThread extends ObjectiveThread {
    private final RobotInfoManager manager;
    private MessageType message;
    private RobotMessager in;
    private RobotMessager out;

    private MapLocation building; // location of building
    private boolean connected; // connected to a beaver

    public ComputationThread(Controller c, MessageType message, RobotInfoManager manager) {
        super(c);
        this.message = message;
        this.connected = false;
        this.id = -1;
        this.manager = manager;
    }

    @Override
    public boolean run(RobotInfo info) throws GameActionException {
        if (info == null) {
            connected = false;
            return false;
        }

        // track bot for acknowledgement
        if (out.readMsg()) {
            if (out.lastMsg.ack) {
                out.clearChannel();
                return false;
            }

            // no ack, find another bot
            in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
            connected = false;
            return false;
        } else {
            // bad message, treat as no ack
            in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
            connected = false;
            return false;
        }
    }

    @Override
    public void find(boolean[] taken) throws GameActionException{
        if (connected) {
            return;
        }

        int[] bots = manager.bots[RobotType.COMPUTER.ordinal()];
        int size = manager.sizes[RobotType.COMPUTER.ordinal()];
        RobotController rc = this.rc;
        for (int i = size - 1; i >= 0; i--) {
            int id = bots[i];

            if (taken[id] || !rc.canSenseRobot(id)) {
                continue;
            }

            taken[i] = true;
            connected = true;
            this.id = id;
            in = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
            out = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
            in.broadcastMsg(message, rc.getLocation());
            return;
        }
    }

    @Override
    public void close() throws GameActionException {
        in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
        out.clearChannel();
        done = true;
    }
}
