package scrimmage;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by jdshen on 1/11/15.
 */
public class ScrimParser {
    static PrintWriter out;
    static int count = 0;
    static String lowerCutoff = "01/13/15 5:00 AM";
    static String upperCutoff = "01/14/15 12:00 AM";
    public static void main(String[] args) throws IOException, ParseException {
        File dir = new File(ScrimParser.class.getResource("").getFile());
        File outFile = new File(ScrimParser.class.getResource("").getFile() + "/script.sh");
        out = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));

        for (File file : dir.listFiles()) {
            print(file);
        }
        out.println("echo \"Matches:" + count + "\";");
        out.close();
    }

    public static void print(File file) throws IOException, ParseException {
        Document doc = Jsoup.parse(file, "UTF-8", "www.battlecode.org");
        Elements loss = doc.select(".loss-1");
        loss.addAll(doc.select(".loss-2"));
        download(loss, "L");
        Elements win = doc.select(".win-1");
        win.addAll(doc.select(".win-2"));
        download(win, "W");
    }

    public static void download(Elements rows, String flag) throws IOException, ParseException {
        String root = ScrimParser.class.getResource("").getFile();

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
        Date lower = sdf.parse(lowerCutoff);
        Date upper = sdf.parse(upperCutoff);

        for (Element row : rows) {
            Element teama  = row.select(".team-a").get(0);
            Element teamb  = row.select(".team-b").get(0);
            String teamaName = teama.select("a").get(0).text();
            String teambName = teamb.select("a").get(0).text();
            Elements elements = row.children();
            String time = elements.get(1).text().trim();
            Date date = sdf.parse(time);
            if (date.compareTo(lower) < 0 || date.compareTo(upper) > 0) {
                continue;
            }

            String ref = elements.last().children().first().attr("href");

            String maps = elements.get(elements.size()-2).text().replace(",", "_");


            int match = Integer.parseInt(ref.replaceAll("[^0-9]+", ""));

            String filename = String.format("titled/%s-%s-%s-%s-%s.rms", match, flag, teamaName, teambName, maps);
            filename = filename.replaceAll("[^0-9a-zA-Z\\./_-]", "");
            out.println(String.format("if [ -f %s.rms ]; then cp %s.rms %s; fi", match, match, filename));
            out.println(String.format("if [ ! -f %s ]; then echo \"missing %s\"; fi", filename, match, filename));
            count++;
        }
    }
}
