package attacker3_1.messaging;

import battlecode.common.*;

/**
 * Created by kevin on 1/7/15.
 */
public class MessagingConstants {
    /**
     * Max channel is 65536
     * Apx Channel Breakdown:
     *  1-8000: Global ID Channels
     *  8000-16000: Local ID Channels
     *  16000-24000: ????? more Id channels?????
     *  ??????
     *  30000: Global Channel
     *  ??????
     *  35000 - 38000 Kill channels
     *  38000 - 38700 Scout channels
     *  39000: Computation Channels
     *  40000 - 62240: BFS Direction maps (14 x 1600)
     *
     */


    //Set channel start locations
    public static final int ID_CHANNEL_START = 0;  //Start of all id-channels dedicated to i/o with specific id.


    public static final int GLOBAL_CHANNEL = 20000;
    public static final int ACTION_QUEUE_CHANNEL = GLOBAL_CHANNEL;

    public static final int KILL_CHANNEL_START = 25000; // 3000 channels for kills

    public static final int SCOUT_BLOCK_SIZE = 5;
    public static final int SCOUT_CHANNEL_START = 28000; // ~ 600 channels (120*120 / 5)

    //Start of ally computation, channels contain # allied unit. ~ 20 channels
    public static final int ALLY_COMPUTATION_CHANNEL_START = 29000;

    // channel for memorizing symmetry = 1 channel
    public static final int SYMMETRY_COMPUTATION_CHANNEL = 29100;

    public static final int BFS_1_CHANNEL_START = 30000; // reserve 16000 channels, for BFS ~ 14401 channels
    public static final int BFS_2_CHANNEL_START = 46000; // reserve 16000 channels, for BFS ~ 14401 channels

    // next channel = 62000

    //Stuff for BFS maps
    public static final int MAX_DIM = Math.max(GameConstants.MAP_MAX_HEIGHT,GameConstants.MAP_MAX_WIDTH);

    //Stuff for ID centered communication
    // Apply mod to id when getting personal channel. Primary hash
    public static final int ID_CHAN_MOD = 4001;

    //starts of sets of id-channels dedicated to input/output with specific robot id
    public static final int ID_OFFSET_GLOBAL_TO_BOT = ID_CHANNEL_START;
    public static final int ID_OFFSET_BOT_TO_GLOBAL = ID_CHANNEL_START + ID_CHAN_MOD;
    public static final int ID_OFFSET_LOCAL_TO_BOT = ID_CHANNEL_START + ID_CHAN_MOD * 2;
    public static final int ID_OFFSET_BOT_TO_LOCAL = ID_CHANNEL_START + ID_CHAN_MOD * 3;

    //secondary hash. apply to id to get identifier check passed with a broadcast message
    //must be less than ID_CHAN_MOD and relative prime to it
    public static final int ID_CHECK_MOD = 31;

    //mask to apply to messages on id-channels to get actual message/identifier check. Greater than the ID_CHECK_MOD
    public static final int ID_MSG_MASK = ID_CHECK_MOD + 1;

    //Broadcast locations are offsets from HQ location.
    public static final int LOC_MASK = 4*GameConstants.MAP_MAX_WIDTH*GameConstants.MAP_MAX_HEIGHT;

    public static int convertLoc(MapLocation loc){
        return loc.x + loc.y*2*GameConstants.MAP_MAX_WIDTH;
    }
}
