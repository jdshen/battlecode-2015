package attacker3_1.movement;

import battlecode.common.*;
import attacker3_1.DirectionOrdinals;
import attacker3_1.controllers.Controller;
import attacker3_1.messaging.DirectionMap;
import attacker3_1.messaging.MessagingConstants;
import attacker3_1.util.FastLocDirSet;
import attacker3_1.util.FastLocSet;

/**
 * Created by jdshen on 1/9/15.
 */
public class Bugger {
    public Controller c;
    public RobotController rc;

    public MapLocation start;
    public MapLocation from;
    public MapLocation cur;
    public MapLocation to;

    public boolean hugging;
    public boolean wentLeft;
    public boolean hugLeft;

    public Direction hugDir;
    public FastLocDirSet seen;
    public boolean recursed;
    public boolean bugging;
    public Mover m;

    public Direction preDir;
    public boolean useMap;
    public DirectionMap map1;
    public DirectionMap map2;

    public FastLocSet bfsSeen;

    public Bugger(Controller c) {
        m = new Mover(c);
        this.rc = c.rc;
        this.c = c;
        seen = new FastLocDirSet();
        bugging = false;
        preDir = Direction.NONE;
        map1 = new DirectionMap(c, MessagingConstants.BFS_1_CHANNEL_START);
        map2 = new DirectionMap(c, MessagingConstants.BFS_2_CHANNEL_START);
        bfsSeen = new FastLocSet();
    }

    public void endBug() {
        bugging = false;
    }

    public void startBug(MapLocation loc) {
        start = rc.getLocation();
        from = rc.getLocation();
        to = loc;
        hugging = false;
        hugLeft = true;
        wentLeft = true;
        bugging = true;
        useMap = true;
        bfsSeen.clear();
    }

    // Something went wrong during, restart the bugging.
    private void restart(MapLocation loc) {
        from = rc.getLocation();
        to = loc;
        hugging = false;
        hugLeft = true;
        seen.clear();
    }

    public boolean[] calcCanMove() {
        // localize
        Direction[] values = Direction.values();
        int length = values.length;
        boolean[] canMove = new boolean[length];
        RobotController rc = this.rc;
        for (int i = length - 1; i >= 0; i--) {
            canMove[i] = rc.canMove(values[i]);
        }
        return canMove;
    }

    public Direction bug(boolean[] canMove) throws GameActionException {
        rc.setIndicatorDot(to, 0, 255, 0);
        cur = rc.getLocation();

        // localize
        MapLocation cur = this.cur;

        if (useMap) {
            Direction dir = map1.readDirection(cur, to);
            if (dir == null) {
                dir = map2.readDirection(cur, to);
            }

            if (bfsSeen.contains(cur) || dir == Direction.OMNI) {
                useMap = false;
            } else if (dir != null && dir != Direction.NONE) {
                bfsSeen.add(cur);
                if (canMove[dir.ordinal()]) {
                    return dir;
                } else if (canMove[dir.rotateLeft().ordinal()]) {
                    return dir.rotateLeft();
                } else if (canMove[dir.rotateRight().ordinal()]) {
                    return dir.rotateRight();
                } else {
                    useMap = false; // hit enemyhq or someone in the way
                }
            }
        }

        Direction desiredDir = cur.directionTo(to);
        if (desiredDir == Direction.NONE || desiredDir == Direction.OMNI) {
            return desiredDir;
        }

        // desiredDir if possible, else left, else right. Makes dist smaller
        Direction bestDir = goInDir(canMove, desiredDir);

        if (hugging) {
            // been here before, restart hugging
            if (seen.contains(cur, hugDir)) {
                hugging = false;
            }

            if (bestDir != null) {
                if (canMove[bestDir.ordinal()] && cur.distanceSquaredTo(to) < from.distanceSquaredTo(to)) {
                    hugging = false;
                    return bestDir;
                }
            }

            seen.add(cur, hugDir);
            return hug(canMove);
        } else {
            if (bestDir != null) {
                return bestDir;
            }

            // start hugging
            seen.clear();
            hugging = true;
            from = cur;
            hugDir = desiredDir;
            hugLeft = wentLeft;
            recursed = false;
            return hug(canMove);
        }
    }

    public Direction goInDir(boolean[] canMove, Direction desiredDir){
        if (canMove[desiredDir.ordinal()]) {
            return desiredDir;
        }

        Direction left = desiredDir.rotateLeft();
        Direction right = desiredDir.rotateRight();
        boolean leftIsBetter = (cur.add(left).distanceSquaredTo(to) <
            cur.add(right).distanceSquaredTo(to));
        if (leftIsBetter) {
            if (canMove[left.ordinal()]) {
                wentLeft = true;
                return left;
            }

            if (canMove[right.ordinal()]) {
                wentLeft = false;
                return right;
            }
        } else {
            if (canMove[right.ordinal()]) {
                wentLeft = false;
                return right;
            }

            if (canMove[left.ordinal()]) {
                wentLeft = true;
                return left;
            }
        }

        return null;
    }

    private Direction handleOffMap(boolean[] canMove) {
        if (!recursed) {
            seen.clear();
            hugLeft = !hugLeft;
            recursed = true;
            return hug(canMove);
        } else {
            //something went wrong, reset
            restart(to);
            return Direction.NONE;
        }
    }

    private Direction hug(boolean[] canMove) {
        Direction tryDir;
        MapLocation tryLoc;
        int i;
        MapLocation cur = this.cur;

        if (hugLeft) {
            tryDir = hugDir.rotateLeft();
            tryLoc = cur.add(tryDir);
            for (i = 0; i < 8 && !canMove[tryDir.ordinal()]; i++) {
                if (rc.senseTerrainTile(tryLoc) == TerrainTile.OFF_MAP) {
                    return handleOffMap(canMove);
                }

                tryDir = tryDir.rotateLeft();
                tryLoc = cur.add(tryDir);
            }
        } else {
            tryDir = hugDir.rotateRight();
            tryLoc = cur.add(tryDir);
            for (i = 0; i < 8 && !canMove[tryDir.ordinal()]; i++) {
                if (rc.senseTerrainTile(tryLoc) == TerrainTile.OFF_MAP) {
                    return handleOffMap(canMove);
                }

                tryDir = tryDir.rotateRight();
                tryLoc = cur.add(tryDir);
            }
        }

        if (i == 8) {
            //blocked in all directions
            return Direction.NONE;
        } else {
            hugDir = hugLeft ?
                DirectionOrdinals.updateHugDirLeft[tryDir.ordinal()] :
                DirectionOrdinals.updateHugDirRight[tryDir.ordinal()];
            return tryDir;
        }
    }
}
