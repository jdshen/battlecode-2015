package attacker3_1.actors;

import battlecode.common.*;
import attacker3_1.controllers.Controller;
import attacker3_1.messaging.MessagingConstants;
import attacker3_1.messaging.ScoutingMessager;
import attacker3_1.movement.Mover;
import attacker3_1.util.AttackerLookups;

public class Scout extends Actor {
    private final int blockSize = MessagingConstants.SCOUT_BLOCK_SIZE;

    private final MapLocation teamhq = c.teamhq;
    private final int width = (GameConstants.MAP_MAX_WIDTH+blockSize-1)/blockSize;
    private final int height = (GameConstants.MAP_MAX_HEIGHT+blockSize-1)/blockSize;
    private final AttackerLookups lookup = new AttackerLookups();

    private Direction[] intToDir = Direction.values();

    private ScoutingMessager scouter;

    private Mover mover;

    private MapLocation target;
    private int currentTargetX;
    private int currentTargetY;

    public Scout(Controller c) {
        super(c);
        this.scouter = new ScoutingMessager(c);
        this.mover = new Mover(c);
        target = teamhq.add(intToDir[c.id % intToDir.length], 20);
        currentTargetX = (target.x / blockSize) % width;
        currentTargetY = (target.y / blockSize) % height;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }



    private boolean makeMove(boolean move) throws GameActionException {

        if (!move) {
            return false;
        }

        RobotController rc = this.rc;
        Controller c = this.c;

        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;
        int curX = (hereX / blockSize) % width;
        int curY = (hereY / blockSize) % height;

        // update scout info
        scouter.update(curX, curY);

        // update scout target if we've arrived
//        rc.setIndicatorString(0, " start of function curX and Y:" + curX +", " + curY + "  currTarget: "
//                 + currentTargetX + ", " + currentTargetY);
        if(curX == currentTargetX && curY == currentTargetY){
//            rc.setIndicatorString(2, " first target update");
            updateTarget(here, curX, curY);
        }

        int[] moveOptions = mover.getDroneInitialMoveOptions(here, true, c.teamhq);
        boolean[] trueCanMove = new boolean[moveOptions.length];
        for(int i = moveOptions.length - 1; i >= 0; i --){
            trueCanMove[i] = moveOptions[i] <= 1;
        }
        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared + 1, c.enemy);

        int numEns = enemies.length;
        //avoid enemies
        int idx = 0;
        for (int i = numEns - 1; i >= 0; i--){
            RobotInfo en = enemies[i];
            MapLocation there = en.location;
            int attackRadius = en.type.attackRadiusSquared;

            Direction[] attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
            for (int j = attacked.length - 1; j >= 0; j--) {
                idx = attacked[j].ordinal();
                moveOptions[idx] = moveOptions[idx] + 2;
                trueCanMove[idx] = false;
            }
            switch(en.type){
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case COMPUTER:
                    break;
                case MISSILE:
                    attacked = lookup.getDirs(15, there.x - hereX, there.y - hereY);
                    break;
                case BASHER:
                    attacked = lookup.getDirs(8, there.x - hereX, there.y - hereY);
                    break;
                case COMMANDER:
                    attacked = lookup.getDirs(24, there.x - hereX, there.y - hereY);
                    break;
                case LAUNCHER:
                    attacked = lookup.getDirs(15, there.x - hereX, there.y - hereY);
                default:
                    break;
            }

            for (int j = attacked.length - 1; j >= 0; j--) {
                idx = attacked[j].ordinal();
                moveOptions[idx] = moveOptions[idx] + 2;
            }
        }

        Direction dir = here.directionTo(target);
        if(mover.makeDroneSafeMove(dir, moveOptions, false)){
            return true;
        }
//        rc.setIndicatorString(2, " after making move towards target");
        if (rc.senseTerrainTile(here.add(dir)) == TerrainTile.OFF_MAP ){
            // TODO may bug out when hitting edge of map on max map size
//            rc.setIndicatorString(2, "offmap");
            scouter.forceUpdate((curX + dir.dx) % width, (curY + dir.dy) % height, 10000);
            updateTarget(here, curX, curY);
            dir = here.directionTo(target);
            if(mover.makeDroneSafeMove(dir, moveOptions, false)){
                return true;
            }
        }
//        rc.setIndicatorString(2, " units in way ");
        // we have units in the way, or enemy is in the way. mark it as visited and pick a new target
        for(int dx = -1; dx <= 1; dx ++){
            for(int dy = -1; dy <= 1; dy ++){
                scouter.update((currentTargetX + dx) % width, (currentTargetY + dy) % height);
            }
        }

        updateTarget(here, curX, curY);

        // Surrounded by enemies. Just try to move away
        if(numEns > 0){
            if(mover.makeDroneSafeMove(dir.opposite(), moveOptions, true)){
                return true;
            }
            Direction[] intToDirs = Direction.values();

            for (int i = numEns -1; i >= 0; i--) {
                RobotInfo en = enemies[i];
                MapLocation there = en.location;
                dir = here.directionTo(there);
                idx = dir.ordinal();
                moveOptions[idx] = moveOptions[idx] + 6;
                idx = dir.rotateLeft().ordinal();
                moveOptions[idx] = moveOptions[idx] + 4;
                idx = dir.rotateRight().ordinal();
                moveOptions[idx] = moveOptions[idx] + 4;
                dir = dir.opposite();
                idx = dir.opposite().ordinal();
                moveOptions[idx] = moveOptions[idx] - 4;
                idx = dir.rotateLeft().ordinal();
                moveOptions[idx] = moveOptions[idx] - 2;
                idx = dir.rotateRight().ordinal();
                moveOptions[idx] = moveOptions[idx] - 2;
            }

            dir = Direction.OMNI;
            Direction curDir;
            int best = moveOptions[dir.ordinal()];
            for(int i = moveOptions.length - 1; i>=0; i--){
                curDir = intToDirs[i];
                if(!rc.canMove(curDir)){
                    trueCanMove[i] = false; //update this with can moves
                    continue;
                }
                if(moveOptions[i] < best){
                    best = moveOptions[i];
                    dir = curDir;
                }
            }
            if(dir != Direction.OMNI){
                if(mover.makeTrueDroneSafeMove(dir, trueCanMove, moveOptions)) {
                    return true;
                }
            }

            for(int i = moveOptions.length - 1; i >= 0; i--) {

                if(trueCanMove[i]){
                    rc.move(intToDirs[i]);
                    return true;
                }
            }
        }

        return false;
    }

    // update target to an adjacent block that lowest value in visited
    private void updateTarget(MapLocation here, int curX, int curY) throws GameActionException {
//        rc.setIndicatorString(1, "updating target using " + here + " " + curX + ", " + curY);
        int best = 1000000; //larger than counter should ever be
        Direction dir; Direction bestDir = Direction.OMNI;
        int x; int y; int val;
        for (int i = intToDir.length - 1; i >= 0; i --) {
            dir = intToDir[i];
            switch(dir){
                case NONE:
                    break;
                case OMNI:
                    break;
                default:
                    x = (curX + dir.dx) % width;
                    y = (curY + dir.dy) % height;
                    val = scouter.getLastScout(x,y);
                    if(val < best){
                        best = val;
                        currentTargetX = x;
                        currentTargetY = y;
                        bestDir = dir;
                    }
            }
        }

        target = here.add(bestDir, blockSize);
//        rc.setIndicatorString(2, "new target is " + target + " " + currentTargetX + ", " + currentTargetY);
    }

    // set the target for the scout to the block containing target loc
    public void setTarget(MapLocation loc) throws GameActionException {

        MapLocation here = rc.getLocation();
        int curX = (here.x / blockSize) % width;
        int curY = (here.y / blockSize) % height;
        int newTargetX = (loc.x / blockSize) % width;
        int newTargetY = (loc.y / blockSize) % height;
        if(curX == newTargetX && curY == newTargetY){
            return;
        }
        currentTargetX = newTargetX;
        currentTargetY = newTargetX;
        target = loc;
    }



}
