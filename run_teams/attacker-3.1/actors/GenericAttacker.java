package attacker3_1.actors;

import battlecode.common.*;
import attacker3_1.OtherGameConstants;
import attacker3_1.controllers.Controller;
import attacker3_1.messaging.KillMessager;

/**
 * Created by kevin on 1/11/15.
 */
public class GenericAttacker extends Actor {

    private KillMessager kill;

    public GenericAttacker(Controller c) {
        super(c);
        kill = new KillMessager(rc);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        kill.updatePass();
        if(!attack){
            return new Action(false, false);
        }

        //ATTACK LOGIC
        //get enemies in range
        int attackRadius = c.attackRadiusSquared;
        double attackPower = c.attackPower;

        RobotInfo[] enemies = rc.senseNearbyRobots(attackRadius, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }

        // look for "best" target
        // currently (enemy-hp)/(enemy-dmg-rate) TODO better targetting
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        RobotInfo bestEnemy = enemy;

        boolean complexTarget = enemies.length <= 3;

        int myDelay = c.attackDelay;
        int turns;
        // get number of turns to kill enemy
        if(complexTarget) {
            double dmgrate = attackPower;
            RobotInfo[] allies = rc.senseNearbyRobots(enemy.location, 15, c.team);
            for (int j = allies.length - 1; j >= 0; j--) {
                RobotInfo ally = allies[j];
                RobotType atype = ally.type;
                if (ally.location.distanceSquaredTo(enemy.location) < atype.attackRadiusSquared) {
                    if(type.attackDelay < myDelay) {
                        dmgrate += type.attackPower * myDelay / type.attackDelay;
                    }
                    else if (type.attackDelay == myDelay){
                        dmgrate += type.attackPower;
                    }
                    else{
                        dmgrate += type.attackPower * type.attackDelay / myDelay;
                    }
                }
            }
            turns = (int) (enemy.health / dmgrate + .999);
        } else{
            turns = (int) (enemy.health / attackPower + .999);
        }


        // get enemy damage rate
        double damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
        double score = turns/ damageRate + getBonus(enemy);

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            if(complexTarget) {
                double dmgrate = attackPower;
                RobotInfo[] allies = rc.senseNearbyRobots(enemy.location, 15, c.team);
                for (int j = allies.length - 1; j >= 0; j--) {
                    RobotInfo ally = allies[j];
                    RobotType atype = ally.type;
                    if (ally.location.distanceSquaredTo(enemy.location) < atype.attackRadiusSquared) {
                        if(type.attackDelay < myDelay) {
                            dmgrate += type.attackPower * myDelay / type.attackDelay;
                        }
                        else if (type.attackDelay == myDelay){
                            dmgrate += type.attackPower;
                        }
                        else{
                            dmgrate += type.attackPower * type.attackDelay / myDelay;
                        }
                    }
                }
                turns = (int) (enemy.health / dmgrate + .999);
            } else{
                turns = (int) (enemy.health / attackPower + .999);
            }
            // get enemy damage rate
            damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
            double newScore = turns/damageRate + getBonus(enemy);
            //switch targets
            if(score > newScore) {
                score = newScore;
                bestEnemy = enemy;
            }
        }

        rc.attackLocation(bestEnemy.location);
        if(bestEnemy.health <= attackPower && bestEnemy.type != RobotType.MISSILE){
            kill.broadcastKill(bestEnemy.ID);
        }

        return new Action(false, true);
    }

    public MapLocation getHarrassTarget(MapLocation originalPoint, MapLocation currentPoint){
        MapLocation here = rc.getLocation();
        // decide whether to keep current point -
        //if original point, if enemy in 3x3, w/e
        // if no enemies in 5x5 centered on current point shifted 1 towards current location, relocate attack
        if (currentPoint.equals(originalPoint)){
            if(rc.senseNearbyRobots(originalPoint, 2, c.enemy).length > 0){
                return originalPoint;
            }
        } else{
            if (rc.senseNearbyRobots(currentPoint.add(currentPoint.directionTo(here)), 4, c.enemy).length > 0){
                return currentPoint;
            }
        }
        // find new point
        // find enemy in tether Set point to 2 square past enemy from where you are.
        RobotInfo[] ens = rc.senseNearbyRobots(originalPoint, 15, c.enemy);
        for (int i = ens.length - 1; i >= 0; i --){
            RobotInfo en = ens[i];
            if(en.type != RobotType.MISSILE){
                return en.location.add(here.directionTo(en.location), 2);
            }
        }

        ens = rc.senseNearbyRobots(originalPoint, 35, c.enemy);
        for (int i = ens.length - 1; i >= 0; i --){
            RobotInfo en = ens[i];
            if(en.type != RobotType.MISSILE){
                return en.location.add(here.directionTo(en.location), 2);
            }
        }

        ens = rc.senseNearbyRobots(originalPoint, OtherGameConstants.HARASS_TETHER, c.enemy);
        for (int i = ens.length - 1; i >= 0; i --){
            RobotInfo en = ens[i];
            if(en.type != RobotType.MISSILE){
                return en.location.add(here.directionTo(en.location), 2);
            }
        }
        return originalPoint;
    }

    public boolean isLocNearStructRange(MapLocation loc, MapLocation[] towers, boolean avoidHQ, MapLocation towerLoc){
        int numTowers = towers.length;
        MapLocation tmp = c.enemyhq;
        if (avoidHQ) {
            switch (numTowers){
                case 6:
                case 5:
                    int thereX = tmp.x;
                    int thereY = tmp.y;
                    int delX = loc.x - thereX;
                    if (delX < 0) {
                        delX = -delX;
                    }
                    int delY = loc.y - thereY;
                    if (delY < 0) {
                        delY = -delY;
                    }
                    if (delX < 8 && delY < 8 && (delX + delY) < 13) {
                        return true;
                    }
                    break; // technically out of sight range of the hq at this point so. ...
                case 4:
                case 3:
                case 2:
                    if (loc.distanceSquaredTo(c.enemyhq) < 49) {
                        return true;
                    }
                    break;
                default:
                    if (loc.distanceSquaredTo(c.enemyhq) <= OtherGameConstants.TOWER_SPLASH_RANGE) {
                        return true;
                    }
            }
        }

        for (int i = numTowers - 1; i >= 0; i --) {
            tmp = towers[i];
            if(tmp.equals(towerLoc)){
                continue;
            }
            if(loc.distanceSquaredTo(tmp) < OtherGameConstants.TOWER_SPLASH_RANGE){
                return true;
            }
        }
        return false;
    }

    // checks if the region in a direction for distance squares is easily traversed
    // returns true if 2/3 squares for each set of 3 are NORMAL
    // distance should be > 0
    public boolean isDirectionClear(Direction dir, int distance){
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();
        Direction left = dir.rotateLeft();
        Direction right = dir.rotateRight();
        MapLocation leftLoc = here.add(left);
        MapLocation rightLoc = here.add(right);
        switch(distance){
            case 1:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            case 2:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            case 3:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            default:
                for (int i = distance; i > 0; i --) {
                    if (rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL) {
                        if (rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL) {
                            if (rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL) {
                                return false;
                            }
                        }
                    } else {
                        if (rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL) {
                            return false;
                        }
                        if (rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL) {
                            return false;
                        }
                    }
                    here = here.add(dir);
                    leftLoc = here.add(left);
                    rightLoc = here.add(right);
                }
                return true;
        }
    }

    public int getAllyCount(int distance, MapLocation loc){
        int allyCount = 0; // Discount self, handicap towards stepping in
        RobotInfo ally;
        RobotType allyType;
        RobotInfo[] allies = rc.senseNearbyRobots(loc, distance, c.team);
        for (int i = allies.length - 1; i >= 0; i--) {
            ally = allies[i];
            allyType = ally.type;
            switch(allyType){
                case HQ:
                    break;
                case TOWER:
                    break;
                case SOLDIER:
                    allyCount += 6;
                    break;
                case BASHER:
                    allyCount += 9;
                    break;
                case DRONE:
                    allyCount += 4;
                    break;
                case TANK:
                    allyCount += 21;
                    break;
                case COMMANDER:
                    int xp = ally.xp;
                    if(xp >= GameConstants.XP_REQUIRED_LEADERSHIP) {
                        allyCount += allies.length;
                        if (xp >= GameConstants.XP_REQUIRED_HEAVY_HANDS) {
                            allyCount += 12;
                            if (xp >= GameConstants.XP_REQUIRED_IMPROVED_LEADERSHIP) {
                                allyCount += allies.length;
                            }
                        }
                    }
                    allyCount += 24;
                    break;
                case MISSILE:
                    break;
                case LAUNCHER:
                    allyCount += 30;
                    break;
                default:
                    break;
            }
        }
        return allyCount;
    }

    public int getBonus(RobotInfo info){
        return 0;
    }
}
