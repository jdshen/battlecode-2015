package attacker3_1.actors;

import battlecode.common.*;
import attacker3_1.controllers.Controller;
import attacker3_1.messaging.MessageType;
import attacker3_1.messaging.RobotMessager;
import attacker3_1.movement.Bugger;

public class Builder extends Actor {
    private final RobotMessager out;
    private Bugger bugger;
    private MapLocation loc;
    private int count;
    private RobotType structure;

    private int buildCount;

    private boolean ack;

    public Builder(
        Controller c, RobotType structure, MapLocation loc, RobotMessager out, int count
    ) {
        super(c);
        this.structure = structure;
        this.loc = loc;
        this.count = count;
        this.bugger = new Bugger(c);
        bugger.startBug(loc);
        this.out = out;
        this.ack = true;
        buildCount = 0;
    }

    public Builder(Controller c, RobotType structure, MapLocation loc, int count) {
        this(c, structure, loc, null, count);
    }

    public Builder(Controller c, RobotType structure, MapLocation loc) {
        this(c, structure, loc, 1);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        boolean moved = makeMove(move);
        if (out != null) {
            if (ack) {
                out.broadcastMsg(MessageType.ACK, rc.getLocation());
            } else {
                out.broadcastMsg(MessageType.UN_ACK, rc.getLocation());
            }
        }

        return new Action(moved, false);
    }

    public boolean makeMove(boolean move) throws GameActionException {
        if (!move) {
            return false;
        }

        if (count == 0) {
            return false;
        }

        RobotController rc = this.rc;

        int distance = rc.getLocation().distanceSquaredTo(loc);
        if (distance <= 2) {
            bugger.endBug();
            if(distance == 0){
                Direction[] values = Direction.values();
                for (int i = values.length - 1; i >= 0; i--){
                    if(rc.canMove(values[i])){
                        rc.move(values[i]);
                        return true;
                    }
                }
            }
        }

        if (bugger.bugging) {
            Direction dir = bugger.bug(bugger.calcCanMove());
            if (dir != Direction.NONE && dir != Direction.OMNI) {
                rc.move(dir);
            }

            return true;
        }


        rc.setIndicatorDot(loc, 255, 0, 0);
        Direction buildDir = rc.getLocation().directionTo(loc);
        if (rc.canBuild(buildDir, structure)) {
            buildCount = 0;
            ack = true;
            rc.build(buildDir, structure);
            count--;
            return true;
        }

        RobotInfo robotInfo = rc.senseRobotAtLocation(rc.getLocation().add(buildDir));
        if (robotInfo == null) {
            return false;
        }

        if (robotInfo.type.isBuilding || robotInfo.type == RobotType.COMPUTER) {
            ack = false;
        } else {
            buildCount++;
            // MAGIC CONSTANT
            if (buildCount > 60) {
                ack = false;
            }
        }



        return false;
    }

}
