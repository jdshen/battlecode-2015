package attacker3_1.computation;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import attacker3_1.controllers.Controller;
import attacker3_1.messaging.DirectionMap;
import attacker3_1.messaging.MessageType;
import attacker3_1.messaging.MessagingConstants;
import attacker3_1.computation.symmetry.*;
import attacker3_1.messaging.RobotMessager;

/**
 * Created by jdshen on 1/24/15.
 */
public class BFSWrapper extends Computation {
    public Symmetry symmetry;
    public int offset;
    private final RobotMessager out;
    public DirectionMap map;
    public MapLocation loc;
    public BFS bfs;

    public BFSWrapper(Controller c, int offset, RobotMessager out) throws GameActionException {
        super(c);
        this.offset = offset;
        this.out = out;
        symmetry = getSymmetry();
        this.map = new DirectionMap(c, offset);
    }

    public Symmetry getSymmetry() throws GameActionException {
        int data = rc.readBroadcast(MessagingConstants.SYMMETRY_COMPUTATION_CHANNEL);
        if (data == 0) {
            return null;
        }
        data--;

        Controller c = this.c;
        int ex = c.enemyhq.x;
        int ey = c.enemyhq.y;
        int tx = c.teamhq.x;
        int ty = c.teamhq.y;

        switch (SymmetryType.values()[data]) {
            case X_REFLECTION:
                return new XReflection(ey + ty);
            case Y_REFLECTION:
                return new YReflection(ex + tx);
            case NEG_DIAGONAL:
                return new NegDiagonalReflection(ex + ty);
            case POS_DIAGONAL:
                return new PosDiagonalReflection(ex - ty);
            case ROTATION:
                return new Rotation(ex + tx, ey + ty);
        }
        return null;
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        if (out != null) {
            out.broadcastMsg(MessageType.ACK, rc.getLocation());
        }

        // read bfs channel(s)
        MapLocation loc = map.readBFSRequest();
        if (this.loc == null || !this.loc.equals(loc)) {
            this.loc = loc;
            if (this.loc != null) {
                bfs = new BFS(c, offset, symmetry);
                bfs.add(this.loc);
            }
        }

        if (this.loc != null && bfs != null) {
            // MAGIC CONSTANT
            if (bfs.compute(limit - 200)) {
                bfs = null; // done
            }
        }

        return false;
    }
}
