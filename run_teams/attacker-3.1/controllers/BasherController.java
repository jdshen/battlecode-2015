package attacker3_1.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker3_1.actors.Action;
import attacker3_1.actors.BasherActor;
import attacker3_1.actors.SupplyActor;
import attacker3_1.messaging.MessagingConstants;
import attacker3_1.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class BasherController extends Controller {
    private final SupplyActor supplyActor;
    public BasherActor attacker;
    private RobotMessager in;

    public BasherController(RobotController rc) {
        super(rc);
        attacker = new BasherActor(this, this.enemyhq);
        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        if (in.readMsg()) {
            attacker.setTarget(in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(true);
                    break;
                case ASSAULT:
                    attacker.ignoreTower(in.lastLoc);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(false);
                    break;
                case DESTROY:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(false);
                    attacker.setAvoidEnemyRange(false);
                    break;
            }
        }

        Action action = attacker.act(move, attack);
        action = supplyActor.act(move, attack);
    }
}
