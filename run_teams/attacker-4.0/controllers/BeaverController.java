package attacker4_0.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker4_0.actors.Action;
import attacker4_0.actors.Miner;
import attacker4_0.actors.ObjectiveListener;
import attacker4_0.actors.SupplyActor;

public class BeaverController extends Controller{
    private SupplyActor supplyActor;
    private Miner miner;
    private ObjectiveListener objectives;
    private boolean firstTurn = true;
    public BeaverController(RobotController rc) {
        super(rc);
        miner = new Miner(this);
        objectives = new ObjectiveListener(this);
        supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action;

        action = objectives.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        if(firstTurn){
            firstTurn = false;
            return;
        }

        action = miner.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
