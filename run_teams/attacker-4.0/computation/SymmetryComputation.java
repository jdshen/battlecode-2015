package attacker4_0.computation;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import attacker4_0.controllers.Controller;
import attacker4_0.messaging.MessagingConstants;
import attacker4_0.computation.symmetry.*;
import attacker4_0.util.FastLocSet;

/**
 * Created by jdshen on 1/23/15.
 */
public class SymmetryComputation extends Computation {
    public Symmetry symmetry;
    public Symmetry rotation;
    public Symmetry reflection;

    // newX = x * multX + addX
    // newY = y * multY + addY

    public SymmetryComputation(Controller c) throws GameActionException {
        super(c);
        if (computeHQAndTowers()) {
            rc.broadcast(MessagingConstants.SYMMETRY_COMPUTATION_CHANNEL, 1 + symmetry.getType().ordinal());
        }
    }

    public boolean computeHQAndTowers() {
        Controller c = this.c;
        int ex = c.enemyhq.x;
        int ey = c.enemyhq.y;
        int tx = c.teamhq.x;
        int ty = c.teamhq.y;

        if (ex == tx) {
            reflection = new XReflection(ey + ty);
        } else if (ey == ty) {
            reflection = new YReflection(ex + tx);
        } else if (ex - ty == tx - ey) {
            reflection = new PosDiagonalReflection(ex - ty);
        } else if (ex + ty == tx + ey) {
            reflection = new NegDiagonalReflection(ex + ty);
        } else {
            symmetry = new Rotation(ex + tx, ey + ty);
            return true;
        }
        rotation = new Rotation(ex + tx, ey + ty);

        FastLocSet enemyTowerSet = new FastLocSet();
        MapLocation[] enemyTowers = rc.senseEnemyTowerLocations();
        for (int i = enemyTowers.length - 1; i >= 0; i--) {
            enemyTowerSet.add(enemyTowers[i]);
        }

        MapLocation[] towers = rc.senseTowerLocations();
        for (int i = towers.length - 1; i >= 0; i--) {
            if (!enemyTowerSet.contains(reflection.get(towers[i]))) {
                // is rotation
                symmetry = rotation;
                return true;
            }

            if (!enemyTowerSet.contains(rotation.get(towers[i]))) {
                // is rotation
                symmetry = reflection;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        // TODO do more than just HQ and towers
        return true;
    }

}
