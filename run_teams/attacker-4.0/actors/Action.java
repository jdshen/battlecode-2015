package attacker4_0.actors;

/**
 * Created by jdshen on 1/7/15.
 */
public class Action {
    public final boolean moved;
    public final boolean attacked;

    public Action(boolean moved, boolean attacked) {
        this.moved = moved;
        this.attacked = attacked;
    }
}
