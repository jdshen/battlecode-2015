package attacker4_0.actors;

import battlecode.common.*;
import attacker4_0.messaging.KillMessager;
import attacker4_0.movement.Mover;
import attacker4_0.util.AttackerLookups;
import attacker4_0.OtherGameConstants;
import attacker4_0.controllers.Controller;
import attacker4_0.movement.Bugger;

/**
 * Created by kevin on 1/19/15.
 */
public class SoldierFighter extends Actor {
    private Bugger bugger;
    private Mover mover;
    private AttackerLookups lookups;
    private Direction[] intToDirs = Direction.values();
    private int commanderXP = 0;

    private KillMessager kill;
    private GenericAttacker attacker;

    private MapLocation target;
    // the original target of a harass command
    private MapLocation harassTarget;

    private boolean avoidTowers;
    private boolean avoidHQ;
    private MapLocation towerLoc;

    private boolean harass;
    private boolean reachedHarass = false;

    private boolean engage = false;

    public SoldierFighter(Controller c, MapLocation target) {
        super(c);
        this.kill = new KillMessager(rc);
        this.bugger = new Bugger(c);
        this.lookups = new AttackerLookups();
        this.target = target;
        this.harassTarget = target;
        this.harass = false;

        this.mover = new Mover(c);
        bugger.startBug(target);
        this.avoidTowers = true;
        this.avoidHQ = true;
        this.towerLoc = c.teamhq;
        this.attacker = new GenericAttacker(c);
    }

    public void setTarget(MapLocation target) {
        harass = false;
        if (!this.target.equals(target)) {
            this.target = target;
            bugger.startBug(target);
        }
    }

    public void setHarassTarget(MapLocation target) {
        harass = true;
        this.harassTarget = target;
        if (!this.target.equals(target)) {
            this.target = target;
            bugger.startBug(target);
            reachedHarass = false;
        }
    }

    public void setAvoidTowers(boolean avoidTowers) {
        this.avoidTowers = avoidTowers;
    }

    public void setAvoidHQ(boolean avoidHQ) {
        this.avoidHQ = avoidHQ;
    }

    public void ignoreTower(MapLocation towerLoc) {
        this.towerLoc = towerLoc;
    }


    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        kill.updatePass();

        if(!move && ! attack){
            return new Action(false, false);
        }

        MapLocation here = rc.getLocation();
        if(harass){
            if(!reachedHarass){
                if(here.distanceSquaredTo(harassTarget) <= 10){
                    reachedHarass = true;
                }
            }
        }
        RobotInfo[] enemies = rc.senseNearbyRobots(29, c.enemy);
        if (enemies.length != 0) {
            return actEnemies(move, attack, enemies);
        }
        engage = false;

        if(reachedHarass) {
            MapLocation newTarget = attacker.getHarrassTarget(harassTarget, target);

            if(!newTarget.equals(target)){
                target = newTarget;
                bugger.startBug(target);
            }
        }

        if(!move){
            return new Action(false, false);
        }
        // if harassing, stop chasing and go towards harass point


        // if no enemies, bug
        if(here.distanceSquaredTo(target) <= 5){
            bugger.endBug();
            return new Action(false,false);
        }
        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(mover.getCanMove(avoidTowers, towerLoc, avoidHQ));
        if(rc.canMove(dir)){
            rc.move(dir);
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actEnemies(boolean move, boolean attack,  RobotInfo[] enemies) throws GameActionException {
        // get damage in direction array
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;

        // Measure damage in each direction and get an estimate of number of enemies where 1 soldier = 6
        int[] damageInDir = new int[intToDirs.length];
        int numEnemies = enemies.length;
        RobotInfo enemy;
        RobotType enType;
        MapLocation enLoc;
        int tmp;
        int damage;
        int enemyCount = 0;
        Direction[] attackedDirs;
        boolean[] missileDirs = new boolean[intToDirs.length];
        for (int i = numEnemies - 1; i >= 0; i--) {
            enemy = enemies[i];
            enLoc = enemy.location;
            enType = enemy.type;
            tmp = enType.attackRadiusSquared;
            boolean adjbasher = false;
            switch(enType){
                case HQ:
                    damage = 6;
                    enemyCount += 24;
                    break;
                case TOWER:
                    damage = 4;
                    enemyCount += 12;
                    break;
                case SOLDIER:
                    damage = 1;
                    enemyCount += 6;
                    break;
                case BASHER:
                    damage = 1;
                    adjbasher = here.isAdjacentTo(enLoc);
                    enemyCount += 9;
                    break;
                case DRONE:
                    damage = 1;
                    enemyCount += 8;
                    break;
                case TANK:
                    damage = 2;
                    enemyCount += 21;
                    break;
                case COMMANDER:
                    damage = 2;
                    commanderXP = enemy.xp;
                    if(commanderXP >= GameConstants.XP_REQUIRED_LEADERSHIP) {
                        enemyCount += numEnemies;
                        if (commanderXP >= GameConstants.XP_REQUIRED_HEAVY_HANDS) {
                            damage = 6;
                            enemyCount += 12;
                            if (commanderXP >= GameConstants.XP_REQUIRED_IMPROVED_LEADERSHIP) {
                                enemyCount += numEnemies;
                            }
                        }
                    }
                    enemyCount += 24;
                    break;
                case MISSILE:
                    damage = 5;
                    break;
                case LAUNCHER:
                    damage = 1;
                    enemyCount += 36;
                    tmp = 24;
                    break;
                case MINER:
                case BEAVER:
                    damage = 0;
                    enemyCount += 2;
                    break;
                default:
                    damage = 0;
                    break;
            }
            if(tmp < 8 && ! adjbasher){
                attackedDirs = lookups.getDirs(5, enLoc.x - hereX, enLoc.y - hereY);
            }
            else{
                attackedDirs = lookups.getDirs(tmp, enLoc.x - hereX, enLoc.y - hereY);
            }
            if(enType == RobotType.MISSILE || enType == RobotType.BASHER){
                for (int j = attackedDirs.length - 1; j >= 0; j--){
                    tmp = attackedDirs[j].ordinal();
                    missileDirs[tmp] = true;
                    damageInDir[tmp] = damageInDir[tmp] + damage;
                }
            }
            else{
                for (int j = attackedDirs.length - 1; j >= 0; j--){
                    tmp = attackedDirs[j].ordinal();
                    damageInDir[tmp] = damageInDir[tmp] + damage;
                }
            }
        }

        // account for buildings/miners
        RobotInfo[] enInRange = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        if(enInRange.length != 0){
            return actFight(move, attack, damageInDir, enInRange, enemyCount);
        }

        // TODO if harassing, change target
//        if (harass) {
//            if(here.distanceSquaredTo())
//        }

        if(!move){
            if(reachedHarass) {
                MapLocation newTarget = attacker.getHarrassTarget(harassTarget, target);

                if(!newTarget.equals(target)){
                    target = newTarget;
                    bugger.startBug(target);
                }
            }
            return new Action(false, false);
        }
        // if outside attack range
        tmp = Direction.OMNI.ordinal();
        if(damageInDir[tmp] == 0){
            return actStepIn(damageInDir, enemyCount, missileDirs);
        }


        return actInEnemyRange(damageInDir, enemyCount, missileDirs);
    }

    // Assume we can move.
    // damageInDir gives weighted dmg (num soldiers) in a direction
    // enemyCount is weighted number of enemies (soldier = 6)
    // counts commander effects if commander seen
    private Action actStepIn(int[] damageInDir, int enemyCount, boolean[] missileDirs) throws GameActionException {
        // check if we are willing to step into enemy range
        engage = false;
        MapLocation here = rc.getLocation();
        MapLocation[] towers = rc.senseEnemyTowerLocations();
        MapLocation[] needCheckTowers = mover.getNeedCheckTowers(towerLoc, towers);
        boolean[] canMove = mover.getCanMove(avoidTowers, needCheckTowers, avoidHQ, mover.numCheckTowers);


        int allyCount = attacker.getAllyCount(10, here); // TODO MAGIC CONSTANT

        if(allyCount > enemyCount){
            // MAGIC CONSTANT on how far
            Direction toTarget = here.directionTo((target));
            if(attacker.isDirectionClear(toTarget , 3)){
                engage = true;
                if (avoidTowers) {
                    MapLocation next = here.add(toTarget);
                    for (int i = 0; i < 3; i ++) {
                        if (attacker.isLocNearStructRange(next, towers, avoidHQ, towerLoc)){
                            engage = false;
                            break;
                        }
                        next = here.add(toTarget);
                    }
                }
            }
        }

        // Don't step in
        if(!engage){
            for(int i = intToDirs.length - 1; i >= 0; i--){
                if(damageInDir[i] > 0){
                    canMove[i] = false;
                }
            }
        } else{
            for (int i = intToDirs.length - 1; i >= 0; i--){
                if(missileDirs[i]){
                    canMove[i] = false;
                }
            }
        }

        rc.setIndicatorString(0, "Enemy Count : " + enemyCount );
        rc.setIndicatorString(1, "Ally Count : " + allyCount);
        rc.setIndicatorString(2, "Target : " + target);

        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(canMove);
        if(rc.canMove(dir)){
            rc.move(dir);
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actInEnemyRange(int[] damageInDir, int enemyCount, boolean[] missileDirs) throws GameActionException {
        // if in enemy attack range - decide retreat or fight
        MapLocation here = rc.getLocation();
        MapLocation[] towers = rc.senseEnemyTowerLocations();
        MapLocation[] needCheckTowers = mover.getNeedCheckTowers(towerLoc, towers);
        boolean[] canMove = mover.getCanMove(avoidTowers, needCheckTowers, avoidHQ, mover.numCheckTowers);


        int allyCount = attacker.getAllyCount(24, here);
        // TODO use engage and the check path clear
        if(!engage){
          // tank/commander walked at us. Check if we want to engage
            if(allyCount > enemyCount){
                Direction toTarget = here.directionTo((target));
                if(attacker.isDirectionClear(toTarget , 2)){
                    engage = true;
                    if (avoidTowers) {
                        MapLocation next = here.add(toTarget);
                        for (int i = 0; i < 2; i ++) {
                            if (attacker.isLocNearStructRange(next, towers, avoidHQ, towerLoc)){
                                engage = false;
                                break;
                            }
                            next = here.add(toTarget);
                        }
                    }
                }
            }
        }

        rc.setIndicatorString(0, "Enemy Count : " + enemyCount );
        rc.setIndicatorString(1, "Ally Count : " + allyCount);
        rc.setIndicatorString(2, "Target : " + target);

        if(!engage) {
            for (int i = intToDirs.length - 1; i >= 0; i--) {
                if (damageInDir[i] > 0) {
                    canMove[i] = false;
                }
            }
        } else {
            for (int i = intToDirs.length - 1; i >= 0; i--) {
                if(missileDirs[i]){
                    canMove[i] = false;
                }
            }
        }

        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(canMove);
        if(rc.canMove(dir)) {
            rc.move(dir);
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actFight(boolean move, boolean attack, int[] damageInDir, RobotInfo[] enInRange, int enemyCount) throws GameActionException {
        engage = false;
        // if fight decide moving in deeper or attacking
        //    TODO should move in deeper if there is no additional row of enemies
        //    and we have a second row. should only consider if < 4 enemies
        // back up if our best attack option is worse than number of soldiers that can hit me
        int numEns = enInRange.length;
        RobotInfo enemy = enInRange[numEns-1];
        RobotType enType = enemy.type;
        MapLocation enLoc = enemy.location;
        RobotInfo bestEnemy = enemy;
        Team myTeam = c.team;
        double myDamage = c.attackPower;
        int myAttackRadius = c.attackRadiusSquared;

        //TODO consider enemy commander
        //TODO improve "our dmg" calculation
        int numAllies = rc.senseNearbyRobots(enLoc, myAttackRadius, myTeam).length + 1;
        int maxAllies = numAllies;
        int turns = (int) ((enemy.health / myDamage) / numAllies + 0.99999);
        double damageRate = enType.attackPower / (0.01 + enType.attackDelay);
        double score = damageRate / turns;
        double newScore;
        int bestTurns = turns;
        for (int i = numEns - 2; i >= 0; i--) {
            enemy = enInRange[i];
            enType = enemy.type;
            enLoc = enemy.location;
            numAllies = rc.senseNearbyRobots(enLoc, myAttackRadius, myTeam).length + 1;
            if(numAllies > maxAllies){
                maxAllies = numAllies;
            }
            turns = (int) (((enemy.health / myDamage) / numAllies) + 0.9999);
            damageRate = enType.attackPower / (0.01 + enType.attackDelay);
            newScore = damageRate / turns;
            if(score < newScore) {
                score = newScore;
                bestEnemy = enemy;
                bestTurns = turns;
            }
        }
        if(attack){
            // take kill shots
            if(bestEnemy.health <= myDamage && bestEnemy.type != RobotType.MISSILE){
                rc.attackLocation(bestEnemy.location);
                kill.broadcastKill(bestEnemy.ID);
                return new Action(false, true);
            }
        }

        double myHealth = rc.getHealth() / 4;
        // reposition?
        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);
        MapLocation here = rc.getLocation();
        Direction dir = here.directionTo(bestEnemy.location);
        Direction behind = dir.opposite();
        Direction behindLeft = behind.rotateLeft();
        Direction behindRight = behind.rotateRight();
        int curDmg = damageInDir[Direction.OMNI.ordinal()];
        int myturns = (int) (myHealth/(0.01 + curDmg) + 0.999);

        if(!move){ //penalty to trying to reposition
            myHealth = myHealth - curDmg;
        }
        int tmpDmg;
        //backup
        if(maxAllies <= 2){
            // Code to force retreat in micro situation
            if(myturns < bestTurns){
                maxAllies = 0;
            }
        }

        // backup
        if(maxAllies < numEns && maxAllies < curDmg){
            if(backUp(canMove, damageInDir, curDmg, myHealth, behind)){
                if(move){
                    rc.move(behind);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
            if(backUp(canMove, damageInDir, curDmg, myHealth, behindLeft)){
                if(move){
                    rc.move(behindLeft);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
            if(backUp(canMove, damageInDir, curDmg, myHealth, behindRight)){
                if(move){
                    rc.move(behindRight);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
        }
        //stepin
        if(bestTurns > 3 && !here.isAdjacentTo(bestEnemy.location) && maxAllies >=3){
            Direction dirLeft = dir.rotateLeft();
            Direction dirRight = dir.rotateRight();
            int idx = dir.ordinal();
            tmpDmg = damageInDir[idx];
            boolean stepin = false;
            if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                stepin = true;
            }else{
                idx = dirLeft.ordinal();
                tmpDmg = damageInDir[idx];
                if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                    dir = dirLeft;
                    stepin = true;
                }else{
                    idx = dirRight.ordinal();
                    tmpDmg = damageInDir[idx];
                    if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                        dir = dirRight;
                        stepin = true;
                    }
                }
            }
            if(stepin) {
                numAllies = 0;
                RobotInfo ally = rc.senseRobotAtLocation(here.add(behind));
                if (ally != null && ally.team == myTeam) {
                    numAllies++;
                }
                ally = rc.senseRobotAtLocation(here.add(behindLeft));
                if (ally != null && ally.team == myTeam) {
                    numAllies++;
                }
                ally = rc.senseRobotAtLocation(here.add(behindRight));
                if (ally != null && ally.team == myTeam) {
                    numAllies++;
                }
                if (numAllies > 1){
                    if(move){
                        rc.move(dir);
                        bugger.endBug();
                        return new Action(true, false);
                    }
                    return new Action(false, false);
                }
            }
        }
        if(attack) {
            rc.attackLocation(enemy.location);
        }
        return new Action(false, true);
    }

    private boolean backUp(boolean[] canMove, int[] damageInDir, int curDmg, double health, Direction dir){
        int idx = dir.ordinal();
        int tmpDmg = damageInDir[idx];
        // TODO MAGIC CONSTANTS
        if(canMove[idx] && tmpDmg * 2 < health && tmpDmg < curDmg) {
            return true;
        }
        return false;

    }


}
