package attacker4_0.actors;

import battlecode.common.*;
import attacker4_0.OtherGameConstants;
import attacker4_0.controllers.Controller;
import attacker4_0.movement.Bugger;
import attacker4_0.movement.Mover;
import attacker4_0.util.AttackerLookups;

/**
 * Created by kevin on 1/12/15.
 */
public class BasherActor extends Actor {
    private final Mover mover;
    private MapLocation target;
    private final Bugger bugger;

    public boolean avoidTowers;
    public boolean avoidEnemyRange;
    public MapLocation towerLoc;
    public boolean avoidHQ;

    private AttackerLookups lookups;

    public boolean wentIn;

    public BasherActor(Controller c, MapLocation target) {
        super(c);
        this.bugger = new Bugger(c);
        this.target = target;
        bugger.startBug(target);
        this.avoidTowers = true;
        this.avoidHQ = true;
        this.towerLoc = c.teamhq;
        this.avoidEnemyRange = true;

        this.wentIn = false;

        this.mover = new Mover(c);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        if (!move) {
            return new Action(false, false);
        }

        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);
        RobotInfo[] enemies = rc.senseNearbyRobots(OtherGameConstants.BASHER_RUSH_RADIUS_SQ, c.enemy);
        if (enemies.length > 0) {
            // always go in hard
            // add backing out code if they are just kiting forever.
            if (wentIn) {
                attack(enemies, canMove);
                return new Action(true, false);
            }

            // back out decision
            RobotInfo[] allies  = rc.senseNearbyRobots(OtherGameConstants.BASHER_RUSH_RADIUS_SQ, c.team);
            if (enemies.length < allies.length) {
                wentIn = true;
                attack(enemies, canMove);
                return new Action(true, false);
            }
        }

        wentIn = false; // everything is dead or gone

        // try to bug, and avoid enemies
        RobotInfo[] allEnemies = rc.senseNearbyRobots(c.sensorRadiusSquared, c.team);

        if (!bugger.bugging) {
            bugger.startBug(target);
        }
        if (allEnemies.length > 0) {
            mover.avoidEnemies(canMove);
        }

        Direction bug = bugger.bug(canMove);
        if (bug != Direction.NONE && bug != Direction.OMNI) {
            rc.move(bug);
        }
        return new Action(true, false);
    }

    public void attack(RobotInfo[] enemies, boolean[] canMove) throws GameActionException {
        // localize
        Controller c = this.c;
        RobotController rc = this.rc;
        int attackRadiusSq = c.type.attackRadiusSquared;
        int rushRadiusSq = OtherGameConstants.BASHER_RUSH_RADIUS_SQ;
        Team enemyTeam = c.enemy;
        int nearWeight = rushRadiusSq;
        int farWeight = attackRadiusSq;
        MapLocation here = rc.getLocation();

        int bestWeight = 0;
        Direction bestDir = null;

        // choose best dir out of left, dir, right, and here.
        // weight = nearWeight * nearEnemies + farWeight * farEnemies
        RobotInfo enemy = enemies[0];

        {
            MapLocation loc = here;
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            // dont count far radius, cause that's cheatin yo
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = Direction.NONE;
            }
        }

        Direction dir = here.directionTo(enemy.location);
        if (canMove[dir.ordinal()]) {
            MapLocation loc = here.add(dir);
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            weight += farWeight * rc.senseNearbyRobots(loc, rushRadiusSq, enemyTeam).length;
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = dir;
            }
        }

        Direction left = dir.rotateLeft();
        if (canMove[left.ordinal()]) {
            MapLocation loc = here.add(left);
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            weight += farWeight * rc.senseNearbyRobots(loc, rushRadiusSq, enemyTeam).length;
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = left;
            }
        }

        Direction right = dir.rotateRight();
        if (canMove[right.ordinal()]) {
            MapLocation loc = here.add(right);
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            weight += farWeight * rc.senseNearbyRobots(loc, rushRadiusSq, enemyTeam).length;
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = right;
            }
        }

        if (bestDir != null) {
            if (bestDir != Direction.NONE) {
                rc.move(bestDir);
            }
            return;
        }

        // can't move towards try other directions, assume we checked voids beforehand
        Direction leftSide = left.rotateLeft();
        if (canMove[leftSide.ordinal()]) {
            MapLocation loc = here.add(leftSide);
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            weight += farWeight * rc.senseNearbyRobots(loc, rushRadiusSq, enemyTeam).length;
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = leftSide;
            }
        }

        Direction rightSide = right.rotateLeft();
        if (canMove[rightSide.ordinal()]) {
            MapLocation loc = here.add(rightSide);
            int weight = nearWeight * rc.senseNearbyRobots(loc, attackRadiusSq, enemyTeam).length;
            weight += farWeight * rc.senseNearbyRobots(loc, rushRadiusSq, enemyTeam).length;
            if (weight > bestWeight) {
                bestWeight = weight;
                bestDir = rightSide;
            }
        }

        if (bestDir != null) {
            rc.move(bestDir);
            return;
        }

        // ??? cry
        return;
    }

    public void setTarget(MapLocation target) {
        if (!this.target.equals(target)) {
            this.target = target;
            bugger.startBug(target);
        }
    }

    public void setAvoidTowers(boolean avoidTowers){
        this.avoidTowers = avoidTowers;
    }

    public void ignoreTower(MapLocation towerLoc){
        this.towerLoc = towerLoc;
    }

    public void setAvoidEnemyRange(boolean avoidEnemyRange) { this.avoidEnemyRange = avoidEnemyRange; }

}
