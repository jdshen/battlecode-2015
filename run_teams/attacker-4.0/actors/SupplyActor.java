package attacker4_0.actors;

import battlecode.common.*;
import attacker4_0.OtherGameConstants;
import attacker4_0.controllers.Controller;

/**
 * Created by jdshen on 1/11/15.
 */
public class SupplyActor extends Actor {
    private final int bytecodeLimit;

    public SupplyActor(Controller c, int bytecodeLimit) {
        super(c);
        this.bytecodeLimit = bytecodeLimit;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        int num = Clock.getBytecodeNum();
        int count = (bytecodeLimit - num - OtherGameConstants.SENSE_NEARBY_BYTECODE) /
            (OtherGameConstants.SUPPLY_TRANSFER_BYTECODE + 100); // cost of a loop
        if (count <= 0) {
            return new Action(false, false);
        }

        RobotController rc = this.rc;

        RobotInfo[] infos = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, c.team);
        double supplyLevel = rc.getSupplyLevel();
        double multiplier = (c.type.supplyUpkeep + 1) * rc.getHealth() / c.type.maxHealth;
        for (int i = infos.length - 1; i >= 0 && count > 0; i--) {
            RobotInfo info = infos[i];

            if (info.type.isBuilding || info.type == RobotType.MISSILE) {
                continue;
            }

            // weighted supply level = supplyLevel / supply upkeep * health / maxHealth
            double transfer = (supplyLevel - info.supplyLevel * multiplier / info.health
                * info.type.maxHealth / (info.type.supplyUpkeep + 1)) / count;
            if (transfer > 0) {
                rc.transferSupplies((int) transfer, info.location);
                count--;
                supplyLevel -= (int) transfer;
            }
        }
        return new Action(false, false);
    }
}
