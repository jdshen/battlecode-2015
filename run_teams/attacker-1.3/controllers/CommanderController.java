package attacker1_3.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker1_3.actors.Action;
import attacker1_3.actors.CommanderFighter;
import attacker1_3.actors.SupplyActor;

/**
 * Created by jdshen on 1/6/15.
 */
public class CommanderController extends Controller {

    private CommanderFighter actor;
    private SupplyActor supplyActor;

    public CommanderController(RobotController rc) {
        super(rc);
        this.actor = new CommanderFighter(this, this.enemyhq);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action = actor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;


        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
