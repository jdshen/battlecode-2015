package attacker1_3.messaging;

import battlecode.common.*;


/**
 * For posting to / reading from maps with directions to path towards some target location
 *
 */
public class DirectionMessager {

    private RobotController rc;
    public Direction lastDir;

    public final int locationChannel;
    public final int offset;

    public final static int LOC_MASK = MessagingConstants.LOC_MASK;

    public final static int WIDTH = GameConstants.MAP_MAX_WIDTH;
    public final static int HEIGHT = GameConstants.MAP_MAX_HEIGHT;

    public Direction[] intToDir = Direction.values();
    public final static int[] loMasks = getNines(1);
    public final static int[] hiMasks = getNines(9);

    // helper method for getting array of powers of nine
    public static int[] getNines(int init){
        int[] neins = new int[9];
        int x = init;
        for(int i = 0; i < 9; i ++){
            neins[i] = x;
            x = x*9;
        }
        return neins;
    }


    // initialize a DirectionMessager for a given start channel (see MessagingConstants)
    public DirectionMessager(RobotController rc, int channelStart){
        locationChannel = channelStart;
        offset = channelStart + 1;

        this.rc = rc;
    }

    /**
     * Broadcast onto the location channel the offset from HQ that this map goes to.
     * @param loc broadcast-ready offset from HQ (has no negative vals)
     * @throws battlecode.common.GameActionException
     */
    public void broadcastLocation(MapLocation loc) throws GameActionException{
        rc.broadcast(locationChannel, loc.x + 2*WIDTH*loc.y);
    }

    /**
     * Get the location the direction map goes to
     * @return broadcast-ready offset from HQ (has no negative vals)
     * @throws battlecode.common.GameActionException
     */
    public MapLocation readLocation() throws GameActionException{
        int data = rc.readBroadcast(locationChannel);
        return new MapLocation(data % (2*WIDTH), (data / (2*WIDTH)));
    }

    /**
     * Post a direction to target location "to" at a loc
     */
    public void broadcastDirection(MapLocation loc, Direction dir) throws GameActionException{
        int x = loc.x % WIDTH;
        int y = loc.y % HEIGHT;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*WIDTH;
        int data = rc.readBroadcast(channel);

        // mask out the other and clear only this bit
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];
        data = data + (-(data % hiMask)/loMask + 1 + dir.ordinal())*loMask;

        rc.broadcast(channel, data);
    }

    /**
     * clear a location
     * @param loc
     * @throws battlecode.common.GameActionException
     */
    public void clearDirection(MapLocation loc) throws GameActionException{
        int x = loc.x % WIDTH;
        int y = loc.y % HEIGHT;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*WIDTH;
        int data = rc.readBroadcast(channel);

        // mask out the other and clear only this bit
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];
        data = data + (-(data % hiMask)/loMask)*loMask;

        rc.broadcast(channel, data);
    }


    /**
     * Read Direction posted for a location
     * @param loc
     * @return
     * @throws battlecode.common.GameActionException
     */
    public Direction readDirection(MapLocation loc) throws GameActionException{
        int x = loc.x % WIDTH;
        int y = loc.y % HEIGHT;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*WIDTH;
        int data = rc.readBroadcast(channel);

        // mask out the other locs and get only this loc
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];
        data = (data % hiMask)/loMask;

        if(data == 0){
            //nothing there
            return null;
        }else{
            data--;
            return intToDir[data];
        }
    }

}
