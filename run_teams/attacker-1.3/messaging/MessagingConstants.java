package attacker1_3.messaging;

import battlecode.common.*;

/**
 * Created by kevin on 1/7/15.
 */
public class MessagingConstants {
    /**
     * Max channel is 65536
     * Apx Channel Breakdown:
     *  1-8000: Global ID Channels
     *  8000-16000: Local ID Channels
     *  16000-24000: ????? more Id channels?????
     *  ??????
     *  30000: Global Channel
     *  ??????
     *  39000: Computation Channels
     *  40000 - 62240: BFS Direction maps (14 x 1600)
     *
     */


    //Set channel start locations
    public static final int ID_CHANNEL_START = 0;  //Start of all id-channels dedicated to i/o with specific id.


    public static final int GLOBAL_CHANNEL = 30000;
    public static final int ACTION_QUEUE_CHANNEL = GLOBAL_CHANNEL;


    //Start of ally computation, channels contain # allied unit. ~ 20 channels
    public static final int ALLY_COMPUTATION_CHANNEL_START = 39000;

    public static final int DIRECTION_CHANNEL_START = 40000; // reserve channels, for BFS


    //Stuff for BFS maps
    //size of each direction map (1601 for 120x120)
    public static final int DIRECTION_MAP_SIZE = 1 + (GameConstants.MAP_MAX_HEIGHT*GameConstants.MAP_MAX_WIDTH+8)/9;
    // start of sets of channels for BFS maps
    public static final int[] DIRECTION_MAP_STARTS = getDirMapStarts(14);


    //Stuff for ID centered communication
    // Apply mod to id when getting personal channel. Primary hash
    public static final int ID_CHAN_MOD = 4001;

    //starts of sets of id-channels dedicated to input/output with specific robot id
    public static final int ID_OFFSET_GLOBAL_TO_BOT = ID_CHANNEL_START;
    public static final int ID_OFFSET_BOT_TO_GLOBAL = ID_CHANNEL_START + ID_CHAN_MOD;
    public static final int ID_OFFSET_LOCAL_TO_BOT = ID_CHANNEL_START + ID_CHAN_MOD * 2;
    public static final int ID_OFFSET_BOT_TO_LOCAL = ID_CHANNEL_START + ID_CHAN_MOD * 3;

    //secondary hash. apply to id to get identifier check passed with a broadcast message
    //must be less than ID_CHAN_MOD and relative prime to it
    public static final int ID_CHECK_MOD = 31;

    //mask to apply to messages on id-channels to get actual message/identifier check. Greater than the ID_CHECK_MOD
    public static final int ID_MSG_MASK = ID_CHECK_MOD + 1;

    //Broadcast locations are offsets from HQ location.
    public static final int LOC_MASK = 4*GameConstants.MAP_MAX_WIDTH*GameConstants.MAP_MAX_HEIGHT;




    public static int convertLoc(MapLocation loc){
        return loc.x + loc.y*2*GameConstants.MAP_MAX_WIDTH;
    }

    // helper method for direction maps. length is how many dir maps we have. Doesn't check for going outside max channel
    public static int[] getDirMapStarts(int length){
        int[] starts = new int[length];
        for(int i = 0; i < length; i ++){
            starts[i] = DIRECTION_CHANNEL_START + i*DIRECTION_MAP_SIZE;
        }
        return starts;
    }
}
