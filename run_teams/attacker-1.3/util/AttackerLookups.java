package attacker1_3.util;

import battlecode.common.Direction;

import java.util.ArrayList;

/**
 * A class for fast lookups to get locations hit by enemy units.
 * Handles locations up to 4 away, and only attack ranges of 15, 2, 10 ,5
 */
public class AttackerLookups {

    public final Direction[] noDirs = {};

    public final Direction[] northEdge = {Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST};
    public final Direction[] eastEdge = {Direction.EAST, Direction.NORTH_EAST, Direction.SOUTH_EAST};
    public final Direction[] southEdge = {Direction.SOUTH, Direction.SOUTH_EAST, Direction.SOUTH_WEST};
    public final Direction[] westEdge = {Direction.WEST, Direction.NORTH_WEST, Direction.SOUTH_WEST};

    public final Direction[] northRect = {
            Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST,
            Direction.EAST, Direction.OMNI, Direction.WEST
    };
    public final Direction[] eastRect = {
            Direction.EAST, Direction.NORTH_EAST, Direction.SOUTH_EAST,
            Direction.NORTH, Direction.OMNI, Direction.SOUTH
    };
    public final Direction[] southRect = {
            Direction.SOUTH, Direction.SOUTH_EAST, Direction.SOUTH_WEST,
            Direction.EAST, Direction.OMNI, Direction.WEST
    };
    public final Direction[] westRect = {
            Direction.WEST, Direction.NORTH_WEST, Direction.SOUTH_WEST,
            Direction.NORTH, Direction.OMNI, Direction.SOUTH
    };

    public final Direction[] northWest = {Direction.NORTH_WEST};
    public final Direction[] northEast = {Direction.NORTH_EAST};
    public final Direction[] southEast = {Direction.SOUTH_EAST};
    public final Direction[] southWest = {Direction.SOUTH_WEST};


    public final Direction[] northWestWest = {Direction.NORTH_WEST, Direction.WEST};
    public final Direction[] northWestNorth = {Direction.NORTH_WEST, Direction.NORTH};
    public final Direction[] northEastEast = {Direction.NORTH_EAST, Direction.EAST};
    public final Direction[] northEastNorth = {Direction.NORTH_EAST, Direction.NORTH};
    public final Direction[] southEastEast = {Direction.SOUTH_EAST, Direction.EAST};
    public final Direction[] southEastSouth = {Direction.SOUTH_EAST, Direction.SOUTH};
    public final Direction[] southWestWest = {Direction.SOUTH_WEST, Direction.WEST};
    public final Direction[] southWestSouth = {Direction.SOUTH_WEST, Direction.SOUTH};

    public final Direction[] northWestCorner = {Direction.NORTH_WEST, Direction.NORTH, Direction.WEST};
    public final Direction[] northEastCorner = {Direction.NORTH_EAST, Direction.NORTH, Direction.EAST};
    public final Direction[] southEastCorner = {Direction.SOUTH_EAST, Direction.SOUTH, Direction.EAST};
    public final Direction[] southWestCorner = {Direction.SOUTH_WEST, Direction.SOUTH, Direction.WEST};

    public final Direction[] northWestSquare = {Direction.NORTH_WEST, Direction.NORTH, Direction.WEST, Direction.OMNI};
    public final Direction[] northEastSquare = {Direction.NORTH_EAST, Direction.NORTH, Direction.EAST, Direction.OMNI};
    public final Direction[] southEastSquare = {Direction.SOUTH_EAST, Direction.SOUTH, Direction.EAST, Direction.OMNI};
    public final Direction[] southWestSquare = {Direction.SOUTH_WEST, Direction.SOUTH, Direction.WEST, Direction.OMNI};

    // 5- square shapes
    public final Direction[] northWestEast = {Direction.NORTH_WEST, Direction.NORTH, Direction.NORTH_EAST, Direction.OMNI, Direction.EAST};
    public final Direction[] northWestSouth = {Direction.NORTH_WEST, Direction.SOUTH_WEST, Direction.WEST, Direction.OMNI, Direction.SOUTH};
    public final Direction[] northEastSouth = {Direction.NORTH_EAST, Direction.SOUTH_EAST, Direction.EAST, Direction.OMNI, Direction.SOUTH};
    public final Direction[] northEastWest = {Direction.NORTH_EAST, Direction.NORTH, Direction.NORTH_WEST, Direction.OMNI, Direction.WEST};
    public final Direction[] southEastWest = {Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.OMNI, Direction.WEST};
    public final Direction[] southEastNorth = {Direction.SOUTH_EAST, Direction.NORTH_EAST, Direction.EAST, Direction.OMNI, Direction.NORTH};
    public final Direction[] southWestNorth = {Direction.SOUTH_WEST, Direction.NORTH_WEST, Direction.WEST, Direction.OMNI, Direction.NORTH};
    public final Direction[] southWestEast = {Direction.SOUTH_WEST, Direction.SOUTH, Direction.SOUTH_EAST, Direction.OMNI, Direction.EAST};


    public final Direction[] northWestDiag = {
            Direction.NORTH_WEST, Direction.NORTH, Direction.WEST,
            Direction.OMNI, Direction.NORTH_EAST, Direction.SOUTH_WEST
    };
    public final Direction[] northEastDiag = {
            Direction.NORTH_EAST, Direction.NORTH, Direction.EAST,
            Direction.OMNI, Direction.NORTH_WEST, Direction.SOUTH_EAST
    };
    public final Direction[] southEastDiag = {
            Direction.SOUTH_EAST, Direction.SOUTH, Direction.EAST,
            Direction.OMNI, Direction.NORTH_EAST, Direction.SOUTH_WEST
    };
    public final Direction[] southWestDiag = {
            Direction.SOUTH_WEST, Direction.SOUTH, Direction.WEST,
            Direction.OMNI, Direction.SOUTH_EAST, Direction.NORTH_WEST
    };

    public final Direction[] northWestAll = {
            Direction.NORTH_WEST, Direction.NORTH, Direction.WEST,
            Direction.OMNI, Direction.NORTH_EAST, Direction.SOUTH_WEST, Direction.SOUTH, Direction.EAST
    };
    public final Direction[] northEastAll = {
            Direction.NORTH_EAST, Direction.NORTH, Direction.EAST,
            Direction.OMNI, Direction.NORTH_WEST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.WEST
    };
    public final Direction[] southEastAll = {
            Direction.SOUTH_EAST, Direction.SOUTH, Direction.EAST,
            Direction.OMNI, Direction.NORTH_EAST, Direction.SOUTH_WEST, Direction.NORTH, Direction.WEST
    };
    public final Direction[] southWestAll = {
            Direction.SOUTH_WEST, Direction.SOUTH, Direction.WEST,
            Direction.OMNI, Direction.SOUTH_EAST, Direction.NORTH_WEST, Direction.NORTH, Direction.EAST
    };



    //0
    public final Direction[] allDirs = {Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST,
            Direction.EAST, Direction.OMNI, Direction.WEST,
            Direction.SOUTH, Direction.SOUTH_EAST, Direction.SOUTH_WEST};


    public AttackerLookups(){
    }

    // Given an offset x and y ( them - you ) and attack range, return all directions that are unsafe
    // only works for ranges of 15, 10, 5, 2
    // x < 0 => they are west of you
    // y < 0 => they are north of you
    public Direction[] getDirs(int range, int x, int y){
        switch(range){
            case 24:
                // TOWERS ALWAYS HIT YOU :D
                switch(x){
                    case -5: return westEdge;
                    case 5: return eastEdge;
                    case -4:
                        switch(y){
                            case -4: return northWest;
                            case 4: return southWest;
                            case -3: return northWestCorner;
                            case 3: return southWestCorner;
                            case -2: return southWestNorth;
                            case 2: return northWestSouth;
                            case -1:
                            case 0:
                            case 1:
                                return allDirs;
                            default:
                                return noDirs;
                        }
                    case 4:
                        switch(y){
                            case -4: return northEast;
                            case 4: return southEast;
                            case -3: return northEastCorner;
                            case 3: return southEastCorner;
                            case -2: return southEastNorth;
                            case 2: return northEastSouth;
                            case -1:
                            case 0:
                            case 1:
                                return allDirs;
                            default:
                                return noDirs;
                        }
                    case -3:
                        switch(y){
                            case -5: return northWest;
                            case 5: return southWest;
                            case -4: return northWestCorner;
                            case 4: return southWestCorner;
                            case -3: return northWestDiag;
                            case 3: return southWestDiag;

                            case -2:
                            case -1:
                            case 0:
                            case 1:
                            case 2:
                                return allDirs;
                            default:
                                return noDirs;
                        }
                    case 3:
                        switch(y){
                            case -5: return northEast;
                            case 5: return southEast;
                            case -4: return northEastCorner;
                            case 4: return southEastCorner;
                            case -3: return northEastDiag;
                            case 3: return southEastDiag;

                            case -2:
                            case -1:
                            case 0:
                            case 1:
                            case 2:
                                return allDirs;
                            default:
                                return noDirs;
                        }

                    case -2:
                    case 2:
                    case -1:
                    case 0:
                    case 1:
                        switch(y){
                            case -5: return northEdge;
                            case 5: return southEdge;
                            case -4: return northRect; //TODO different for 2/-2 actually
                            case 4: return southRect;
                                //
                            case -3:
                            case 3:
                            case -2:
                            case -1:
                            case 0:
                            case 1:
                            case 2:
                                return allDirs;
                            default:
                                return noDirs;
                        }
                    default:
                        return noDirs;
                }
            case 15:
                switch(x){
                    case -4:
                        switch(y){
                            case -3: return northWest;
                            case -2: return northWestWest;
                            case -1: return westEdge;
                            case 0: return westEdge;
                            case 1: return westEdge;
                            case 2: return southWestWest;
                            case 3: return southWest;
                            default: return noDirs;
                        }
                    case -3:
                        switch(y){
                            case -4: return northWest;
                            case -3: return northWestCorner;
                            case -2: return southWestNorth;
                            case -1: return westRect;
                            case 0: return westRect;
                            case 1: return westRect;
                            case 2: return northWestSouth;
                            case 3: return southWestCorner;
                            case 4: return southWest;
                            default: return noDirs;
                        }
                    case -2:
                        switch(y){
                            case -4: return northWestNorth;
                            case -3: return northEastWest;
                            case -2: return northWestAll;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southWestAll;
                            case 3: return southEastWest;
                            case 4: return southWestSouth;
                            default: return noDirs;
                        }
                    case -1:
                    case 0:
                    case 1:
                        switch(y){
                            case -4: return northEdge;
                            case -3: return northRect;
                            case -2:
                            case -1:
                            case 0:
                            case 1:
                            case 2:
                                return allDirs;
                            case 3: return southRect;
                            case 4: return southEdge;
                            default: return noDirs;
                        }
                    case 2:
                        switch(y){
                            case -4: return northEastNorth;
                            case -3: return northWestEast;
                            case -2: return northEastAll;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southEastAll;
                            case 3: return southWestEast;
                            case 4: return southEastSouth;
                            default: return noDirs;
                        }
                    case 3:
                        switch(y){
                            case -4: return northEast;
                            case -3: return northEastCorner;
                            case -2: return southEastNorth;
                            case -1: return eastRect;
                            case 0: return eastRect;
                            case 1: return eastRect;
                            case 2: return northEastSouth;
                            case 3: return southEastCorner;
                            case 4: return southEast;
                            default: return noDirs;
                        }
                    case 4:
                        switch(y){
                            case -3: return northEast;
                            case -2: return northEastEast;
                            case -1: return eastEdge;
                            case 0: return eastEdge;
                            case 1: return eastEdge;
                            case 2: return southEastEast;
                            case 3: return southEast;
                            default: return noDirs;
                        }
                    default: return noDirs;
                }
            case 10:
                switch(x){
                    case -4:
                        switch(y){
                            case -2: return northWest;
                            case -1: return northWestWest;
                            case 0: return westEdge;
                            case 1: return southWestWest;
                            case 2: return southWest;
                            default: return noDirs;
                        }
                    case -3:
                        switch(y){
                            case -3: return northWest;
                            case -2: return northWestCorner;
                            case -1: return southWestNorth;
                            case 0: return westRect;
                            case 1: return northWestSouth;
                            case 2: return southWestCorner;
                            case 3: return southWest;
                            default: return noDirs;
                        }
                    case -2:
                        switch(y){
                            case -4: return northWest;
                            case -3: return northWestCorner;
                            case -2: return northWestDiag;
                            case -1: return northWestAll;
                            case 0: return allDirs;
                            case 1: return southWestAll;
                            case 2: return southWestDiag;
                            case 3: return southWestCorner;
                            case 4: return southWest;
                            default: return noDirs;
                        }
                    case -1:
                        switch(y){
                            case -4: return northWestNorth;
                            case -3: return northEastWest;
                            case -2: return northWestAll;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southWestAll;
                            case 3: return southEastWest;
                            case 4: return southWestSouth;
                            default: return noDirs;
                        }
                    case 0:
                        switch(y){
                            case -4: return northEdge;
                            case -3: return northRect;
                            case -2:
                            case -1:
                            case 0:
                            case 1:
                            case 2:
                                return allDirs;
                            case 3: return southRect;
                            case 4: return southEdge;
                            default: return noDirs;
                        }
                    case 1:
                        switch(y){
                            case -4: return northEastNorth;
                            case -3: return northWestEast;
                            case -2: return northEastAll;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southEastAll;
                            case 3: return southWestEast;
                            case 4: return southEastSouth;
                            default: return noDirs;
                        }
                    case 2:
                        switch(y){
                            case -4: return northEast;
                            case -3: return northEastCorner;
                            case -2: return northEastDiag;
                            case -1: return northEastAll;
                            case 0: return allDirs;
                            case 1: return southEastAll;
                            case 2: return southEastDiag;
                            case 3: return southEastCorner;
                            case 4: return southEast;
                            default: return noDirs;
                        }
                    case 3:
                        switch(y){
                            case -3: return northEast;
                            case -2: return northEastCorner;
                            case -1: return southEastNorth;
                            case 0: return eastRect;
                            case 1: return northEastSouth;
                            case 2: return southEastCorner;
                            case 3: return southEast;
                            default: return noDirs;
                        }
                    case 4:
                        switch(y){
                            case -2: return northEast;
                            case -1: return northEastEast;
                            case 0: return eastEdge;
                            case 1: return southEastEast;
                            case 2: return southEast;
                            default: return noDirs;
                        }
                    default: return noDirs;
                }
            case 8:
                switch(x){
                    case -3:
                        switch(y){
                            case -3: return northWest;
                            case -2: return northWestWest;
                            case -1: return westEdge;
                            case 0: return westEdge;
                            case 1: return westEdge;
                            case 2: return southWestWest;
                            case 3: return southWest;
                            default: return noDirs;
                        }
                    case -2:
                        switch(y){
                            case -3: return northWestNorth;
                            case -2: return northWestSquare;
                            case -1: return westRect;
                            case 0: return westRect;
                            case 1: return westRect;
                            case 2: return southWestSquare;
                            case 3: return southWestSouth;
                            default: return noDirs;
                        }
                    case -1:
                    case 0:
                    case 1:
                        switch(y){
                            case -3: return northEdge;
                            case -2: return northRect;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southRect;
                            case 3: return southEdge;
                            default: return noDirs;
                        }
                    case 2:
                        switch(y){
                            case -3: return northEastNorth;
                            case -2: return northEastSquare;
                            case -1: return eastRect;
                            case 0: return eastRect;
                            case 1: return eastRect;
                            case 2: return southEastSquare;
                            case 3: return southEastSouth;
                            default: return noDirs;
                        }
                    case 3:
                        switch(y){
                            case -3: return northEast;
                            case -2: return northEastEast;
                            case -1: return eastEdge;
                            case 0: return eastEdge;
                            case 1: return eastEdge;
                            case 2: return southEastEast;
                            case 3: return southEast;
                            default: return noDirs;
                        }
                    default: return noDirs;
                }
            case 5:
                switch(x){
                    case -3:
                        switch(y){
                            case -3: return noDirs;
                            case -2: return northWest;
                            case -1: return northWestWest;
                            case 0: return westEdge;
                            case 1: return southWestWest;
                            case 2: return southWest;
                            case 3: return noDirs;
                            default:
                                return noDirs;
                        }
                    case -2:
                        switch(y){
                            case -3: return northWest;
                            case -2: return northWestCorner;
                            case -1: return southWestNorth;
                            case 0: return westRect;
                            case 1: return northWestSouth;
                            case 2: return southWestCorner;
                            case 3: return southWest;
                            default:
                                return noDirs;
                        }
                    case -1:
                        switch(y){
                            case -3: return northWestNorth;
                            case -2: return northEastWest;
                            case -1: return northWestAll;
                            case 0: return allDirs;
                            case 1: return southWestAll;
                            case 2: return southEastWest;
                            case 3: return southWestSouth;
                            default:
                                return noDirs;
                        }
                    case 0:
                        switch(y){
                            case -3: return northEdge;
                            case -2: return northRect;
                            case -1: return allDirs;
                            case 0: return allDirs;
                            case 1: return allDirs;
                            case 2: return southRect;
                            case 3: return southEdge;
                            default:
                                return noDirs;
                        }
                    case 1:
                        switch(y){
                            case -3: return northEastNorth;
                            case -2: return northWestEast;
                            case -1: return northEastAll;
                            case 0: return allDirs;
                            case 1: return southEastAll;
                            case 2: return southWestEast;
                            case 3: return southEastSouth;
                            default:
                                return noDirs;
                        }
                    case 2:
                        switch(y){
                            case -3: return northEast;
                            case -2: return northEastCorner;
                            case -1: return southEastNorth;
                            case 0: return eastRect;
                            case 1: return northEastSouth;
                            case 2: return southEastCorner;
                            case 3: return southEast;
                            default:
                                return noDirs;
                        }
                    case 3:
                        switch(y){
                            case -3: return noDirs;
                            case -2: return northEast;
                            case -1: return northEastEast;
                            case 0: return eastEdge;
                            case 1: return southEastEast;
                            case 2: return southEast;
                            case 3: return noDirs;
                            default:
                                return noDirs;
                        }
                    default:
                        return noDirs;
                }
            case 2: //TODO change to "fake" range for melee to dodge more
                switch(x){
                    case -2:
                        switch(y){
                            case -2: return northWest;
                            case -1: return northWestWest;
                            case 0: return westEdge;
                            case 1: return southWestWest;
                            case 2: return southWest;
                            default:
                                return noDirs;
                        }
                    case -1:
                        switch(y){
                            case -2: return northWestNorth;
                            case -1: return northWestSquare;
                            case 0: return westRect;
                            case 1: return southWestSquare;
                            case 2: return southWestSouth;
                            default:
                                return noDirs;
                        }
                    case 0:
                        switch(y){
                            case -2: return northEdge;
                            case -1: return northRect;
                            case 0: return  allDirs;
                            case 1: return southRect;
                            case 2: return southEdge;
                            default:
                                return noDirs;
                        }
                    case 1:
                        switch(y){
                            case -2: return northEastNorth;
                            case -1: return northEastSquare;
                            case 0: return eastRect;
                            case 1: return southEastSquare;
                            case 2: return southEastSouth;
                            default:
                                return noDirs;
                        }
                    case 2:
                        switch(y){
                            case -2: return northEast;
                            case -1: return northEastEast;
                            case 0: return eastEdge;
                            case 1: return southEastEast;
                            case 2: return southEast;
                            default:
                                return noDirs;
                        }
                    default:
                        return noDirs;
                }
            default:
                return noDirs;
        }
    }

}
