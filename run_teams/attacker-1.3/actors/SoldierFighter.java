package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.movement.Mover;
import attacker1_3.util.AttackerLookups;
import attacker1_3.OtherGameConstants;
import attacker1_3.controllers.Controller;
import attacker1_3.movement.Bugger;

/**
 * Created by kevin on 1/19/15.
 */
public class SoldierFighter extends Actor {
    private Bugger bugger;
    private Mover mover;
    private AttackerLookups lookups;
    private Direction[] intToDirs = Direction.values();
    private int commanderXP = 0;

    private MapLocation target;
    private boolean avoidTowers;
    private boolean avoidHQ;
    private MapLocation towerLoc;

    public SoldierFighter(Controller c, MapLocation target) {
        super(c);
        this.bugger = new Bugger(c);
        this.lookups = new AttackerLookups();
        this.target = target;
        this.mover = new Mover(c);
        bugger.startBug(target);
        this.avoidTowers = true;
        this.avoidHQ = true;
        this.towerLoc = c.teamhq;
    }

    public void setTarget(MapLocation target) {
        if (this.target != target) {
            this.target = target;
            bugger.startBug(target);
        }
    }

    public void setAvoidTowers(boolean avoidTowers) {
        this.avoidTowers = avoidTowers;
    }

    public void setAvoidHQ(boolean avoidHQ) {
        this.avoidHQ = avoidHQ;
    }

    public void ignoreTower(MapLocation towerLoc) {
        this.towerLoc = towerLoc;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        if(!move && ! attack){
            return new Action(false, false);
        }

        RobotInfo[] enemies = rc.senseNearbyRobots(25, c.enemy);
        if (enemies.length != 0) {
            return actEnemies(move, attack, enemies);
        }

        if(!move){
            return new Action(false, false);
        }

        // if no enemies, bug
        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(mover.getCanMove(avoidTowers, towerLoc, avoidHQ));
        if(rc.canMove(dir)){
            rc.move(dir);
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actEnemies(boolean move, boolean attack,  RobotInfo[] enemies) throws GameActionException {
        // get damage in direction array
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;
        int[] damageInDir = new int[intToDirs.length];
        int numEnemies = enemies.length;
        RobotInfo enemy;
        RobotType enType;
        MapLocation enLoc;
        int tmp;
        int weight;
        int enemyCount = 0;
        Direction[] attackedDirs;
        for (int i = numEnemies - 1; i >= 0; i--) {
            enemy = enemies[i];
            enLoc = enemy.location;
            enType = enemy.type;
            tmp = enType.attackRadiusSquared;
            boolean adjbasher = false;
            switch(enType){
                case HQ:
                    weight = 6;
                    enemyCount += 24;
                    break;
                case TOWER:
                    weight = 4;
                    enemyCount += 12;
                    break;
                case SOLDIER:
                    weight = 1;
                    enemyCount += 6;
                    break;
                case BASHER:
                    weight = 1;
                    adjbasher = here.isAdjacentTo(enLoc);
                    enemyCount += 9;
                    break;
                case DRONE:
                    weight = 1;
                    enemyCount += 8;
                    break;
                case TANK:
                    weight = 2;
                    enemyCount += 21;
                    break;
                case COMMANDER:
                    weight = 2;
                    commanderXP = enemy.xp;
                    if(commanderXP >= GameConstants.XP_REQUIRED_LEADERSHIP) {
                        enemyCount += numEnemies;
                        if (commanderXP >= GameConstants.XP_REQUIRED_HEAVY_HANDS) {
                            weight = 6;
                            enemyCount += 12;
                            if (commanderXP >= GameConstants.XP_REQUIRED_IMPROVED_LEADERSHIP) {
                                enemyCount += numEnemies;
                            }
                        }
                    }
                    enemyCount += 24; // TODO consider whether they have the exp
                    break;
                case MISSILE:
                    weight = 5;
                    break;
                case LAUNCHER:
                    weight = 0;
                    enemyCount += 30;
                    break;
                case MINER:
                case BEAVER:
                    weight = 0;
                    enemyCount += 2;
                    break;
                default:
                    weight = 0;
                    break;
            }
            if(tmp < 8 && ! adjbasher){
                attackedDirs = lookups.getDirs(5, enLoc.x - hereX, enLoc.y - hereY);
            }
            else{
                attackedDirs = lookups.getDirs(tmp, enLoc.x - hereX, enLoc.y - hereY);
            }
            for (int j = attackedDirs.length - 1; j >= 0; j--){
                tmp = attackedDirs[j].ordinal();
                damageInDir[tmp] = damageInDir[tmp] + weight;
            }
        }

        // account for buildings/miners
        RobotInfo[] enInRange = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        if(enInRange.length != 0){
            return actFight(move, attack, damageInDir, enInRange);
        }

        // if outside attack range
        // decide to step towards enemies?
        tmp = Direction.OMNI.ordinal();
        if(damageInDir[tmp] == 0){
            if(!move){
                return new Action(false, false);
            }
            return actStepIn(damageInDir, enemyCount);
        }


        return actInEnemyRange(move, attack, damageInDir, enemyCount);
    }

    // Assume we can move.
    // damageInDir gives weighted dmg (num soldiers) in a direction
    // enemyCount is weighted number of enemies (soldier = 6)
    // counts commander effects if commander seen
    private Action actStepIn(int[] damageInDir, int enemyCount) throws GameActionException {
        //    check if path is clear towards enemies
        //    consider unit numbers
        // should consider enemy commander vs ally commander effects
        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);
        // check if we are willing to step into enemy range
        int allyCount = getAllyCount();
        //TODO check open path towards target/we won't go into chokepoint

        if(allyCount <= enemyCount){
            // Don't step in
            for(int i = intToDirs.length - 1; i >= 0; i--){
                if(damageInDir[i] > 0){
                    canMove[i] = false;
                }
            }
        }
//        if(!bugger.bugging){
//            bugger.startBug(target);
//        }
//        Direction dir = bugger.bug(canMove);
//        if(rc.canMove(dir)) {
//            rc.move(dir);
//            return new Action(true, false);
//        }
        Direction dir = rc.getLocation().directionTo(target);
        if(mover.makeSafeMove(dir, canMove, true)){
            bugger.endBug();
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actInEnemyRange(boolean move, boolean attack, int[] damageInDir, int enemyCount) throws GameActionException {
        // if in enemy attack range - decide retreat or fight

        if(!move){
            return new Action(false, false);
        }
        // In Tank range or Commander Range TODO improve this
        int allyCount = getAllyCount();
//        if(!bugger.bugging){
//            bugger.startBug(target);
//        }
        Direction dir = rc.getLocation().directionTo(target);
        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);
        if(allyCount > enemyCount) {
            // fuck them
//            Direction dir = bugger.bug(canMove);
//            if(rc.canMove(dir)) {
//                rc.move(dir);
//                return new Action(true, false);
//            }
            if(mover.makeSafeMove(dir, canMove, true)){
                bugger.endBug();
                return new Action(true, false);
            }
            return new Action(false, false);
        }
        Direction bestDir = Direction.OMNI;
        int bestDamage = damageInDir[bestDir.ordinal()];
        for (int i = intToDirs.length - 1; i >= 0; i--) {
            if(canMove[i] && damageInDir[i] < bestDamage){
                bestDamage = damageInDir[i];
                bestDir = intToDirs[i];
            }
            if (damageInDir[i] > 0) {
                canMove[i] = false;
            }
        }
//        Direction dir = bugger.bug(canMove);
//        if(rc.canMove(dir)) {
//            rc.move(dir);
//            return new Action(true, false);
//        }
        if(mover.makeSafeMove(dir, canMove, true)){
            bugger.endBug();
            return new Action(true, false);
        }
        if(rc.canMove(bestDir)){
            rc.move(bestDir);
            bugger.endBug();
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    private Action actFight(boolean move, boolean attack, int[] damageInDir, RobotInfo[] enInRange) throws GameActionException {
        // if fight decide moving in deeper or attacking
        //    TODO should move in deeper if there is no additional row of enemies
        //    and we have a second row. should only consider if < 4 enemies
        // back up if our best attack option is worse than number of soldiers that can hit me
        int numEns = enInRange.length;
        RobotInfo enemy = enInRange[numEns-1];
        RobotType enType = enemy.type;
        MapLocation enLoc = enemy.location;
        MapLocation bestLoc = enemy.location;
        Team myTeam = c.team;
        double myDamage = c.attackPower;

        //TODO consider enemy commander
        //TODO improve "our dmg" calculation
        int numAllies = rc.senseNearbyRobots(enLoc, c.attackRadiusSquared, myTeam).length + 1;
        int maxAllies = numAllies;
        int turns = (int) ((enemy.health / myDamage) / numAllies + 0.99999);
        double damageRate = enType.attackPower / (0.01 + enType.attackDelay);
        double score = damageRate / turns;
        double newScore;
        int bestTurns = turns;
        for (int i = numEns - 2; i >= 0; i--) {
            enemy = enInRange[i];
            enType = enemy.type;
            enLoc = enemy.location;
            numAllies = rc.senseNearbyRobots(enLoc, c.attackRadiusSquared, myTeam).length + 1;
            if(numAllies > maxAllies){
                maxAllies = numAllies;
            }
            turns = (int) (((enemy.health / myDamage) / numAllies) + 0.9999);
            damageRate = enType.attackPower / (0.01 + enType.attackDelay);
            newScore = damageRate / turns;
            if(score < newScore) {
                score = newScore;
                bestLoc = enLoc;
                bestTurns = turns;
            }
        }

        double myHealth = rc.getHealth() / 4;
        // reposition?
        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);
        MapLocation here = rc.getLocation();
        Direction dir = here.directionTo(bestLoc);
        Direction behind = dir.opposite();
        Direction behindLeft = behind.rotateLeft();
        Direction behindRight = behind.rotateRight();
        int curDmg = damageInDir[Direction.OMNI.ordinal()];
        int myturns = (int) (myHealth/(0.1 + curDmg) + 0.999);

        if(!move){ //penalty to trying to reposition
            myHealth = myHealth - curDmg;
        }
        int tmpDmg;
        //backup
        if(maxAllies == 1){
            // Code to force retreat in micro situation
            if(myturns < bestTurns){
                maxAllies = 0;
                curDmg += 2;
            }
        }

        if(maxAllies < curDmg){
            if(backUp(canMove, damageInDir, curDmg, myHealth, behind)){
                if(move){
                    rc.move(behind);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
            if(backUp(canMove, damageInDir, curDmg, myHealth, behindLeft)){
                if(move){
                    rc.move(behindLeft);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
            if(backUp(canMove, damageInDir, curDmg, myHealth, behindRight)){
                if(move){
                    rc.move(behindRight);
                    bugger.endBug();
                    return new Action(true, false);
                }
                return new Action(false, false);
            }
        }
        //stepin
        if(!here.isAdjacentTo(bestLoc) && maxAllies >=3){
            Direction dirLeft = dir.rotateLeft();
            Direction dirRight = dir.rotateRight();
            int idx = dir.ordinal();
            tmpDmg = damageInDir[idx];
            boolean stepin = false;
            if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                stepin = true;
            }else{
                idx = dirLeft.ordinal();
                tmpDmg = damageInDir[idx];
                if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                    dir = dirLeft;
                    stepin = true;
                }else{
                    idx = dirRight.ordinal();
                    tmpDmg = damageInDir[idx];
                    if(canMove[idx] && tmpDmg <= curDmg && tmpDmg * 2 < myHealth){
                        dir = dirLeft;
                        stepin = true;
                    }
                }
            }
            if(stepin) {
                numAllies = 0;
                enemy = rc.senseRobotAtLocation(here.add(behind));
                if (enemy != null && enemy.team == myTeam) {
                    numAllies++;
                }
                enemy = rc.senseRobotAtLocation(here.add(behindLeft));
                if (enemy != null && enemy.team == myTeam) {
                    numAllies++;
                }
                enemy = rc.senseRobotAtLocation(here.add(behindRight));
                if (enemy != null && enemy.team == myTeam) {
                    numAllies++;
                }
                if (numAllies > 1){
                    if(move){
                        rc.move(dir);
                        bugger.endBug();
                        return new Action(true, false);
                    }
                    return new Action(false, false);
                }
            }
        }
        if(attack) {
            rc.attackLocation(bestLoc);
        }
        return new Action(false, true);
    }

    private boolean backUp(boolean[] canMove, int[] damageInDir, int curDmg, double health, Direction dir){
        int idx = dir.ordinal();
        int tmpDmg = damageInDir[idx];
        // TODO MAGIC CONSTANTS
        if(canMove[idx] && tmpDmg * 2 < health && tmpDmg < curDmg) {
            return true;
        }
        return false;

    }

    private int getAllyCount(){
        int allyCount = 0; // Discount self, handicap towards stepping in
        RobotInfo ally;
        RobotType allyType;
        RobotInfo[] allies = rc.senseNearbyRobots(25, c.team);
        for (int i = allies.length - 1; i >= 0; i--) {
            ally = allies[i];
            allyType = ally.type;
            switch(allyType){
                case HQ:
                    break;
                case TOWER:
                    break;
                case SOLDIER:
                    allyCount += 6;
                    break;
                case BASHER:
                    allyCount += 8;
                    break;
                case DRONE:
                    allyCount += 4;
                    break;
                case TANK:
                    allyCount += 21;
                    break;
                case COMMANDER:
                    int xp = ally.xp;
                    if(xp >= GameConstants.XP_REQUIRED_LEADERSHIP) {
                        allyCount += allies.length;
                        if (xp >= GameConstants.XP_REQUIRED_HEAVY_HANDS) {
                            allyCount += 12;
                            if (xp >= GameConstants.XP_REQUIRED_IMPROVED_LEADERSHIP) {
                                allyCount += allies.length;
                            }
                        }
                    }
                    allyCount += 9;
                    break;
                case MISSILE:
                    break;
                case LAUNCHER:
                    allyCount += 30;
                    break;
                default:
                    break;
            }
        }
        return allyCount;
    }
}
