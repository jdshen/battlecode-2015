package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;

public class Scout extends Actor {
    private final int blockSize = 5;
    private MapLocation currentTarget;

    private final int width = GameConstants.MAP_MAX_WIDTH/blockSize;
    private final int height = GameConstants.MAP_MAX_HEIGHT/blockSize;
    private int[][] scouted = new int[width][height];

    public Scout(Controller c) {
        super(c);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }



    private boolean makeMove(boolean move) throws GameActionException {
        if (!move) {
            return false;
        }
        RobotController rc = this.rc;
        Controller c = this.c;
        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared, c.enemy);

        //TODO avoid enemies
        if (enemies.length != 0) {
            return false;
        }

        MapLocation here = rc.getLocation();

        /*
        Go to square if
         */
        // hash loc
        Direction next;
        Direction[] valid = new Direction[8];
        MapLocation[] locs = new MapLocation[8];
        int size = 0;
        for (Direction dir = Direction.NORTH; (next = dir.rotateRight()) != Direction.NORTH; dir = next) {
            if (!rc.canMove(dir)) {
                continue;
            }

            int multiple = 4;
            switch (dir) {
                case NORTH:
                case EAST:
                case SOUTH:
                case WEST:
                    multiple = 4;
                    break;
                case NORTH_EAST:
                case SOUTH_EAST:
                case SOUTH_WEST:
                case NORTH_WEST:
                    multiple = 3;
                    break;
                default:
                    break;
            }

            MapLocation loc = here.add(dir, multiple);
            if (rc.senseTerrainTile(loc) == TerrainTile.OFF_MAP) {
                continue;
            }

            locs[size] = loc;
            valid[size] = dir;
            size++;
        }

        for (int i = 10; i >= 0; i--) {
            for (int j = size - 1; j >= 0; j--) {
                Direction dir = valid[j];
                if (dir == null) {
                    continue;
                }

                MapLocation loc = locs[j] = locs[j].add(dir);
                TerrainTile tile = rc.senseTerrainTile(loc);

                if (tile == TerrainTile.UNKNOWN) {
                    rc.setIndicatorDot(loc, 100, 0, 0);
                    rc.move(dir);
                    return true;
                } else if (tile == TerrainTile.OFF_MAP) {
                    valid[j] = null;
                }
            }
        }

        return false;
    }
}
