package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.OtherGameConstants;
import attacker1_3.controllers.Controller;

/**
 * Created by jdshen on 1/11/15.
 */
public class SupplyActor extends Actor {
    private final int bytecodeLimit;

    public SupplyActor(Controller c, int bytecodeLimit) {
        super(c);
        this.bytecodeLimit = bytecodeLimit;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        int num = Clock.getBytecodeNum();
        int count = (bytecodeLimit - num - OtherGameConstants.SENSE_NEARBY_BYTECODE) /
            OtherGameConstants.SUPPLY_TRANSFER_BYTECODE;
        RobotInfo[] infos = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, c.team);
        double supplyLevel = rc.getSupplyLevel();
        for (int i = infos.length - 1; i >= 0; i--) {
            RobotInfo info = infos[i];
            if (count <= 0 || info.type.isBuilding || info.type == RobotType.MISSILE) {
                continue;
            }

            if (supplyLevel - info.supplyLevel >= count) {
                int transfer = (int) ((supplyLevel - info.supplyLevel) / count);
                rc.transferSupplies(transfer, info.location);
                count--;
                supplyLevel -= transfer;
            }
        }
        return new Action(false, false);
    }
}
