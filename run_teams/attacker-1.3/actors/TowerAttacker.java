package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;
import attacker1_3.messaging.MessageType;
import attacker1_3.messaging.MessagingConstants;
import attacker1_3.messaging.RobotMessager;

/**
 * Attacker for Towers
 */
public class TowerAttacker extends Actor{

    private RobotMessager idmessager;

    public TowerAttacker(Controller c) {
        super(c);
        idmessager = new RobotMessager(c, c.id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        //clear old channel status TODO this is somewhat wasteful
        idmessager.clearChannel();
        if(!attack) {
            return new Action(false, false);
        }
        //get enemies in range
        RobotInfo[] enemies = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }
        // alert HQ that tower is engaging enemy
        idmessager.broadcastMsg(MessageType.NEARBY_ENEMY, rc.getLocation());

        //look for best target
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        // get number of turns to kill enemy
        int turns = (int) (enemy.health / c.attackPower + .999);
        // get enemy damage rate
        double damageRate = type.attackPower / (0.01+type.attackDelay);
        double score = turns/ damageRate;
        MapLocation loc = enemy.location;

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / c.attackPower + .999);
            // get enemy damage rate
            damageRate = type.attackPower / (0.01+type.attackDelay);
            double newScore = turns/damageRate;
            //switch targets
            if(score > newScore){
                score = newScore;
                loc = enemy.location;
            }
        }

        rc.attackLocation(loc);

        return new Action(false, true);
    }
}
