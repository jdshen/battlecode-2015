package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;

/**
 * Attacker for HQ..
 */
public class HQAttacker extends Actor{
    private final MapLocation[] splashAttackPoints = getSplashAttackPoints();
    private int splashIndex = 0;
    // # times to try attacking a splash attack point
    private final int retries = 5;

    public HQAttacker(Controller c) {
        super(c);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        if(!attack){
            return new Action(false, false);
        }

        // figure out how many allied towers are left
        int numTowers = rc.senseTowerLocations().length;
        switch(numTowers){
            case 6:
                return attackSplash(GameConstants.HQ_BUFFED_DAMAGE_MULTIPLIER_LEVEL_2*c.attackPower);
            case 5:
                return attackSplash(GameConstants.HQ_BUFFED_DAMAGE_MULTIPLIER_LEVEL_1*c.attackPower);
            case 4:
            case 3:
                int range = GameConstants.HQ_BUFFED_ATTACK_RADIUS_SQUARED;
                RobotInfo[] enemies = rc.senseNearbyRobots(range, c.enemy);
                return attackNormal(GameConstants.HQ_BUFFED_DAMAGE_MULTIPLIER_LEVEL_1*c.attackPower,
                        enemies);
            case 2:
                range = GameConstants.HQ_BUFFED_ATTACK_RADIUS_SQUARED;
                enemies = rc.senseNearbyRobots(range, c.enemy);
                return attackNormal(c.attackPower, enemies);
            default:
                range = c.attackRadiusSquared;
                enemies = rc.senseNearbyRobots(range, c.enemy);
                return attackNormal(c.attackPower, enemies);
        }


    }

    // Assume range + splash buffs are applied. Pass the current buffed damage
    public Action attackSplash(double damage) throws GameActionException {
        int range = GameConstants.HQ_BUFFED_ATTACK_RADIUS_SQUARED;
        RobotInfo[] enemies = rc.senseNearbyRobots(range, c.enemy);
        if(enemies.length > 0) {
            return attackNormal(damage, enemies);
        }
        enemies = rc.senseNearbyRobots(51, c.enemy);
        RobotInfo en;
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;
        int delX;
        int delY;
        int signX;
        int signY;
        for(int i = enemies.length - 1; i >= 0; i--){
            en = enemies[i];
            here = en.location;
            delX = hereX - here.x;
            delY = hereY - here.y;
            if(delX < 0){
                delX = -delX;
                signX = -1;
            }
            else{
                signX = 1;
            }
            if(delY < 0){
                delY = -delY;
                signY = -1;
            }
            else{
                signY = 1;
            }
            if(delX < 7 && delY < 7 && (delX + delY) < 11){
                // TODO maybe don't hit allies, but friendly fire seems off for now
                rc.attackLocation(here.add(signX, signY));
                return new Action(false, true);
            }
        }
        //RANDOMLY ATTACK POINTS IN A CIRCLE
        for(int i = 0; i < retries; i++){
            if(rc.senseRobotAtLocation(splashAttackPoints[splashIndex]) == null){
                //MAGIC NUMBER CONSTANT
                rc.attackLocation(splashAttackPoints[splashIndex]);
                splashIndex = (splashIndex + 1) % 20;
                return new Action(false,true);
            }
            splashIndex = (splashIndex + 1) % 20;

        }
        return  new Action(false,false);
    }

    public Action attackNormal(double damage, RobotInfo[] enemies) throws GameActionException {

        if(enemies.length == 0){
            return new Action(false, false);
        }
        //look for best target
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        // get number of turns to kill enemy
        int turns = (int) (enemy.health / damage + .999);
        // get enemy damage rate
        double damageRate = type.attackPower / (0.01+type.attackDelay);
        double score = turns/ damageRate;
        MapLocation loc = enemy.location;

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / damage + .999);
            // get enemy damage rate
            damageRate = type.attackPower / (0.01+type.attackDelay);
            double newScore = turns/damageRate;
            //switch targets
            if(score > newScore){
                score = newScore;
                loc = enemy.location;
            }
        }

        rc.attackLocation(loc);
        return new Action(false, true);
    }

    public MapLocation[] getSplashAttackPoints(){
        MapLocation center = c.teamhq;
        MapLocation[] locs = new MapLocation[]{
                center.add(5,1),
                center.add(5,3),
                center.add(4,4),
                center.add(3,5),
                center.add(1,5),
                center.add(5,-1),
                center.add(5,-3),
                center.add(4,-4),
                center.add(3,-5),
                center.add(1,-5),
                center.add(-5,1),
                center.add(-5,3),
                center.add(-4,4),
                center.add(-3,5),
                center.add(-1,5),
                center.add(-5,-1),
                center.add(-5,-3),
                center.add(-4,-4),
                center.add(-3,-5),
                center.add(-1,-5),
        };

        return locs;
    }
}
