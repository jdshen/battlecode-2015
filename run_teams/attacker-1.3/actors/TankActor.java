package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;
import attacker1_3.movement.Bugger;
import attacker1_3.movement.Mover;
import attacker1_3.util.AttackerLookups;

/**
 * Created by kevin on 1/11/15.
 */
public class TankActor extends Actor{
    private MapLocation target;

    public GenericAttacker attacker;
    public AttackerLookups lookup;

    public Bugger bugger;

    public Mover mover;

    public boolean avoidTowers;
    public boolean avoidEnemyRange;
    public MapLocation towerLoc;
    public boolean avoidHQ;

    public TankActor(Controller c, MapLocation target) {
        super(c);
        this.mover = new Mover(c);
        this.bugger = new Bugger(c);
        setTarget(target);
        lookup = new AttackerLookups();
        this.attacker = new GenericAttacker(c);
        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
        this.avoidEnemyRange = true;
        this.avoidHQ = true;
    }

    public void setTarget(MapLocation target){
        if (this.target != target) {
            this.target = target;
            bugger.startBug(target);
        }
    }

    public void setAvoidTowers(boolean avoidTowers){
        this.avoidTowers = avoidTowers;
    }

    public void setAvoidHQ(boolean avoidHQ){
        this.avoidHQ = avoidHQ;
    }

    public void ignoreTower(MapLocation towerLoc){
        this.towerLoc = towerLoc;
    }

    public void setAvoidEnemyRange(boolean avoidEnemyRange) { this.avoidEnemyRange = avoidEnemyRange; }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        RobotController rc = this.rc;
        Controller c = this.c;
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;

        if (!attack) {
            // just wait
            RobotInfo[] enemies = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
            if(enemies.length > 0) {
                return new Action(false, false);
            }
        }
        // attack anything in range.
        Action act = attacker.act(move, attack);
        // End turn after attacking
        if(act.attacked){
            return new Action (false, true);
        }
        if(!move){
            return new Action(false, false);
        }

        //Movement decision. Try to bug to target, staying out of range.

        boolean[] canMove = mover.getCanMove(avoidTowers, towerLoc, avoidHQ);

        //check enemy ranges
        if(avoidEnemyRange) {
            RobotInfo[] enemies = rc.senseNearbyRobots(25, c.enemy); //TODO MAGIC CONSTANT
            int numEnemies = enemies.length;
            RobotInfo en;
            MapLocation there;
            Direction[] attacked;
            int idx = 0;
            for (int i = numEnemies - 1; i >= 0; i--) {
                en = enemies[i];
                there = en.location;
                int attackRadius = en.type.attackRadiusSquared;
                if (attackRadius < 9) { // not a threat to tanks TODO MAGIC CONSTANT
                    continue;
                }
                attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);

                for (int j = attacked.length - 1; j >= 0; j--) {
                    idx = attacked[j].ordinal();
                    canMove[idx] = false;
                }
            }
        }

        //stop when close enough
        if(!here.isAdjacentTo(target)) {
            //bug
            Direction dir = bugger.bug(canMove);
            if (rc.canMove(dir)) {
                rc.move(dir);
                return new Action(true, false);
            }
        }
        Direction dir = here.directionTo(target);
        if(canMove[dir.ordinal()]){
            rc.move(dir);
            return new Action(true, false);
        }
        return new Action(false, false);
    }
}
