package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;
import attacker1_3.messaging.MessageType;
import attacker1_3.messaging.RobotMessager;
import attacker1_3.movement.Bugger;

public class Builder extends Actor {
    private final RobotMessager out;
    private Bugger bugger;
    private MapLocation loc;
    private int count;
    private RobotType structure;

    public Builder(
        Controller c, RobotType structure, MapLocation loc, RobotMessager out, int count
    ) {
        super(c);
        this.structure = structure;
        this.loc = loc;
        this.count = count;
        this.bugger = new Bugger(c);
        bugger.startBug(loc);
        this.out = out;
    }

    public Builder(Controller c, RobotType structure, MapLocation loc, int count) {
        this(c, structure, loc, null, count);
    }

    public Builder(Controller c, RobotType structure, MapLocation loc) {
        this(c, structure, loc, 1);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }

    public boolean makeMove(boolean move) throws GameActionException {
        if (out != null) {
            out.broadcastMsg(MessageType.ACK, rc.getLocation());
        }

        if (!move) {
            return false;
        }

        if (count == 0) {
            return false;
        }

        RobotController rc = this.rc;

        if (rc.getLocation().distanceSquaredTo(loc) <= 2) {
            bugger.endBug();
        }

        if (bugger.bugging) {
            Direction dir = bugger.bug(bugger.calcCanMove());
            if (dir != Direction.NONE && dir != Direction.OMNI) {
                rc.move(dir);
            }

            return true;
        }

        rc.setIndicatorDot(loc, 255, 0, 0);
        Direction buildDir = rc.getLocation().directionTo(loc);
        if (rc.canBuild(buildDir, structure)) {
            rc.build(buildDir, structure);
            count--;
            return true;
        }

        return false;
    }

}
