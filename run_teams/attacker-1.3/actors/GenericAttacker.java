package attacker1_3.actors;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import attacker1_3.controllers.Controller;

/**
 * Created by kevin on 1/11/15.
 */
public class GenericAttacker extends Actor {

    public GenericAttacker(Controller c) { super(c); }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        if(!attack){
            return new Action(false, false);
        }


        //ATTACK LOGIC
        //get enemies in range
        RobotInfo[] enemies = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }

        //look for "best" target
        // currently (enemy-hp)/(enemy-dmg-rate) TODO better targetting
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;

        // get number of turns to kill enemy
        int turns = (int) (enemy.health / c.attackPower + .999);
        // get enemy damage rate
        double damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
        double score = turns/ damageRate + getBonus(enemy);
        MapLocation loc = enemy.location;

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / c.attackPower + .999);
            // get enemy damage rate
            damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
            double newScore = turns/damageRate + getBonus(enemy);
            //switch targets
            if(score > newScore){
                score = newScore;
                loc = enemy.location;
            }
        }

        rc.attackLocation(loc);

        return new Action(false, true);
    }

    public int getBonus(RobotInfo info){
        return 0;
    }
}
