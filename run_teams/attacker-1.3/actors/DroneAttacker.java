package attacker1_3.actors;

import battlecode.common.*;
import attacker1_3.controllers.Controller;
import attacker1_3.util.FastIterableIntSet;

/**
 * For Drones - only attacaks
 * Created by kevin on 1/9/15.
 */
public class DroneAttacker extends GenericAttacker {

    public DroneAttacker(Controller c) {
        super(c);
    }

    // attack bonus. Minus is good
    @Override
    public int getBonus(RobotInfo info){
        switch(info.type){
            case HQ:
            case TOWER:
            case HANDWASHSTATION:
            case SUPPLYDEPOT:
                break;
            case TECHNOLOGYINSTITUTE:
            case TRAININGFIELD:
                return -20;
            case BARRACKS:
            case HELIPAD:
                return -30;
            case TANKFACTORY:
            case MINERFACTORY:
            case AEROSPACELAB:
                return -50;
            case BEAVER:
                return -50;
            case COMPUTER:
                return -100;
            case SOLDIER:
                break;
            case BASHER:
                break;
            case MINER:
                return -40;
            case DRONE:
                break;
            case TANK:
                break;
            case COMMANDER:
                break;
            case LAUNCHER:
                break;
            case MISSILE:
                return 50;
        }
        return 0;

    }
}
