package attacker1_3.threads;

import battlecode.common.GameActionException;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import attacker1_3.controllers.Controller;
import attacker1_3.messaging.MessageType;
import attacker1_3.messaging.MessagingConstants;
import attacker1_3.messaging.RobotMessager;

/**
 * Created by jdshen on 1/8/15.
 */
public class SpawnThread extends ObjectiveThread {
    private final RobotType type;
    private final RobotType building;
    private MessageType message;
    private RobotMessager in;
    private RobotMessager out;

    private boolean connected; // connected to a beaver
    private RobotInfoManager manager;

    public SpawnThread(Controller c, RobotType type, RobotInfoManager manager) {
        this(c, type, -1, manager);
    }

    public SpawnThread(Controller c, RobotType type, int suggestedID, RobotInfoManager manager) {
        super(c);
        this.type = type;
        this.manager = manager;
        this.building = type.spawnSource;
        // TODO possibly replace this with something that figures out how many units of each type
        this.message = MessageType.getSpawnMessage(type);
        this.connected = false;
        this.id = suggestedID;
    }

    @Override
    public boolean run(RobotInfo info) throws GameActionException {
        if (info == null) {
            connected = false;
            return false;
        }

        // track bot for acknowledgement
        if (out.readMsg()) {
            if (out.lastMsg.ack) {
                if (out.lastMsg == MessageType.SPAWNED) {
                    manager.addSpawnedBots(out.lastLoc);
                }
                out.clearChannel();
                return false;
            }

            // no ack, find another bot
            in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
            connected = false;
            return false;
        }

        return false;

    }

    @Override
    public void find(RobotInfo[] bots, boolean[] taken) throws GameActionException {
        if (connected) {
            return;
        }

        for (int i = bots.length - 1; i >= 0; i--) {
            RobotInfo info = bots[i];
            int id = info.ID;

            if (taken[id]) {
                continue;
            }

            if (info.type == building && info.builder == null) {
                taken[i] = true;
                connected = true;
                this.id = id;
                in = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
                out = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
                in.broadcastMsg(message, rc.getLocation());
                return;
            }
        }
    }

    @Override
    public void close() throws GameActionException {
        in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
        out.clearChannel();
        done = true;
    }
}
