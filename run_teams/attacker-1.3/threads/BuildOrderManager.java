package attacker1_3.threads;

import battlecode.common.*;
import attacker1_3.OtherGameConstants;
import attacker1_3.controllers.Controller;

public class BuildOrderManager {
    private final Controller c;
    private final RobotController rc;
    private final ObjectiveManager obj;

    private ObjectiveThread mainThread;
    private BuildingLocator locator;
    private boolean[] taken;

    private GeneralMicro micro;
    public RobotInfoManager allies;

    private static final RobotType[] mainLine = new RobotType[]{
        RobotType.MINERFACTORY, RobotType.MINER,
        RobotType.BARRACKS, RobotType.BASHER,
        RobotType.TANKFACTORY, RobotType.TANK,
        RobotType.TANKFACTORY, RobotType.TANK,
        RobotType.SUPPLYDEPOT, null,
//        RobotType.BARRACKS, null,
        RobotType.TECHNOLOGYINSTITUTE, null,
        RobotType.TRAININGFIELD, RobotType.COMMANDER,
        RobotType.TANKFACTORY, RobotType.TANK,
        RobotType.SUPPLYDEPOT, null,
    };

    private static int mainLineRepeat = mainLine.length - 4;

    private int mainLineIndex;

    public BuildOrderManager(Controller c, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;
        this.obj = new ObjectiveManager(c);
        this.locator = new BuildingLocator(c);
        this.taken = new boolean[OtherGameConstants.ROBOT_MAX_ID];
        mainLineIndex = 0;
        this.allies = allies;
        this.micro = new GeneralMicro(c, allies);
    }

    public void run() throws GameActionException {
        RobotInfo[] allAllies = rc.senseNearbyRobots(Controller.MAX_DIAGONAL_SQ, c.team);

        if (mainThread == null) {
            RobotType type = mainLine[mainLineIndex];
            MapLocation loc = locator.next(type);
            mainThread = new BuildThread(c, loc, type, allies);
            obj.add(mainThread);
        } else {
            if (mainThread.isSuccess()) {
                if (mainLine[mainLineIndex + 1] != null) {
                    obj.add(new SpawnThread(c, mainLine[mainLineIndex + 1], allies));
                }
                mainLineIndex += 2;
                if (mainLineIndex >= mainLine.length) {
                    mainLineIndex = mainLineRepeat;
                }
            }

            if (mainThread.isSuccess() || mainThread.isFailed()) {
                RobotType type = mainLine[mainLineIndex];
                MapLocation loc = locator.next(type);
                mainThread = new BuildThread(c, loc, type, allies);
                obj.add(mainThread);
            }
        }

        allies.clearBots();

        // Uncomment out code to debug RobotInfoManager
//        String s = "";
//        for (int i = 0; i < allies.bots.length / 2; i++) {
//            s += RobotType.values()[i] + ":";
//            s += allies.sizes[i] + ",";
//        }
//
//        rc.setIndicatorString(0, s);
//        s = "";
//
//        for (int i = allies.bots.length / 2; i < allies.bots.length; i++) {
//            s += RobotType.values()[i] + ":";
//            s += allies.sizes[i] + ",";
//        }
//        rc.setIndicatorString(1, s);


        obj.run(allAllies, taken);
        micro.run(taken);
    }
}
