package attacker1_3.threads;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import attacker1_3.OtherGameConstants;
import attacker1_3.controllers.Controller;

/**
 * Created by jdshen on 1/9/15.
 */
public class ObjectiveManager {
    private static final int MAX_THREADS = 16;

    private ObjectiveThread[] threads;
    private final Controller c;
    private final RobotController rc;
    private int[] freeBots;
    private int freeBotCount;

    public ObjectiveManager(Controller c) {
        this.c = c;
        this.rc = c.rc;
        threads = new ObjectiveThread[MAX_THREADS];
        freeBots = new int[MAX_THREADS];
        freeBotCount = 0;
    }

    public int add(ObjectiveThread thread) {
        ObjectiveThread[] threads = this.threads;
        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                threads[i] = thread;
                return i;
            }
        }

        return -1;
    }

    public void run(RobotInfo[] allies, boolean[] taken) throws GameActionException {
        for (int i = freeBotCount - 1; i >= 0; i--) {
            taken[freeBots[i]] = false;
        }
        freeBotCount = 0;

        ObjectiveThread[] threads = this.threads;
        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                continue;
            }

            ObjectiveThread thread = threads[i];
            int id = thread.id;
            if (id >= 0) {
                taken[id] = true;
                boolean done = rc.canSenseRobot(id) ? thread.run(rc.senseRobot(id)) : thread.run(null);
                if (done) {
                    if (thread.isSuccess()) {
                        freeBots[freeBotCount] = id;
                        freeBotCount++;
                    }
                    threads[i] = null;
                }
            }
        }

        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                continue;
            }

            ObjectiveThread thread = threads[i];
            thread.find(allies, taken);
        }
    }

}
