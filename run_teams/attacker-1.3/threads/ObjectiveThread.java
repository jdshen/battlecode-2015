package attacker1_3.threads;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import attacker1_3.controllers.Controller;

/**
 * Created by jdshen on 1/7/15.
 */
public abstract class ObjectiveThread {
    public final Controller c;
    public final RobotController rc;
    public int id = -1;
    protected boolean success;
    protected boolean done;

    public ObjectiveThread(Controller c) {
        this.c = c;
        this.rc = c.rc;
        success = false;
        done = false;
    }

    /**
     * Track a bot, issue commands, returns true if the thread is closed
     */
    public abstract boolean run(RobotInfo info) throws GameActionException;

//TODO change this to take in robots arranged by type
    /**
     * Find a bot for the objective thread, always called after run
     * Should alter parameters with which bot it took if any
     * Should also execute any code itself if it needs to issue a command
     */
    public abstract void find(RobotInfo[] bots, boolean[] taken) throws GameActionException;

    /**
     * Thread completed successfully
     * @return
     */
    public boolean isSuccess() {
        return done && success;
    }

    /**
     * Thread terminated unsucessfully
     * @return
     */
    public boolean isFailed() {
        return done && !success;
    }

    /**
     * Forcibly close a thread
     */
    public abstract void close() throws GameActionException;
}
