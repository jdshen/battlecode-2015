package attacker1_3;

import battlecode.common.RobotType;

/**
 * Created by jdshen on 1/6/15.
 */
public class OtherGameConstants {
    public static final double DIAGONAL_FACTOR = 1.4;
    public static final int TOWER_SPLASH_RANGE = 34; // 25 + 9
    public static final int TOWER_NEARBY_RANGE = 100; // 36
    public static final int SUPPLY_TRANSFER_BYTECODE = 500;
    public static final int SENSE_NEARBY_BYTECODE = 100;
    public static final int MAX_MAP_OFFSET = 32000;
    public static final int ROBOT_MAX_ID = 32001;
    public static final int MAX_UNITS_TYPE = 500; // MAX UNITS OF A TYPE

    public static final int TANK_SPLASH_RADIUS_SQ = 25; // tank radius + boundary of buffer
    public static final int TANK_UNSPLASH_RADIUS_SQ = 7; // tank radius - boundary of buffer

    public static final int BASHER_RUSH_RADIUS_SQ = RobotType.TANK.attackRadiusSquared; // MAX RAD
}
