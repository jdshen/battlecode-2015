package attacker4_1.messaging;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;

/**
 * Created by kevin on 1/21/15.
 */
public class KillMessager {
    public int channel = MessagingConstants.KILL_CHANNEL_START;
    private RobotController rc;

    /**
     * Initiliazes Messager
     * @param rc Robot Controller
     * */
    public KillMessager(RobotController rc){
        this.rc = rc;
    }

    /**
     * Always make sure update is done. Or else broadcasts may override.
     * Checks channels in a rolling fashion.
     * No check against channel number going too high, but it's limited in theory.
     * This update does nothing with data
     * @throws GameActionException
     */
    public void updatePass() throws GameActionException {
        int data = rc.readBroadcast(channel);
        while (data != 0) {
            channel++;
            data = rc.readBroadcast(channel);
        }
    }

    /**
     * Broadcast a kill
     * @throws GameActionException
     */
    public void broadcastKill(int id) throws GameActionException{
        rc.broadcast(channel, id);
        //manually update receiver
        channel ++;
    }

    /**
     * Broadcast a kill
     * @throws GameActionException
     */
    public void broadcastKill(RobotInfo info) throws GameActionException{
        int id = info.ID;
        rc.broadcast(channel, id);
        //manually update receiver
        channel ++;
    }
}
