package attacker4_1.actors;

import battlecode.common.*;
import attacker4_1.controllers.Controller;
import attacker4_1.messaging.KillMessager;
import attacker4_1.messaging.MessageType;
import attacker4_1.messaging.MessagingConstants;
import attacker4_1.messaging.RobotMessager;

/**
 * Attacker for Towers
 */
public class TowerAttacker extends Actor{

    private RobotMessager idmessager;
    private KillMessager kill;

    public TowerAttacker(Controller c) {
        super(c);
        kill = new KillMessager(rc);
        idmessager = new RobotMessager(c, c.id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        //clear old channel status TODO this is somewhat wasteful
        idmessager.clearChannel();
        kill.updatePass();
        if(!attack) {
            return new Action(false, false);
        }
        //get enemies in range
        RobotInfo[] enemies = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }
        // alert HQ that tower is engaging enemy
        idmessager.broadcastMsg(MessageType.NEARBY_ENEMY, rc.getLocation());

        double attackPower = c.attackPower;

        //look for best target
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        RobotInfo bestEnemy = enemy;
        // get number of turns to kill enemy
        int turns = (int) (enemy.health / attackPower + .999);
        // get enemy damage rate
        double damageRate = type.attackPower / (0.01+type.attackDelay);
        double score = turns/ damageRate;

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / attackPower + .999);
            // get enemy damage rate
            damageRate = type.attackPower / (0.01+type.attackDelay);
            double newScore = turns/damageRate;
            //switch targets
            if(score > newScore){
                score = newScore;
                bestEnemy = enemy;
            }
        }

        rc.attackLocation(bestEnemy.location);

        if(bestEnemy.health <= attackPower && bestEnemy.type != RobotType.MISSILE){
            kill.broadcastKill(bestEnemy.ID);
        }

        return new Action(false, true);
    }
}
