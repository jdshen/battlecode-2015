package attacker4_1.threads;

import battlecode.common.*;
import attacker4_1.controllers.Controller;
import attacker4_1.util.FastLocSet;

/**
 * Created by jdshen on 1/10/15.
 */
public class BuildingLocator {
    private final static int MAX_STATES = 20000; // 14400
    private final RobotController rc;
    private final Controller c;
    private final MapLocation here;

    private FastLocSet seen;

    public MapLocation[] queue;
    public int queueStart;
    public int queueEnd;

    public BuildingLocator(Controller c) {
        this.c = c;
        this.rc = c.rc;
        this.here = rc.getLocation();
        queueStart = 0;
        queueEnd = 0;
        this.seen = new FastLocSet();
        queue = new MapLocation[MAX_STATES];
        MapLocation[] banned = MapLocation.getAllMapLocationsWithinRadiusSq(here, 2);
        for (int i = banned.length - 1; i >= 0; i--) {
            seen.add(banned[i]);
        }

        MapLocation[] start = MapLocation.getAllMapLocationsWithinRadiusSq(here, 8);
        for (int i = start.length - 1; i >= 0; i--) {
            if (!seen.contains(start[i])) {
                queue[queueEnd++] = start[i];
                seen.add(start[i]);
            }
        }
    }

    public MapLocation next(RobotType type) {
        int num = Clock.getBytecodeNum();
        RobotController rc = this.rc;
        MapLocation hq = c.teamhq;
        // localize
        int queueStart = this.queueStart;
        int queueEnd = this.queueEnd;

        // localize pointers
        MapLocation[] queue = this.queue;

        // seen, hash
        boolean[][] seen = this.seen.has;
        int HASH = FastLocSet.HASH;
        int OFFSET = FastLocSet.OFFSET;

        // localize statics
        Direction NORTH = Direction.NORTH;
        Direction SOUTH = Direction.SOUTH;
        Direction EAST = Direction.EAST;
        Direction WEST = Direction.WEST;
        Direction NORTH_EAST = Direction.NORTH_EAST;
        Direction NORTH_WEST = Direction.NORTH_WEST;
        Direction SOUTH_EAST = Direction.SOUTH_EAST;
        Direction SOUTH_WEST = Direction.SOUTH_WEST;

        while (queueStart < queueEnd) {
            MapLocation loc = queue[queueStart++]; // pop

            // add next locs
            Direction dir = loc.directionTo(hq).opposite();
            {
                MapLocation next = loc.add(dir);
                if (!seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH]) {
                    queue[queueEnd++] = next;
                    seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH] = true;
                }
            }

            {
                MapLocation next = loc.add(dir.rotateLeft());
                if (!seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH]) {
                    queue[queueEnd++] = next;
                    seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH] = true;
                }
            }

            {
                MapLocation next = loc.add(dir.rotateRight());
                if (!seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH]) {
                    queue[queueEnd++] = next;
                    seen[(next.x + OFFSET) % HASH][(next.y + OFFSET) % HASH] = true;
                }
            }

            if (!rc.isPathable(type, loc)) {
                continue;
            }

            boolean ne = rc.isPathable(type, loc.add(NORTH_EAST));
            boolean nw = rc.isPathable(type, loc.add(NORTH_WEST));
            boolean se = rc.isPathable(type, loc.add(SOUTH_EAST));
            boolean sw = rc.isPathable(type, loc.add(SOUTH_WEST));

            if ((!rc.isPathable(type,loc.add(NORTH)) && (ne || nw))
                || (!rc.isPathable(type,loc.add(SOUTH)) && (se || sw))
                || (!rc.isPathable(type,loc.add(EAST)) && (se || ne))
                || (!rc.isPathable(type,loc.add(WEST)) && (nw || sw))
            ) {
                continue;
            }

            this.queueStart = queueStart;
            this.queueEnd = queueEnd;

            rc.setIndicatorString(1, Clock.getBytecodeNum() - num + ",");
            return loc;
        }

        // should never reach
        return null;
    }
}
