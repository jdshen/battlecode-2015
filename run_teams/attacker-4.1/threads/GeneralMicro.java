package attacker4_1.threads;

import battlecode.common.*;
import attacker4_1.OtherGameConstants;
import attacker4_1.controllers.Controller;
import attacker4_1.messaging.CommandMessager;
import attacker4_1.messaging.DirectionMap;
import attacker4_1.messaging.MessageType;
import attacker4_1.messaging.MessagingConstants;

/**
 * Created by jdshen on 1/11/15.
 */
public class GeneralMicro {
    private RobotController rc;
    private Controller c;
    private RobotInfoManager allies;

    private MapLocation attackPoint;
    private int attackCycle;
    private MessageType attackCommand;

    private MapLocation avoid;
    private MapLocation harassPoint;
    private int harassCycle;
    public MessageType harassCommand;

    private MapLocation defendPoint;
    private MessageType defendCommand;

    private CommandMessager messager;

    private int[] lastMessage;

    private DirectionMap map;

    public GeneralMicro(Controller c, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;

        this.allies = allies;

        messager = new CommandMessager(c);

        attackPoint = c.enemyhq;
        attackCycle = 100;
        attackCommand = MessageType.HARASS;

        harassPoint = c.enemyhq;
        harassCycle = 100;
        harassCommand = MessageType.SCOUT;

        defendPoint = c.enemyhq;
        defendCommand = MessageType.HARASS;

        lastMessage = new int[OtherGameConstants.ROBOT_MAX_ID];

        map = new DirectionMap(c, MessagingConstants.BFS_1_CHANNEL_START);
    }

    public void calcAttackLocation() {
        int roundGoIn = rc.getRoundLimit() - 250;
        if (attackCycle == 0 || Clock.getRoundNum() >= roundGoIn) {
            attackCycle = 60;
        } else {
            attackCycle--;
            return;
        }

        int tanks = allies.sizes[RobotType.TANK.ordinal()];
        int launchers = allies.sizes[RobotType.LAUNCHER.ordinal()];
        tanks = Math.max(tanks, launchers * 8 / 5);

        MapLocation teamhq = c.teamhq;
        MapLocation enemyhq = c.enemyhq;

        RobotInfo[] enemies = rc.senseNearbyRobots(c.teamhq, Controller.MAX_DIAGONAL_SQ, c.enemy);

        if ((tanks > 15 && enemies.length < 7) || Clock.getRoundNum() >= roundGoIn) {
            if (tanks > 25 || Clock.getRoundNum() >= roundGoIn) {
                attackCommand = MessageType.DESTROY;
            } else {
                attackCommand = MessageType.ASSAULT;
            }

            MapLocation[] locs = rc.senseEnemyTowerLocations();
            if (locs.length == 0) {
                attackPoint = enemyhq;
                return;
            }

            int maxDist = 0;
            MapLocation target = null;
            for (int i = locs.length - 1; i >= 0; i--) {
                int dist = locs[i].distanceSquaredTo(enemyhq);
                if (dist > maxDist) {
                    maxDist = dist;
                    target = locs[i];
                }
            }

            attackPoint = target;
            return;
        }

        attackCommand = MessageType.HARASS;

        if (enemies.length > 0) {
            MapLocation closest = null;
            int minDist = Controller.MAX_DIAGONAL_SQ;
            int num = Clock.getBytecodeNum();

            for (int i = Math.min(enemies.length, 15) - 1; i >= 0; i--) {
                RobotType type = enemies[i].type;
                switch(type) {
                    // ignore
                    case SUPPLYDEPOT:
                    case TECHNOLOGYINSTITUTE:
                    case BARRACKS:
                    case HELIPAD:
                    case TRAININGFIELD:
                    case TANKFACTORY:
                    case MINERFACTORY:
                    case HANDWASHSTATION:
                    case AEROSPACELAB:
                    case BEAVER:
                    case COMPUTER:
                    case MINER:
                    case HQ:
                    case TOWER:
                    case DRONE:
                        continue;

                    // lets go
                    case SOLDIER:
                    case BASHER:
                    case TANK:
                    case COMMANDER:
                    case LAUNCHER:
                    case MISSILE:
                        break;
                }

                MapLocation loc = enemies[i].location;
                int dist = loc.distanceSquaredTo(teamhq) + loc.distanceSquaredTo(attackPoint);
                if (dist < minDist) {
                    closest = loc;
                    minDist = dist;
                }
            }

            if (closest == null) {
                MapLocation[] locs = rc.senseEnemyTowerLocations();
                if (locs.length == 0) {
                    attackPoint = enemyhq;
                    return;
                }

                int maxDist = 0;
                MapLocation target = null;
                for (int i = locs.length - 1; i >= 0; i--) {
                    int dist = locs[i].distanceSquaredTo(enemyhq);
                    if (dist > maxDist) {
                        maxDist = dist;
                        target = locs[i];
                    }
                }

                attackPoint = target;
                return;
            }

            attackPoint = closest;
            return;
        }

        MapLocation[] alliedTowers = rc.senseTowerLocations();
        if (alliedTowers.length > 0) {
            attackPoint = alliedTowers[0];
            return;
        }
    }

    public void calcAvoidLocation(RobotInfo[] enemies)  throws GameActionException {
        for (int i = Math.min(enemies.length, 10) - 1; i >= 0; i--) {
            RobotType type = enemies[i].type;

            switch(type) {
                // avoid
                case HQ:
                case TOWER:
                case SOLDIER:
                case BASHER:
                case TANK:
                case COMMANDER:
                case LAUNCHER:
                case MISSILE:
                    avoid = enemies[i].location;
                    return;
            }
        }
    }

    public void calcHarassLocation() throws GameActionException {
        if (attackCommand != MessageType.HARASS && Clock.getRoundNum() >= rc.getRoundLimit() - 250) {
            harassCommand = attackCommand;
            harassPoint = attackPoint;
            return;
        }

        if (harassCycle == 0) {
            harassCycle = 3;
        } else {
            harassCycle--;
            return;
        }

        // magic constant
        RobotInfo[] enemies = (harassCommand == MessageType.HARASS) ?
            rc.senseNearbyRobots(harassPoint, 100, c.enemy) :
            rc.senseNearbyRobots(c.teamhq, Controller.MAX_DIAGONAL_SQ, c.enemy);

        MapLocation closest = null;
        int minDist = Controller.MAX_DIAGONAL_SQ;

        calcAvoidLocation(enemies);

        for (int i = Math.min(enemies.length, 15) - 1; i >= 0; i--) {
            RobotType type = enemies[i].type;
            switch(type) {
                // pursue
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case BEAVER:
                case COMPUTER:
                case MINER:
                    break;

                // nope
                case DRONE:
                case HQ:
                case TOWER:
                case SOLDIER:
                case BASHER:
                case TANK:
                case COMMANDER:
                case LAUNCHER:
                case MISSILE:
                    continue;
            }

            MapLocation loc = enemies[i].location;
            if (avoid != null && loc.distanceSquaredTo(avoid) <= 35) {
                continue;
            }

            if (loc.distanceSquaredTo(c.enemyhq) <= 55) {
                continue;
            }

            int dist = loc.distanceSquaredTo(harassPoint) - loc.distanceSquaredTo(c.enemyhq) / 4;
            if (dist < minDist) {
                closest = loc;
                minDist = dist;
            }
        }

        if (closest == null) {
                harassCommand = MessageType.SCOUT;
                return;
        }

        harassPoint = closest;
        harassCommand = MessageType.HARASS;
    }

    public void calcDefendLocation() throws GameActionException {
        if (attackCommand != MessageType.HARASS && Clock.getRoundNum() >= rc.getRoundLimit() - 250) {
            defendCommand = attackCommand;
            defendPoint = attackPoint;
            return;
        }

        // magic constant
        RobotInfo[] enemies = (defendCommand == MessageType.HARASS) ?
            rc.senseNearbyRobots(defendPoint, 100, c.enemy) :
            rc.senseNearbyRobots(c.teamhq, Controller.MAX_DIAGONAL_SQ, c.enemy);

        MapLocation closest = null;
        int minDist = Controller.MAX_DIAGONAL_SQ;

        for (int i = Math.min(enemies.length, 15) - 1; i >= 0; i--) {
            RobotType type = enemies[i].type;
            switch(type) {
                // pursue
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case BEAVER:
                case COMPUTER:
                case MINER:
                case DRONE:
                    // pursue drones as well
                    break;

                // nope
                case HQ:
                case TOWER:
                case SOLDIER:
                case BASHER:
                case TANK:
                case COMMANDER:
                case LAUNCHER:
                case MISSILE:
                    continue;
            }

            MapLocation loc = enemies[i].location;
            if (avoid != null && loc.distanceSquaredTo(avoid) <= 35) {
                continue;
            }

            if (loc.distanceSquaredTo(c.enemyhq) <= 55) {
                continue;
            }

            if (loc.distanceSquaredTo(attackPoint) <= 35) {
                continue;
            }

            int dist = loc.distanceSquaredTo(defendPoint) - loc.distanceSquaredTo(c.enemyhq) / 4;
            if (dist < minDist) {
                closest = loc;
                minDist = dist;
            }
        }

        if (closest == null) {
            defendPoint = attackPoint;
            return;
        }

        defendPoint = closest;
        defendCommand = MessageType.HARASS;

    }

    public void run(boolean[] taken) throws GameActionException {
        calcAttackLocation();
        calcHarassLocation();
        calcDefendLocation();

        MapLocation attackPoint = this.attackPoint;
        rc.setIndicatorDot(attackPoint, 255, 0, 255);

        rc.setIndicatorDot(harassPoint, 255, 255, 0);
        rc.setIndicatorDot(defendPoint, 255, 255, 255);


        map.broadcastBFSRequest(attackPoint);

        // attack msg
        {
            int msg = messager.computeMessage(attackCommand, attackPoint);
            broadcast(RobotType.BASHER, taken, msg);
            broadcast(RobotType.LAUNCHER, taken, msg);
            broadcast(RobotType.SOLDIER, taken, msg);
            broadcast(RobotType.TANK, taken, msg);
        }

        // harass msg
        {
            int msg = messager.computeMessage(harassCommand, harassPoint);
            broadcast(RobotType.DRONE, taken, msg);
        }

        // defend msg
        {
            int msg = messager.computeMessage(defendCommand, defendPoint);
            broadcast(RobotType.COMMANDER, taken, msg);
        }
    }

    public void broadcast(RobotType type, boolean[] taken, int msg) throws GameActionException {
        broadcast(allies.bots[type.ordinal()], allies.sizes[type.ordinal()], taken, msg);
    }

    public void broadcast(int[] ids, int size, boolean[] taken, int msg) throws GameActionException {
        // localize
        int[] lastMessage = this.lastMessage;
        int offset = MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT;
        int ID_CHAN_MOD = messager.ID_CHAN_MOD;
        int ID_CHECK_MOD = messager.ID_CHECK_MOD;
        RobotController rc = this.rc;

        for (int i = size - 1; i >= 0; i--) {
            if (Clock.getBytecodeNum() > 9000) {
                return;
            }
            int id = ids[i];
            if (taken[id]) {
                continue;
            }

            if (lastMessage[id] != msg) {
                rc.broadcast(offset + id % ID_CHAN_MOD, (id % ID_CHECK_MOD) + msg);
                lastMessage[id] = msg;
            }
        }
    }

}
