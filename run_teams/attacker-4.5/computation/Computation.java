package attacker4_5.computation;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker4_5.controllers.Controller;

public abstract class Computation {
    public final Controller c;
    public final RobotController rc;

    public Computation(Controller c) {
        this.c = c;
        this.rc = c.rc;
    }

    public abstract boolean compute(int limit) throws GameActionException;
}
