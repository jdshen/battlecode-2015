package attacker4_5.computation.symmetry;

import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/23/15.
 */
public class Rotation implements Symmetry {
    private final int centerX2;
    private final int centerY2;

    public Rotation(int centerX2, int centerY2) {
        this.centerX2 = centerX2;
        this.centerY2 = centerY2;
    }

    @Override
    public SymmetryType getType() {
        return SymmetryType.ROTATION;
    }

    @Override
    public MapLocation get(MapLocation loc) {
        return new MapLocation(centerX2 - loc.x, centerY2 - loc.y);
    }

    @Override
    public MapLocation get(int x, int y) {
        return new MapLocation(centerX2 - x, centerY2 - y);
    }
}
