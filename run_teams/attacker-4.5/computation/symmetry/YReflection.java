package attacker4_5.computation.symmetry;

import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/23/15.
 */
public class YReflection implements Symmetry {
    private int centerX2;

    public YReflection(int centerX2) {
        this.centerX2 = centerX2;
    }

    @Override
    public SymmetryType getType() {
        return SymmetryType.Y_REFLECTION;
    }

    @Override
    public MapLocation get(MapLocation loc) {
        return new MapLocation(centerX2 - loc.x, loc.y);
    }

    @Override
    public MapLocation get(int x, int y) {
        return new MapLocation(centerX2 - x, y);
    }
}
