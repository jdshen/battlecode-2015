package attacker4_5.actors;

import battlecode.common.*;
import attacker4_5.util.AttackerLookups;
import attacker4_5.OtherGameConstants;
import attacker4_5.controllers.Controller;
import attacker4_5.movement.Bugger;
import attacker4_5.movement.Mover;

/**
 * Makes fight decisions for commander
 *
 * Decides step ins
 *
 * should always try to stay at 8-10 range away
 *
 * Assumes we have flash
 *
 */
public class CommanderFighter extends Actor{
    public final int MIN_STEPIN_HEALTH = 160;
    public final int MIN_RETREAT_HEALTH = 80;

    private AttackerLookups lookups;
    private GenericAttacker attacker;
    private HeavyHandsAttacker heavyAttacker;

    private Direction[] intToDirs = Direction.values();
    private boolean avoidTowers;
    private MapLocation towerLoc;

    // will ignore tower splash dmg when calculating damage if off
    private boolean avoidHQ;
    private MapLocation target;
    private Bugger bugger;
    private Bugger retreater;
    private MapLocation retreatLoc;
    private Mover mover;

    private boolean hunting;
    private boolean heavyHands;

    public CommanderFighter(Controller c, MapLocation targetLoc) {
        super(c);
        this.attacker = new GenericAttacker(c);
        this.heavyAttacker = new HeavyHandsAttacker(c);
        this.lookups = new AttackerLookups();
        this.avoidHQ = true;
        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
        this.target = targetLoc;
        this.bugger = new Bugger(c);
        this.mover = new Mover(c);
        bugger.startBug(target);
        this.retreater = new Bugger(c);
        this.retreatLoc = c.teamhq;
        retreater.endBug();
        this.hunting = false;
    }

    public void setAvoidTowers(boolean avoidTowers) {
        this.avoidTowers = avoidTowers;
    }

    public void ignoreTower(MapLocation towerLoc) {
        this.towerLoc = towerLoc;
    }

    public void setAvoidHQ(boolean avoidHQ) {
        this.avoidHQ = avoidHQ;
    }

    public void setRetreatLoc(MapLocation retreatLoc) {
        if (this.retreatLoc != retreatLoc) {
            this.retreatLoc = retreatLoc;
            if (retreater.bugging){
                retreater.startBug(retreatLoc);
            }
        }
    }

    public void setTarget(MapLocation target){
        hunting = false;
        if (!this.target.equals(target)) {
            this.target = target;
            if (bugger.bugging) {
                bugger.startBug(target);
            }
        }
    }

    public void setHuntTarget(MapLocation target){
        hunting = true;
        if (!this.target.equals(target)) {
            this.target = target;
            if (bugger.bugging) {
                bugger.startBug(target);
            }
        }
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        // decide stuff
        if(!move && !attack){
            return new Action(false, false);
        }
        if(!heavyHands) {
            heavyHands = rc.hasLearnedSkill(CommanderSkillType.HEAVY_HANDS);
        }
        RobotInfo[] enemies = rc.senseNearbyRobots(26, c.enemy);
        int numEnemies = enemies.length;
        if(numEnemies == 0){
            if(!move){
                return new Action(false, false);
            }
            return actNoEnemies();
        }

        RobotInfo[] enemiesInRange = rc.senseNearbyRobots(c.attackRadiusSquared, c.enemy);
        boolean attacked;
        // attack if possible
        if(heavyHands){
            // target based on attack delay/damage, but hit commanders first
            Action act = heavyAttacker.actWithEnemies(move, attack, enemiesInRange);
            attacked = act.attacked;
        }
        else{
            Action act = attacker.actWithEnemies(move, attack, enemiesInRange);
            attacked = act.attacked;
        }
        if(!move){
            return new Action(false, attacked);
        }

        return new Action(actEnemies(enemies, enemiesInRange), attacked);

    }

    // action when enemies around
    public boolean actEnemies(RobotInfo[] enemies, RobotInfo[] enemiesInRange) throws GameActionException {
        if(hunting){
            if(actHunt(rc.senseNearbyRobots(52, c.enemy), enemiesInRange)){
                return true;
            }
        }

        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;
        int numDirs = intToDirs.length;
        int numEnemies = enemies.length;

        // how much dmg can be dealt before we can move away from square
        int[] damageInDir = new int[numDirs];
        // places we are willing to move - should not be in 8 range of a unit
        boolean[] moveOptions = new boolean[numDirs];
        boolean[] nearEnemy = new boolean[numDirs];
        updateInitialMoveOptions(damageInDir, moveOptions);

        int idx;
        int enemyCount = 0;
        int damage = 0;
        int attackRadius;
        RobotInfo en;
        MapLocation there;
        Direction[] attackedDirs;
        for (int i = numEnemies - 1; i >= 0; i --) {
            en = enemies[i];
            there = en.location;
            RobotType enType = en.type;
            attackRadius = enType.attackRadiusSquared;
            switch(enType){
                case HQ:
                    damage = 24; // assuming that if we went in, hq lost tower buffs
                    enemyCount += 24;
                    break;
                case TOWER:
                    damage = 16;
                    enemyCount += 12;
                    break;
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case COMPUTER:
                    continue;
                case BEAVER:
                    damage = 4;
                    enemyCount += 2;
                    break;
                case SOLDIER:
                    damage = 8;
                    enemyCount += 6;
                    break;
                case BASHER:
                    damage = 8;
                    enemyCount += 9;
                    break;
                case MINER:
                    damage = 3;
                    enemyCount += 2;
                    break;
                case DRONE:
                    damage = 8;
                    enemyCount += 7;
                    break;
                case TANK:
                    damage = 20;
                    enemyCount += 21;
                    break;
                case COMMANDER:
                    damage = 20;
                    enemyCount += 24;
                    break;
                case LAUNCHER:
                    damage = 1; // enough to drive away when low health
                    attackRadius = 15;
                    enemyCount += 36;
                    break;
                case MISSILE:
                    damage = 20; // avoid missiles moar
                    attackedDirs = lookups.getDirs(2, there.x - hereX, there.y - hereY);
                    for (int j = attackedDirs.length - 1; j >= 0; j--) {
                        idx = attackedDirs[j].ordinal();
                        damageInDir[idx] = damageInDir[idx] + damage;
                    }
                    break;
                default:
                    damage = 0;
                    break;
            }
            if(heavyHands){
                if(en.weaponDelay > 1){
                    damage = damage/2;
                }
            }
            if (attackRadius < 8){
                attackedDirs = lookups.getDirs(5, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    damageInDir[idx] = damageInDir[idx] + damage;

                    nearEnemy[idx] = true;
                }
            } else {
                attackedDirs = lookups.getDirs(attackRadius, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    damageInDir[idx] = damageInDir[idx]   + damage;
                }
                attackedDirs = lookups.getDirs(5, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    nearEnemy[idx] = true;
                }
            }
        }

        int curDamage = damageInDir[Direction.OMNI.ordinal()];
        //RETREAT if under threshold and under attack. Otherwise retreat if less allies
        if(rc.getHealth() < MIN_RETREAT_HEALTH){
            if(curDamage == 0){
                int numAllies = attacker.getAllyCount(25, here);
                if(numAllies + 12 >= enemyCount){
                    return false;
                }
            }
            return retreat(enemies, damageInDir, moveOptions);
        }

        if(curDamage >= rc.getHealth()){
            return retreat(enemies, damageInDir, moveOptions);
        }

        if(enemiesInRange.length > 0){
            return actFightRange(enemies, damageInDir, moveOptions, enemyCount, nearEnemy);
        }

        return actStepIn(enemies, damageInDir, moveOptions, enemyCount);

    }

    // returns true if makes a mvoe, false if it does nothing
    // returns false if any tanks
    public boolean actHunt(RobotInfo[] enemies, RobotInfo[] enemiesInRange) throws GameActionException {
        // Check enemies has no tanks/launchers/commanders and we can solo them
        if(rc.getHealth() <= MIN_RETREAT_HEALTH) {
            return false;
        }
        RobotController rc = this.rc;

        int numEns = enemies.length;
        MapLocation here = rc.getLocation();
        int enemyCount = 0;
        int droneCount = 0;
        MapLocation[] flashTargets = new MapLocation[numEns];
        int numFlashTargets = 0;
        RobotInfo closest = null;
        MapLocation there;
        int bestDistance = 10000;
        for(int i = enemies.length - 1; i >= 0; i --){
            RobotInfo en = enemies[i];
            switch(en.type){
                case HQ:
                case TOWER:
                case TANK:
                case COMMANDER:
                case LAUNCHER:
                case MISSILE:
                case BASHER:
                    return false;
                case MINER:
                case BEAVER:
                    there = en.location;
                    flashTargets[numFlashTargets] = there;
                    numFlashTargets ++;
                    enemyCount += 2;
                    if(bestDistance > 3){
                        int tmp = here.distanceSquaredTo(there);
                        if(tmp < bestDistance){
                            bestDistance = tmp;
                            closest = en;
                        }
                    }
                    break;
                case DRONE:
                    there = en.location;
                    flashTargets[numFlashTargets] = there;
                    numFlashTargets++;
                    enemyCount += 6;
                    droneCount ++;
                    if(bestDistance > 3){
                        int tmp = here.distanceSquaredTo(there);
                        if(tmp < bestDistance){
                            bestDistance = tmp;
                            closest = en;
                        }
                    }
                    break;
                case SOLDIER:
                    enemyCount += 6;
                default:
                    there = en.location;
                    if(bestDistance > 3){
                        int tmp = here.distanceSquaredTo(there);
                        if(tmp < bestDistance){
                            bestDistance = tmp;
                            closest = en;
                        }
                    }
                    break;
            }
        }
        if(enemyCount > 24) {
            return false;
        }
        if(closest == null){
            return false;
        }

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for (int i = numTowers - 1; i >= 0; i --) {
            there = towers[i];
            if(here.distanceSquaredTo(there) < OtherGameConstants.FLASH_TOWER_SPLASH_RANGE){
                needCheckTowers[numCheckTowers] = there;
                numCheckTowers ++;
            }
        }
        // if enemies in range, fight
        if(enemiesInRange.length > 0) {
            if(bestDistance <= 5){
                if(enemiesInRange.length >  2){
                    // up in our grill
                    boolean[] canMoves = mover.getCanMove(avoidTowers, needCheckTowers, avoidHQ, numCheckTowers);
                    backUp(canMoves, new int[canMoves.length], enemiesInRange);
                }
                return true; // dont do anynthing. we got this.
            }
            MapLocation closestLoc = closest.location;
            if(!inTowerRange(closestLoc, needCheckTowers, numCheckTowers)) {
                Direction thereToHere = closestLoc.directionTo(here);
                Direction dir = here.directionTo(closestLoc.add(thereToHere));
                if (rc.canMove(dir)) {
                    rc.move(dir);
                    bugger.endBug();
                    retreater.endBug();
                    return true;
                }
                if (rc.getFlashCooldown() < 1) {
                    MapLocation flashTarget = closestLoc.add(thereToHere);
                    if(rc.isPathable(c.type, flashTarget)) {
                        rc.castFlash(flashTarget);
                        bugger.endBug();
                        retreater.endBug();
                        return true;
                    }
                    flashTarget = closestLoc.add(thereToHere.rotateLeft());
                    if(rc.isPathable(c.type, flashTarget)) {
                        rc.castFlash(flashTarget);
                        bugger.endBug();
                        retreater.endBug();
                        return true;
                    }
                    flashTarget = closestLoc.add(thereToHere.rotateRight());
                    if(rc.isPathable(c.type, flashTarget)) {
                        rc.castFlash(flashTarget);
                        bugger.endBug();
                        retreater.endBug();
                        return true;
                    }
                }
            }
            boolean[] canMoves = mover.getCanMove(avoidTowers, needCheckTowers, avoidHQ, numCheckTowers);
            if(mover.makeSafeMove(here.directionTo(closestLoc),canMoves, false)){
                bugger.endBug();;
                retreater.endBug();
            }
            return true; // Stick with enemy.
        }



        // if enemies out of range, try to flash in.
        if(rc.getFlashCooldown() < 1){
            // flash at enemies
            Direction[] intToDirs = Direction.values();
            int numDirs = intToDirs.length;
            for(int i = numFlashTargets - 1; i >= 0; i --) {
                MapLocation targetLoc = flashTargets[i];
                boolean skip = false;
                for(int j = numDirs - 1; j >= 0; j --){
                    Direction dir = intToDirs[i];
                    if(dir == Direction.OMNI || dir == Direction.NONE){
                        continue;
                    }
                    if(!rc.canSenseLocation(targetLoc.add(dir))){
                        skip = true;
                        break;
                    }
                }
                if(skip){
                    continue;
                }
                if(droneCount >2){
                    RobotInfo[] drones = rc.senseNearbyRobots(targetLoc, 4, c.enemy);
                    int numDrones = 0;
                    for(int j = drones.length - 1; j >= 0; j--){
                        if(drones[j].type == RobotType.DRONE || drones[j].type == RobotType.SOLDIER) {
                            numDrones ++;
                        }
                    }
                    if(numDrones > 2){
                        continue;
                    }
                }
                if(inTowerRange(targetLoc, needCheckTowers, numCheckTowers)){
                   continue;
                }
                // SWITCH CASE TO GET LOCS TO TRY
                MapLocation flashTarget;
                Direction hereToThere = here.directionTo(targetLoc);
                Direction thereToHere = targetLoc.directionTo(here);
                switch(here.distanceSquaredTo(targetLoc)){
                    case 26:
                        flashTarget = targetLoc.add(thereToHere,2);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = here.add(hereToThere,3);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = targetLoc.add(thereToHere,3);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 25:
                        if(hereToThere.isDiagonal()){
                            MapLocation tmp = targetLoc.add(thereToHere,2);

                            flashTarget = tmp.add(hereToThere.rotateLeft());
                            if(rc.isPathable(c.type, flashTarget)) {
                                rc.castFlash(flashTarget);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                            flashTarget = tmp.add(hereToThere.rotateRight());
                            if(rc.isPathable(c.type, flashTarget)) {
                                rc.castFlash(flashTarget);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                            if(rc.isPathable(c.type, tmp)) {
                                rc.castFlash(tmp);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                        }else{
                            MapLocation tmp = targetLoc.add(thereToHere,2);
                            if(rc.isPathable(c.type, tmp)) {
                                rc.castFlash(tmp);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                            flashTarget = tmp.add(hereToThere.rotateLeft().rotateLeft());
                            if(rc.isPathable(c.type, flashTarget)) {
                                rc.castFlash(flashTarget);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                            flashTarget = tmp.add(hereToThere.rotateRight().rotateRight());
                            if(rc.isPathable(c.type, flashTarget)) {
                                rc.castFlash(flashTarget);
                                bugger.endBug();
                                retreater.endBug();
                                return true;
                            }
                        }
                        break;
                    case 20:
                        MapLocation tmp = targetLoc.add(thereToHere);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = here.add(hereToThere,2);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        Direction tmpDir = tmp.directionTo(here);
                        flashTarget = tmp.add(tmpDir);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = here.add(tmpDir.opposite(), 3);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 18:
                        tmp = targetLoc.add(thereToHere);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateLeft().rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateRight().rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(thereToHere.rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(thereToHere.rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 17:
                        flashTarget = targetLoc.add(thereToHere,1);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = here.add(hereToThere,3);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = targetLoc.add(thereToHere,2);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 16:
                        tmp = targetLoc.add(thereToHere);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateLeft().rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateRight().rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 13:
                        tmp = targetLoc.add(thereToHere);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(thereToHere.rotateLeft().rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(thereToHere.rotateRight().rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    // large numbers
                    case 32:
                        tmp = targetLoc.add(thereToHere,2);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateLeft().rotateLeft());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = tmp.add(hereToThere.rotateRight().rotateRight());
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 34:
                        flashTarget = targetLoc.add(thereToHere, 2);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        flashTarget = here.add(hereToThere, 2);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        break;
                    case 29:
                        tmp = targetLoc.add(thereToHere, 3);
                        if(rc.isPathable(c.type, tmp)) {
                            rc.castFlash(tmp);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                        tmpDir = here.directionTo(tmp);
                        flashTarget = here.add(hereToThere, 2).add(tmpDir);
                        if(rc.isPathable(c.type, flashTarget)) {
                            rc.castFlash(flashTarget);
                            bugger.endBug();
                            retreater.endBug();
                            return true;
                        }
                    default:
                        break;
                }
            }
        }

        MapLocation targetLoc = closest.location;
        for(int i = intToDirs.length - 1; i >= 0; i --){
            Direction dir = intToDirs[i];
            if(dir == Direction.OMNI || dir == Direction.NONE){
                continue;
            }
            if(!rc.canSenseLocation(targetLoc.add(dir))){
                return false;
            }
        }
        boolean[] canMoves = mover.getCanMove(avoidTowers, needCheckTowers, avoidHQ, numCheckTowers);
        // move towards closest
        if (mover.makeSafeMove(here.directionTo(closest.location), canMoves, false)) {
            bugger.endBug();
            retreater.endBug();
            return true;
        }
        return false;
    }

    // Not retreating, not going to die
    public boolean actFightRange(RobotInfo[] enemies, int[] damageInDir, boolean[] moveOptions, int enemyCount, boolean[] nearEnemy) throws GameActionException {
        MapLocation here = rc.getLocation();
        int myScore = 18;
        if(heavyHands){
            myScore += 18;
        }

        int curDamage = damageInDir[Direction.OMNI.ordinal()];
        int numDirs = intToDirs.length;
        if(curDamage == 0){
            return false;
        }

        // check for adjacent enemies, back up if there are
        RobotInfo[] adjEnemies = rc.senseNearbyRobots(2, c.enemy);

        if(adjEnemies.length > 0) {
            for(int i = adjEnemies.length - 1; i >= 0; i --){
                RobotInfo en = adjEnemies[i];
                if(en.type.isBuilding){
                    continue;
                }
                Direction dir = here.directionTo(en.location);
                moveOptions[dir.rotateLeft().ordinal()] = false;
                moveOptions[dir.rotateRight().ordinal()] = false;
            }
            return backUp(moveOptions, damageInDir, enemies);
        }

        // Decide whether to move closer
        // step onto a square if it doesn't make you retreat next turn
        // and if its not near enemies
        for (int i = numDirs - 1; i >= 0; i--){
            if(!moveOptions[i]){
                continue;
            }
            // don't move anywhere with more dmg
            if(damageInDir[i] > curDamage) {
                moveOptions[i] = false;
                continue;
            }
            if(rc.getHealth() - damageInDir[i] <= MIN_RETREAT_HEALTH ||
                    2*damageInDir[i] >= rc.getHealth()){
                moveOptions[i] = false;
                continue;
            }
            if(nearEnemy[i]){
                moveOptions[i] = false;
            }
        }
        if(attacker.getAllyCount(2, here) + myScore < enemyCount){
            return backUp(moveOptions, damageInDir, enemies);
        }
        // if heavy hands, chill. keep doing dat delay
        if(heavyHands){
            return false;
        } // otherwise move up towards target
        if(mover.makeSafeMove(here.directionTo(target), moveOptions, true)){
                bugger.endBug();
                retreater.endBug();
            return true;
        }
        return false;
    }

    public boolean backUp(boolean[] moveOptions, int[] damageInDir, RobotInfo[] enemies) throws GameActionException {
        MapLocation here = rc.getLocation();
        int idx;
        for (int i = enemies.length - 1; i >= 0; i --){
            Direction dir = here.directionTo(enemies[i].location);
            idx = dir.ordinal();
            damageInDir[idx] = damageInDir[idx] + 3;
            idx = dir.rotateLeft().ordinal();
            damageInDir[idx] = damageInDir[idx] + 1;
            idx = dir.rotateRight().ordinal();
            damageInDir[idx] = damageInDir[idx] + 1;
        }
        Direction dir = Direction.OMNI;
        Direction curDir;
        int best = 10000;
        for(int i = moveOptions.length - 1; i >= 0; i --) {
            curDir = intToDirs[i];
            if(moveOptions[i]){
                if(damageInDir[i] < best){
                    dir = curDir;
                }
            }
        }
        if(dir != Direction.OMNI){
            rc.move(dir);
            bugger.endBug();
            retreater.endBug();
            return true;
        }
        return false;
    }

    public boolean actStepIn(RobotInfo[] enemies, int[] damageInDir, boolean[] moveOptions, int enemyCount) throws GameActionException {
        int curDamage = damageInDir[Direction.OMNI.ordinal()];
        int numDirs = intToDirs.length;
        int myScore = 18;
        if(heavyHands){
            switch(enemies.length){
                case 2:
                    RobotType enType = enemies[1].type;
                    if(enType == RobotType.COMMANDER || enType == RobotType.TOWER || enType == RobotType.HQ){
                        break;
                    }
                case 1:
                    enType = enemies[0].type;
                    if(enType != RobotType.COMMANDER && enType != RobotType.TOWER && enType != RobotType.HQ){
                        myScore += 100;
                    }
                    break;
                default:
                    break;
            }
            myScore += 12;
        }
        boolean avoidRange = false;
        // If under attack - probably a tank or launcher
        if(curDamage != 0){
            // decide engage or back up
            avoidRange = avoidRange || enemyCount >= attacker.getAllyCount(24, rc.getLocation()) + myScore;
        }
        else{
            avoidRange = avoidRange || enemyCount >= attacker.getAllyCount(10, rc.getLocation()) + myScore;
            avoidRange = avoidRange || rc.getHealth() <= MIN_STEPIN_HEALTH;
        }


        if (avoidRange) {
            //don't step in range
            for (int i = numDirs - 1; i >= 0; i--) {
                if (!moveOptions[i]) {
                    continue;
                }
                if (damageInDir[i] > 0) {
                    moveOptions[i] = false;
                    continue;
                }
            }
        }
        else {
            // Don't step onto a square if it makes you retreat next turn
            for (int i = numDirs - 1; i >= 0; i--) {
                if (!moveOptions[i]) {
                    continue;
                }
                if (rc.getHealth() - damageInDir[i] <= MIN_RETREAT_HEALTH ||
                        2 * damageInDir[i] >= rc.getHealth()) {
                    moveOptions[i] = false;
                    continue;
                }
            }
        }

        // bug at target
        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(moveOptions);
        if(dir == Direction.OMNI || dir == Direction.NONE){
            return false;
        }
        rc.move(dir);
        retreater.endBug();

        return true;
    }

    public boolean inTowerRange(MapLocation loc, MapLocation[] needCheckTowers, int numCheckTowers){
        int TOWER_RANGE = OtherGameConstants.TOWER_SPLASH_RANGE;
        for (int i = numCheckTowers - 1; i >= 0; i --){
            if(loc.distanceSquaredTo(needCheckTowers[i]) <= TOWER_RANGE){
                return true;
            }
        }
        if(loc.distanceSquaredTo(c.enemyhq) <= OtherGameConstants.HQ_SPLASH_RANGE){
            return true;
        }
        return false;
    }

    public Action actNoEnemies() throws GameActionException {
        //if low on health, just hang out
        if(rc.getHealth() < MIN_STEPIN_HEALTH){
            if(!retreater.bugging){
                retreater.startBug(retreatLoc);
            }
            Direction dir = retreater.bug(mover.getCanMove(true, c.teamhq, true));
            if(rc.canMove(dir)){
                rc.move(dir);
                bugger.endBug();
                return new Action(true, false);
            }
            return new Action(false, false);
        }

        // Hunt down stragglers
        if(hunting) {
            RobotInfo[] enemies = rc.senseNearbyRobots(52, c.enemy);
            if (enemies.length != 0) {
                if (actHunt(enemies, new RobotInfo[0])) {
                    return new Action(true, false);
                }
            }
        }

        if(rc.getLocation().distanceSquaredTo(target) < 5){
            return new Action(false, false);
        }
        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(mover.getCanMove(avoidTowers, towerLoc, avoidHQ));
        if(dir != Direction.OMNI && dir != Direction.NONE){
            rc.move(dir);
            retreater.endBug();
            return new Action(true, false);
        }
        return new Action(false, false);
    }

    public boolean retreat(RobotInfo[] enemies, int[] damageInDir, boolean[] moveOptions) throws GameActionException {

        //RETREAT LOGIC - TRY TO BUG TO RETREAT LOCATION AVOIDING DMG
        // loop through directions, if no damage and can move, move
        for (int i = intToDirs.length - 1; i >= 0; i--){
            if(damageInDir[i] > 1 ){
                moveOptions[i] = false;
            }
        }
        if(!retreater.bugging){
            retreater.startBug(retreatLoc);
        }
        Direction dir = retreater.bug(moveOptions);
        if(rc.canMove(dir)){
            rc.move(dir);
            bugger.endBug();
            return true;
        }

        if(rc.getFlashCooldown() > 0) {
            return false;
        }

        // TODO retreat logic, Flash away if no moves.
        int[] retreatDirs = new int[intToDirs.length];
        int numEnemies = enemies.length;
        RobotInfo en;
        MapLocation there;
        MapLocation here = rc.getLocation();

        // Try flashing towards retreat loc;
        dir = here.directionTo(retreatLoc);
        if(tryFlash(dir, true, true)){
            bugger.endBug();
            retreater.endBug();
            return true;
        }
        if(tryFlash(dir.rotateLeft(), true, false)){
            bugger.endBug();
            retreater.endBug();
            return true;
        }
        if(tryFlash(dir.rotateRight(), true, false)){
            bugger.endBug();
            retreater.endBug();
            return true;
        }

        dir = Direction.OMNI;
        int idx;
        retreatDirs[Direction.OMNI.ordinal()] = 100;
        retreatDirs[Direction.NONE.ordinal()] = 100;
        // find directions "away from enemies"
        for (int i = numEnemies -1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            dir = here.directionTo(there);
            idx = dir.ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 4;
            idx = dir.rotateLeft().ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 2;
            idx = dir.rotateRight().ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 2;
            dir = dir.opposite();
            idx = dir.opposite().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 3;
            idx = dir.rotateLeft().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 1;
            idx = dir.rotateRight().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 1;
        }
        // set dir to the best retreat direction
        idx = 100;
        for(int i = intToDirs.length - 1; i >= 0; i--){
            if(retreatDirs[i] < idx){
                dir = intToDirs[i];
                idx = retreatDirs[i];
            }
        }

        // flash away from enemies;
        if(tryFlash(dir, false, true)){
            return true;
        }

        return false;
    }

    public boolean tryFlash(Direction dir, boolean checkSenseEnemies, boolean rotations) throws GameActionException {
        MapLocation here = rc.getLocation();
        MapLocation ideal = here.add(dir, 2);
        if(!dir.isDiagonal()){
            ideal = ideal.add(dir);
        }
        if(rc.isPathable(c.type, ideal)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(ideal, 15, c.enemy).length == 0) {
                rc.castFlash(ideal);
                return true;
            }

        }

        if(!rotations){
            return false;
        }
        Direction right = dir.rotateRight().rotateRight();
        Direction left = dir.rotateLeft().rotateLeft();

        here = ideal.add(right);
        if(rc.isPathable(c.type, here)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(here, 15, c.enemy).length == 0) {
                rc.castFlash(here);
                return true;
            }
        }
        here = ideal.add(left);
        if(rc.isPathable(c.type, here)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(here, 15, c.enemy).length == 0) {
                rc.castFlash(here);
                return true;
            }
        }
        return false;
    }



    public void updateInitialMoveOptions(int[] damageInDir, boolean[] moveOptions){
        MapLocation here = rc.getLocation();
        Direction dir;
        MapLocation tmpLoc;

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for (int i = numTowers - 1; i >= 0; i--) {
            tmpLoc = towers[i];
            if (here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE) {
                if (tmpLoc.equals(towerLoc)) {
                    continue;
                }
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers++;
            }
        }
        for (int i = moveOptions.length - 1; i >= 0; i--) {
            dir = intToDirs[i];
            if (!rc.canMove(dir)) {
                continue;
            }
            if(!avoidTowers){
                moveOptions[i] = true;
                continue;
            }
            tmpLoc = here.add(dir);
            if(avoidHQ) {
                switch (towers.length) {
                    case 6:
                    case 5:
                        int thereX = c.enemyhq.x;
                        int thereY = c.enemyhq.y;
                        int delX = tmpLoc.x - thereX;
                        if (delX < 0) {
                            delX = -delX;
                        }
                        int delY = tmpLoc.y - thereY;
                        if (delY < 0) {
                            delY = -delY;
                        }
                        if (delX < 7 && delY < 7 && (delX + delY) < 11) {
                            damageInDir[i] = 240;
                            continue;
                        }
                        break; // technically out of sight range of the hq at this point so. ...
                    case 4:
                    case 3:
                    case 2:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                            damageInDir[i] = 36; // TODO technically wrong
                            continue;
                        }
                        break;
                    default:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                            damageInDir[i] = 24;
                            continue;
                        }
                }
            }
            moveOptions[i] = true;

            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    damageInDir[i] = 16;
                    moveOptions[i] = false;
                    break;
                }
            }
        }

    }

}
