package attacker4_5.threads;

import battlecode.common.*;
import attacker4_5.OtherGameConstants;
import attacker4_5.controllers.Controller;
import attacker4_5.messaging.MessagingConstants;

/**
 * Created by jdshen on 1/27/15.
 */
public class EnemyInfoManager {
    private final Controller c;
    private final RobotController rc;
    public int[][] bots; // bot infos, indexed by types
    public RobotInfo[] infoMap;
    public int[] sizes; // current sizes of bots
    public int[] totals; // lifetime totals of bots
    public boolean[] hash;
    public static final int length = RobotType.values().length;
    public int type;

    public int[] killScore; // kills by type

    public boolean[] deleteHash; // id boolean
    public int[] killerType; // map from id to RobotType who killed them

    public int channel;

    public EnemyInfoManager(Controller c) {
        // localize
        int length = EnemyInfoManager.length;
        int ROBOT_MAX_ID = OtherGameConstants.ROBOT_MAX_ID;

        bots = new int[length][OtherGameConstants.MAX_UNITS_TYPE];
        infoMap = new RobotInfo[ROBOT_MAX_ID];
        sizes = new int[length];
        totals = new int[length];
        hash = new boolean[ROBOT_MAX_ID];

        killScore = new int[length];
        deleteHash = new boolean[ROBOT_MAX_ID];
        killerType = new int[ROBOT_MAX_ID];

        channel = MessagingConstants.KILL_CHANNEL_START;

        this.c = c;
        this.rc = c.rc;
        type = 0;
    }

    public void addBots(RobotInfo[] enemies) {
        int[][] bots = this.bots;
        int[] sizes = this.sizes;
        boolean[] hash = this.hash;
        int[] totals = this.totals;
        RobotInfo[] infoMap = this.infoMap;
        for (int i = Math.min(enemies.length - 1, 20); i >= 0; i--) {
            RobotInfo enemy = enemies[i];
            if (!hash[enemy.ID]) {
                if (enemy.type != RobotType.MISSILE) {
                    int ord = enemy.type.ordinal();
                    bots[ord][sizes[ord]++] = enemy.ID;
                    hash[enemy.ID] = true;
                    infoMap[enemy.ID] = enemy;
                    totals[ord]++;
                }
            } else {
                infoMap[enemy.ID] = enemy;
            }
        }
    }

    public void addDeletions() throws GameActionException {
        int end = this.channel + 10;
        RobotController rc = this.rc;
        int MAX_ID = OtherGameConstants.ROBOT_MAX_ID;
        boolean[] deleteHash = this.deleteHash;
        int[] killerType = this.killerType;

        for (int channel = this.channel; channel < end; channel++) {
            int data = rc.readBroadcast(channel);
            if (data == 0) {
                this.channel = channel;
                return;
            }

            data--;
            int id = data % MAX_ID;
            int type = data / MAX_ID;

            deleteHash[id] = true;
            killerType[id] = type;
        }
        this.channel = this.channel + 10;
    }

    public void clearBots() {
        // Clear on a rotating basis
        if (Clock.getRoundNum() % 2 != 1) {
            return; // skip a turn
        }
        int i = type;
        boolean[] hash = this.hash;
        int size = sizes[i];

        int[] newBots = bots[i];
        int newSize = 0;

        boolean[] deleteHash = this.deleteHash;
        int[] killScore = this.killScore;
        int[] killerType = this.killerType;
        RobotInfo[] infoMap = this.infoMap;

        for (int j = 0; j < size; j++) {
            int id = newBots[j];

            if (deleteHash[id]) {
                killScore[killerType[id]] += infoMap[id].type.oreCost;
                hash[id] = false;
            } else {
                // add the bot in
                newBots[newSize++] = newBots[j];
            }
        }

        // store new bots
        sizes[i] = newSize;

        type = (type + 1) % length;

    }
}
