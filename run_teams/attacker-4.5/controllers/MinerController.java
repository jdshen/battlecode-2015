package attacker4_5.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker4_5.actors.Action;
import attacker4_5.actors.Miner;
import attacker4_5.actors.SupplyActor;

/**
 * Created by jdshen on 1/6/15.
 */
public class MinerController extends Controller {
    private SupplyActor supplyActor;
    private Miner miner;

    public MinerController(RobotController rc) {
        super(rc);
        miner = new Miner(this);
        supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        Action action;
        action = miner.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
