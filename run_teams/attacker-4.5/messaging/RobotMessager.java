package attacker4_5.messaging;

import battlecode.common.*;
import attacker4_5.controllers.Controller;

/**
 * Class for managing a single id-channel. Channel is associated with a robot id and offset by
 * channel = id % CommsProtocol.ID_CHAN_MOD + offset
 *
 * Only differs from CommandMessager by the fact that channel/idcheck are precomputed to save bytecode
 *
 * Should be initialized if you know a robot will be using a particular channel (i.e. its personal inbound/outbound channels) a lot.
 */
public class RobotMessager {
    private RobotController rc;
    private Controller c;
    public final int channel;
    public final int msgIDCheck; //Added to messages to confirm id
    public static MessageType[] msgs = MessageType.values();
    public MapLocation lastLoc;
    public MessageType lastMsg;

    public final int ID_CHAN_MOD = MessagingConstants.ID_CHAN_MOD;
    public final int ID_MSG_MASK = MessagingConstants.ID_MSG_MASK;
    public final int LOC_MASK = MessagingConstants.LOC_MASK;
    public final int WIDTH = 2*GameConstants.MAP_MAX_WIDTH;
    public final int HEIGHT = 2*GameConstants.MAP_MAX_HEIGHT;


    // added to locations when being broadcasted (to make positive), subtracted off when reading
    public final int LOC_X_CONVERSION;
    public final int LOC_Y_CONVERSION;

    /**
     * Feed in offset to get whether this channel is an input to or output from robot #id
     * @param c controller
     * @param myid robot id to associate
     * @param offset
     */
    public RobotMessager(Controller c, int myid, int offset) {
        this.rc = c.rc;
        this.c = c;
        channel = (myid % ID_CHAN_MOD) + offset;
        msgIDCheck = (myid % MessagingConstants.ID_CHECK_MOD) + 1; // + 1 to make channel non-zero

        this.LOC_X_CONVERSION = - c.teamhq.x + GameConstants.MAP_MAX_WIDTH;
        this.LOC_Y_CONVERSION = - c.teamhq.y + GameConstants.MAP_MAX_HEIGHT;
    }

    /**
     * Check channel for message. Saves if any
     * @return true if new message
     * @throws GameActionException
     */
    public boolean readMsg() throws GameActionException{
        int data = rc.readBroadcast(channel);
        if(data == 0){
            return false;
        }else{
            if(data % ID_MSG_MASK == msgIDCheck){
                data = data/ID_MSG_MASK;
                lastMsg = msgs[data/(LOC_MASK)];
                lastLoc = new MapLocation(
                        (data % WIDTH) - LOC_X_CONVERSION,
                        ((data / WIDTH) % HEIGHT) - LOC_Y_CONVERSION
                );
                return true;
            }
            return false;
        }
    }

    public void clearChannel() throws GameActionException{
        rc.broadcast(channel, 0);
    }

    /**
     * Broadcast to channel
     * @param msg a MessageType
     * @param loc a location
     * @throws GameActionException
     */
    public void broadcastMsg(MessageType msg, MapLocation loc) throws GameActionException{
        int x = loc.x + LOC_X_CONVERSION;
        int y = loc.y + LOC_Y_CONVERSION;
        rc.broadcast(channel, msgIDCheck + ID_MSG_MASK*((x + y*WIDTH) + msg.ordinal()*LOC_MASK));
    }
}
