package attacker4_5.messaging;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import attacker4_5.OtherGameConstants;

/**
 * Created by kevin on 1/21/15.
 */
public class KillMessager {
    public int channel = MessagingConstants.KILL_CHANNEL_START;
    private RobotController rc;
    public final int botType;
    public final int MAX_ID = OtherGameConstants.ROBOT_MAX_ID;
    public final int TYPE_INFO;

    /**
     * Initiliazes Messager
     * @param rc Robot Controller
     * */
    public KillMessager(RobotController rc){
        this.rc = rc;
        botType = rc.getType().ordinal();
        TYPE_INFO = MAX_ID * botType + 1;
    }

    /**
     * Always make sure update is done. Or else broadcasts may override.
     * Checks channels in a rolling fashion.
     * No check against channel number going too high, but it's limited in theory.
     * This update does nothing with data
     * @throws GameActionException
     */
    public void updatePass() throws GameActionException {
        int data = rc.readBroadcast(channel);
        while (data != 0) {
            channel++;
            data = rc.readBroadcast(channel);
        }
    }

    /**
     * Broadcast a kill
     * @throws GameActionException
     */
    public void broadcastKill(int id) throws GameActionException{
        rc.broadcast(channel, id + TYPE_INFO);
        //manually update receiver
        channel ++;
    }

    /**
     * Broadcast a kill
     * @throws GameActionException
     */
    public void broadcastKill(RobotInfo info) throws GameActionException{
        int id = info.ID;
        rc.broadcast(channel, id + TYPE_INFO);
        //manually update receiver
        channel ++;
    }
}
