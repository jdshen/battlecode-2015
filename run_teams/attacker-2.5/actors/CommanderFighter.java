package attacker2_5.actors;

import battlecode.common.*;
import attacker2_5.util.AttackerLookups;
import attacker2_5.OtherGameConstants;
import attacker2_5.controllers.Controller;
import attacker2_5.movement.Bugger;
import attacker2_5.movement.Mover;

/**
 * Makes fight decisions for commander
 *
 * Decides step ins
 *
 * should always try to stay at 8-10 range away
 *
 * Assumes we have flash
 *
 */
public class CommanderFighter extends Actor{
    public final int MIN_STEPIN_HEALTH = 160;
    public final int MIN_RETREAT_HEALTH = 60;

    private AttackerLookups lookups;
    private GenericAttacker attacker;
    private HeavyHandsAttacker heavyAttacker;

    private Direction[] intToDirs = Direction.values();
    private boolean avoidTowers;
    private MapLocation towerLoc;
    // will ignore tower splash dmg when calculating damage if off
    private boolean avoidHQ;
    private MapLocation target;
    private Bugger bugger;
    private Bugger retreater;
    private MapLocation retreatLoc;
    private Mover mover;

    public CommanderFighter(Controller c, MapLocation targetLoc) {
        super(c);
        this.attacker = new GenericAttacker(c);
        this.heavyAttacker = new HeavyHandsAttacker(c);
        this.lookups = new AttackerLookups();
        this.avoidHQ = true;
        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
        this.target = targetLoc;
        this.bugger = new Bugger(c);
        this.mover = new Mover(c);
        bugger.startBug(target);
        this.retreater = new Bugger(c);
        this.retreatLoc = c.teamhq;
        retreater.endBug();
    }

    public void setAvoidTowers(boolean avoidTowers) {
        this.avoidTowers = avoidTowers;
    }

    public void ignoreTower(MapLocation towerLoc) {
        this.towerLoc = towerLoc;
    }

    public void setAvoidHQ(boolean avoidHQ) {
        this.avoidHQ = avoidHQ;
    }

    public void setRetreatLoc(MapLocation retreatLoc) {
        if (this.retreatLoc != retreatLoc) {
            this.retreatLoc = retreatLoc;
            if (retreater.bugging){
                retreater.startBug(retreatLoc);
            }
        }
    }

    public void setTarget(MapLocation target){
        if (!this.target.equals(target)) {
            this.target = target;
            if (bugger.bugging) {
                bugger.startBug(target);
            }
        }
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        // decide stuff
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;
        if(!move && !attack){
            return new Action(false, false);
        }
        boolean heavyHands = rc.hasLearnedSkill(CommanderSkillType.HEAVY_HANDS);

        RobotInfo[] enemies = rc.senseNearbyRobots(25, c.enemy);
        int numEnemies = enemies.length;
        if(numEnemies == 0){
            return actNoEnemies(move);

        }

        boolean attacked = false;
        // attack if possible
        if(heavyHands){
            // target based on attack delay/damage, but hit commanders first
            Action act = heavyAttacker.act(move, attack);
            attacked = act.attacked;
        }
        else{
            Action act = attacker.act(move, attack);
            attacked = act.attacked;
        }
        int numDirs = intToDirs.length;

        // how much dmg can be dealt before we can move away from square
        int[] damageInDir = new int[numDirs];
        // places we are willing to move - should not be in 8 range of a unit
        boolean[] moveOptions = new boolean[numDirs];
        boolean[] nearEnemy = new boolean[numDirs];
        updateInitialMoveOptions(damageInDir, moveOptions);

        int idx;
        int damage;
        int attackRadius;
        RobotInfo en;
        MapLocation there;
        Direction[] attackedDirs;
        for (int i = numEnemies - 1; i >= 0; i --) {
            en = enemies[i];
            there = en.location;
            RobotType enType = en.type;
            attackRadius = enType.attackRadiusSquared;
            switch(enType){
                case HQ:
                    damage = 24; // assuming that if we went in, hq lost tower buffs
                    break;
                case TOWER:
                    damage = 16;
                    break;
                case BEAVER:
                    damage = 4;
                    break;
                case SOLDIER:
                    damage = 8;
                    break;
                case BASHER:
                    damage = 8;
                    break;
                case MINER:
                    damage = 3;
                    break;
                case DRONE:
                    damage = 8;
                    break;
                case TANK:
                    damage = 20;
                    break;
                case COMMANDER:
                    damage = 20;
                    break;
                case MISSILE:
                    damage = 20;
                    break;
                default:
                    damage = 0;
                    break;
            }
            if(heavyHands){
                if(en.weaponDelay > 1){
                    damage = damage/2;
                }
            }
            if (attackRadius < 8){
                attackedDirs = lookups.getDirs(5, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    damageInDir[idx] = damageInDir[idx] + damage;

                    nearEnemy[idx] = true;
                }
            } else {
                attackedDirs = lookups.getDirs(attackRadius, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    damageInDir[idx] = damageInDir[idx]   + damage;
                }
                attackedDirs = lookups.getDirs(5, there.x - hereX, there.y - hereY);
                for (int j = attackedDirs.length - 1; j >= 0; j--) {
                    idx = attackedDirs[j].ordinal();
                    nearEnemy[idx] = true;
                }
            }
//            // NEVER WALK INTO COMMANDER RANGE
//            if (enType == RobotType.COMMANDER){
//               for (int j = attackedDirs.length - 1; j >= 0; j--) {
//                    idx = attackedDirs[j].ordinal();
//                    moveOptions[idx] = false;
//                }
//            }
        }

        int curDamage = damageInDir[Direction.OMNI.ordinal()];
        //RETREAT
        if(rc.getHealth() < MIN_RETREAT_HEALTH){
            if(curDamage == 0){
                int numAllies = rc.senseNearbyRobots(25, c.team).length;
                if(numAllies *2 > numEnemies){
                    return new Action(false, false);
                }
            }
            return retreat(enemies, damageInDir, moveOptions, move, attacked);
        }

        if(curDamage >= rc.getHealth()){
            return retreat(enemies, damageInDir, moveOptions, move, attacked);
        }

        if(!move){
            return new Action(false, attacked);
        }
        // Go towards target location
        // Not in range, decide stepin
        if(curDamage == 0){
            if(rc.getHealth() <= MIN_STEPIN_HEALTH){
                //don't step in range
                for(int i = numDirs - 1; i >= 0; i--){
                    if(!moveOptions[i]){
                        continue;
                    }
                    if(damageInDir[i] > 0){
                        moveOptions[i] = false;
                        continue;
                    }
                }
            }
        }

        // check for adjacent enemies, back up if there are
        enemies = rc.senseNearbyRobots(2, c.enemy);
        if(enemies.length != 0) {
            // TODO improve backup
            if (mover.makeSafeMove(enemies[0].location.directionTo(here), moveOptions, false)) {
                retreater.endBug();
                bugger.endBug();
                return new Action(true, attacked);
            }
            return new Action(false, attacked);
        }

        // Decide whether to move closer
        // step onto a square if it doesn't make you retreat next turn
        // and if its not near enemies
        for (int i = numDirs - 1; i >= 0; i--){
            if(!moveOptions[i]){
                continue;
            }
            // don't move anywhere with more dmg
            if(attacked && damageInDir[i] > curDamage) {
                moveOptions[i] = false;
                continue;
            }
            if(rc.getHealth() - damageInDir[i] <= MIN_RETREAT_HEALTH ||
                    2*damageInDir[i] >= rc.getHealth()){
                moveOptions[i] = false;
                continue;
            }
            if(nearEnemy[i]){
                moveOptions[i] = false;
            }
        }
        //if we attacked, don't bug. Try to move towards target or stay still.
        if(attacked){
            if(heavyHands){
                return new Action(false, true);
            }
            if(mover.makeSafeMove(here.directionTo(target), moveOptions, true)){
                bugger.endBug();
                retreater.endBug();
                return new Action(true, true);
            }
            return new Action(false, true);
        }

        if(!bugger.bugging){
            bugger.startBug(target);
        }
        Direction dir = bugger.bug(moveOptions);
        if(dir == Direction.OMNI || dir == Direction.NONE){
            // back away from adjacent enemies
            return new Action(false, attacked);
        }
        rc.move(dir);
        retreater.endBug();

        // If we decide not to move, "fake" consume the move
        return new Action(true, attacked);

    }

    public Action actNoEnemies(boolean move) throws GameActionException {
        //if low on health, just hang out
        if(rc.getHealth() < MIN_STEPIN_HEALTH){
            return new Action(false, false);
        }

        if(move){
            if(rc.getLocation().distanceSquaredTo(target) < 5){
                return new Action(false, false);
            }
            if(!bugger.bugging){
                bugger.startBug(target);
            }
            Direction dir = bugger.bug(mover.getCanMove(avoidTowers, towerLoc, avoidHQ));
            if(dir != Direction.OMNI && dir != Direction.NONE){
                rc.move(dir);
                retreater.endBug();
                return new Action(true, false);
            }
        }
        return new Action(false, false);

    }

    public Action retreat(RobotInfo[] enemies, int[] damageInDir, boolean[] moveOptions, boolean move, boolean attacked) throws GameActionException {
        if(!move){
            return new Action(false, attacked);
        }
        //RETREAT LOGIC - TRY TO BUG TO RETREAT LOCATION AVOIDING DMG
        // loop through directions, if no damage and can move, move
        for (int i = intToDirs.length - 1; i >= 0; i--){
            if(damageInDir[i] > 0 ){
                moveOptions[i] = false;
            }
        }
        if(!retreater.bugging){
            retreater.startBug(retreatLoc);
        }
        Direction dir = retreater.bug(moveOptions);
        if(rc.canMove(dir)){
            rc.move(dir);
            bugger.endBug();
            return new Action(true, attacked);
        }

        if(rc.getFlashCooldown() > 0) {
            return new Action(false, attacked);
        }

        // TODO retreat logic, Flash away if no moves.
        int[] retreatDirs = new int[intToDirs.length];
        int numEnemies = enemies.length;
        RobotInfo en;
        MapLocation there;
        MapLocation here = rc.getLocation();

        // Try flashing towards retreat loc;
        dir = here.directionTo(retreatLoc);
        if(tryFlash(dir, true, true)){
            bugger.endBug();
            retreater.endBug();
            return new Action(true, attacked);
        }

        dir = Direction.OMNI;
        int idx;
        retreatDirs[Direction.OMNI.ordinal()] = 100;
        retreatDirs[Direction.NONE.ordinal()] = 100;
        // find directions "away from enemies"
        for (int i = numEnemies -1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            dir = here.directionTo(there);
            idx = dir.ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 4;
            idx = dir.rotateLeft().ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 2;
            idx = dir.rotateRight().ordinal();
            retreatDirs[idx] = retreatDirs[idx] + 2;
            dir = dir.opposite();
            idx = dir.opposite().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 3;
            idx = dir.rotateLeft().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 1;
            idx = dir.rotateRight().ordinal();
            retreatDirs[idx] = retreatDirs[idx] - 1;
        }
        // set dir to the best retreat direction
        idx = 100;
        for(int i = intToDirs.length - 1; i >= 0; i--){
            if(retreatDirs[i] < idx){
                dir = intToDirs[i];
                idx = retreatDirs[i];
            }
        }

        // flash away from enemies;
        // TODO make more than 3 locs - check remaining squares for flashing to
        if(tryFlash(dir, false, true)){
            return new Action(true, attacked);
        }

        return new Action(false, attacked);
    }

    public boolean tryFlash(Direction dir, boolean checkSenseEnemies, boolean rotations) throws GameActionException {
        MapLocation here = rc.getLocation();
        MapLocation ideal = here.add(dir, 2);
        if(!dir.isDiagonal()){
            ideal = ideal.add(dir);
        }
        if(rc.isPathable(c.type, ideal)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(ideal, 15, c.enemy).length == 0) {
                rc.castFlash(ideal);
                return true;
            }

        }

        if(!rotations){
            return false;
        }
        Direction right = dir.rotateRight().rotateRight();
        Direction left = dir.rotateLeft().rotateLeft();

        here = ideal.add(right);
        if(rc.isPathable(c.type, here)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(here, 15, c.enemy).length == 0) {
                rc.castFlash(here);
                return true;
            }
        }
        here = ideal.add(left);
        if(rc.isPathable(c.type, here)){
            if(!checkSenseEnemies || rc.senseNearbyRobots(here, 15, c.enemy).length == 0) {
                rc.castFlash(here);
                return true;
            }
        }
        return false;
    }

    public void updateInitialMoveOptions(int[] damageInDir, boolean[] moveOptions){
        MapLocation here = rc.getLocation();
        Direction dir;
        MapLocation tmpLoc;

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for (int i = numTowers - 1; i >= 0; i--) {
            tmpLoc = towers[i];
            if (here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE) {
                if (tmpLoc.equals(towerLoc)) {
                    continue;
                }
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers++;
            }
        }
        for (int i = moveOptions.length - 1; i >= 0; i--) {
            dir = intToDirs[i];
            if (!rc.canMove(dir)) {
                continue;
            }
            if(!avoidTowers){
                moveOptions[i] = true;
                continue;
            }
            tmpLoc = here.add(dir);
            if(avoidHQ) {
                switch (towers.length) {
                    case 6:
                    case 5:
                        int thereX = c.enemyhq.x;
                        int thereY = c.enemyhq.y;
                        int delX = tmpLoc.x - thereX;
                        if (delX < 0) {
                            delX = -delX;
                        }
                        int delY = tmpLoc.y - thereY;
                        if (delY < 0) {
                            delY = -delY;
                        }
                        if (delX < 7 && delY < 7 && (delX + delY) < 11) {
                            damageInDir[i] = 240;
                            continue;
                        }
                        break; // technically out of sight range of the hq at this point so. ...
                    case 4:
                    case 3:
                    case 2:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                            damageInDir[i] = 36; // TODO technically wrong
                            continue;
                        }
                        break;
                    default:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                            damageInDir[i] = 24;
                            continue;
                        }
                }
            }
            moveOptions[i] = true;

            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    damageInDir[i] = 16;
                    moveOptions[i] = false;
                    break;
                }
            }
        }

    }

}
