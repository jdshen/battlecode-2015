package attacker2_5.actors;

import battlecode.common.*;
import attacker2_5.OtherGameConstants;
import attacker2_5.controllers.Controller;
import attacker2_5.messaging.KillMessager;

/**
 * Created by kevin on 1/11/15.
 */
public class GenericAttacker extends Actor {

    private KillMessager kill;

    public GenericAttacker(Controller c) {
        super(c);
        kill = new KillMessager(rc);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        kill.updatePass();
        if(!attack){
            return new Action(false, false);
        }

        //ATTACK LOGIC
        //get enemies in range
        int attackRadius = c.attackRadiusSquared;
        double attackPower = c.attackPower;

        RobotInfo[] enemies = rc.senseNearbyRobots(attackRadius, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }

        // look for "best" target
        // currently (enemy-hp)/(enemy-dmg-rate) TODO better targetting
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        RobotInfo bestEnemy = enemy;

        boolean complexTarget = enemies.length <= 3;

        int myDelay = c.attackDelay;
        int turns;
        // get number of turns to kill enemy
        if(complexTarget) {
            double dmgrate = attackPower;
            RobotInfo[] allies = rc.senseNearbyRobots(enemy.location, 15, c.team);
            for (int j = allies.length - 1; j >= 0; j--) {
                RobotInfo ally = allies[j];
                RobotType atype = ally.type;
                if (ally.location.distanceSquaredTo(enemy.location) < atype.attackRadiusSquared) {
                    if(type.attackDelay < myDelay) {
                        dmgrate += type.attackPower * myDelay / type.attackDelay;
                    }
                    else if (type.attackDelay == myDelay){
                        dmgrate += type.attackPower;
                    }
                    else{
                        dmgrate += type.attackPower * type.attackDelay / myDelay;
                    }
                }
            }
            turns = (int) (enemy.health / dmgrate + .999);
        } else{
            turns = (int) (enemy.health / attackPower + .999);
        }


        // get enemy damage rate
        double damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
        double score = turns/ damageRate + getBonus(enemy);

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            if(complexTarget) {
                double dmgrate = attackPower;
                RobotInfo[] allies = rc.senseNearbyRobots(enemy.location, 15, c.team);
                for (int j = allies.length - 1; j >= 0; j--) {
                    RobotInfo ally = allies[j];
                    RobotType atype = ally.type;
                    if (ally.location.distanceSquaredTo(enemy.location) < atype.attackRadiusSquared) {
                        if(type.attackDelay < myDelay) {
                            dmgrate += type.attackPower * myDelay / type.attackDelay;
                        }
                        else if (type.attackDelay == myDelay){
                            dmgrate += type.attackPower;
                        }
                        else{
                            dmgrate += type.attackPower * type.attackDelay / myDelay;
                        }
                    }
                }
                turns = (int) (enemy.health / dmgrate + .999);
            } else{
                turns = (int) (enemy.health / attackPower + .999);
            }
            // get enemy damage rate
            damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
            double newScore = turns/damageRate + getBonus(enemy);
            //switch targets
            if(score > newScore) {
                score = newScore;
                bestEnemy = enemy;
            }
        }

        rc.attackLocation(bestEnemy.location);
        if(bestEnemy.health <= attackPower && bestEnemy.type != RobotType.MISSILE){
            kill.broadcastKill(bestEnemy.ID);
        }

        return new Action(false, true);
    }

    // checks if the region in a direction for distance squares is easily traversed
    // returns true if 2/3 squares for each set of 3 are NORMAL
    // distance should be > 0
    public boolean isDirectionClear(Direction dir, int distance){
        RobotController rc = this.rc;
        MapLocation here = rc.getLocation();
        Direction left = dir.rotateLeft();
        Direction right = dir.rotateRight();
        MapLocation leftLoc = here.add(left);
        MapLocation rightLoc = here.add(right);
        switch(distance){
            case 1:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            case 2:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            case 3:
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                here = here.add(dir);
                leftLoc = here.add(left);
                rightLoc = here.add(right);
                if(rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL){
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                            return false;
                        }
                    }
                } else {
                    if(rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL){
                        return false;
                    }
                    if(rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL){
                        return false;
                    }
                }
                return true;
            default:
                for (int i = distance; i > 0; i --) {
                    if (rc.senseTerrainTile(leftLoc) == TerrainTile.NORMAL) {
                        if (rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL) {
                            if (rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL) {
                                return false;
                            }
                        }
                    } else {
                        if (rc.senseTerrainTile(rightLoc) != TerrainTile.NORMAL) {
                            return false;
                        }
                        if (rc.senseTerrainTile(here.add(dir)) != TerrainTile.NORMAL) {
                            return false;
                        }
                    }
                    here = here.add(dir);
                    leftLoc = here.add(left);
                    rightLoc = here.add(right);
                }
                return true;
        }
    }

    public int getAllyCount(int distance, MapLocation loc){
        int allyCount = 0; // Discount self, handicap towards stepping in
        RobotInfo ally;
        RobotType allyType;
        RobotInfo[] allies = rc.senseNearbyRobots(loc, distance, c.team);
        for (int i = allies.length - 1; i >= 0; i--) {
            ally = allies[i];
            allyType = ally.type;
            switch(allyType){
                case HQ:
                    break;
                case TOWER:
                    break;
                case SOLDIER:
                    allyCount += 6;
                    break;
                case BASHER:
                    allyCount += 9;
                    break;
                case DRONE:
                    allyCount += 4;
                    break;
                case TANK:
                    allyCount += 21;
                    break;
                case COMMANDER:
                    int xp = ally.xp;
                    if(xp >= GameConstants.XP_REQUIRED_LEADERSHIP) {
                        allyCount += allies.length;
                        if (xp >= GameConstants.XP_REQUIRED_HEAVY_HANDS) {
                            allyCount += 12;
                            if (xp >= GameConstants.XP_REQUIRED_IMPROVED_LEADERSHIP) {
                                allyCount += allies.length;
                            }
                        }
                    }
                    allyCount += 24;
                    break;
                case MISSILE:
                    break;
                case LAUNCHER:
                    allyCount += 30;
                    break;
                default:
                    break;
            }
        }
        return allyCount;
    }

    public int getBonus(RobotInfo info){
        return 0;
    }
}
