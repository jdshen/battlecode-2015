package attacker2_5;

import battlecode.common.RobotType;

/**
 * Created by jdshen on 1/6/15.
 */
public class OtherGameConstants {
    public static final double DIAGONAL_FACTOR = 1.4;
    public static final int TOWER_SPLASH_RANGE = 34; // 25 + 9
    public static final int TOWER_NEARBY_RANGE = 100; // 36
    public static final int SUPPLY_TRANSFER_BYTECODE = 500;
    public static final int SENSE_NEARBY_BYTECODE = 100;
    public static final int MAX_MAP_OFFSET = 32000;
    public static final int ROBOT_MAX_ID = 32001;
    public static final int MAX_UNITS_TYPE = 500; // MAX UNITS OF A TYPE

    public static final int TANK_SPLASH_RADIUS_SQ = 25; // tank radius + boundary of buffer
    public static final int TANK_UNSPLASH_RADIUS_SQ = 7; // tank radius - boundary of buffer

    public static final int BASHER_RUSH_RADIUS_SQ = RobotType.TANK.attackRadiusSquared; // MAX RAD

    public static final int[] SUPPLY_LOOKUP = new int[] {
        0, 100, 152, 193, 230, 263, 293, 321, 348, 374,
        398, 422, 444, 466, 487, 508, 528, 547, 566, 585,
        603, 621, 639, 656, 673, 690, 706, 722, 738, 754,
        770, 785, 800, 815, 830, 844, 859, 873, 887, 901,
        915, 928, 942, 955, 968, 982, 995, 1008, 1020, 1033,
        1046, 1058, 1071, 1083, 1095, 1107, 1119, 1131, 1143, 1155,
    }; // for 0.6

    public static final int SUPPLY_BASE = 200; // base * multiplier
}
