package attacker2_5.messaging;

import battlecode.common.RobotType;

/**
 * Created by kevin on 1/7/15.
 */
public enum MessageType {
    //global
    ASSAULT, DESTROY, DEFEND, HARASS,

    // build commands = 9
    BUILD_AEROSPACELAB(RobotType.AEROSPACELAB, true, false),
    BUILD_BARRACKS(RobotType.BARRACKS, true, false),
    BUILD_MINERFACTORY(RobotType.MINERFACTORY, true, false),
    BUILD_HELIPAD(RobotType.HELIPAD, true, false),
    BUILD_SUPPLYDEPOT(RobotType.SUPPLYDEPOT, true, false),
    BUILD_TANKFACTORY(RobotType.TANKFACTORY, true, false),
    BUILD_TECHNOLOGYINSTITUTE(RobotType.TECHNOLOGYINSTITUTE, true, false),
    BUILD_TRAININGFIELD(RobotType.TRAININGFIELD, true, false),
    BUILD_HANDWASHSTATION(RobotType.HANDWASHSTATION, true, false),

    // spawn commands = 9
    SPAWN_MINER(RobotType.MINER, false, true),
    SPAWN_BEAVER(RobotType.BEAVER, false, true),
    SPAWN_TANK(RobotType.TANK, false, true),
    SPAWN_BASHER(RobotType.BASHER, false, true),
    SPAWN_COMMANDER(RobotType.COMMANDER, false, true),
    SPAWN_COMPUTER(RobotType.COMPUTER, false, true),
    SPAWN_DRONE(RobotType.DRONE, false, true),
    SPAWN_LAUNCHER(RobotType.LAUNCHER, false, true),
    SPAWN_SOLDIER(RobotType.SOLDIER, false, true),

    // acknowledgements and stop action
    ACK(true), STOP_ACTION, SPAWNED(true),
    UN_ACK, // impossible action, retry

    // computer actions
    BFS_1, BFS_2,

    // to hq/pasture
    NEARBY_ENEMY // when assaulting/defending

    // defending

    ;

    public static MessageType getBuildMessage(RobotType type) {
        switch(type) {
            case SUPPLYDEPOT:
                return BUILD_SUPPLYDEPOT;
            case TECHNOLOGYINSTITUTE:
                return BUILD_TECHNOLOGYINSTITUTE;
            case BARRACKS:
                return BUILD_BARRACKS;
            case HELIPAD:
                return BUILD_HELIPAD;
            case TRAININGFIELD:
                return BUILD_TRAININGFIELD;
            case TANKFACTORY:
                return BUILD_TANKFACTORY;
            case MINERFACTORY:
                return BUILD_MINERFACTORY;
            case HANDWASHSTATION:
                return BUILD_HANDWASHSTATION;
            case AEROSPACELAB:
                return BUILD_AEROSPACELAB;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static MessageType getSpawnMessage(RobotType type) {
        switch(type) {
            case BEAVER:
                return SPAWN_BEAVER;
            case COMPUTER:
                return SPAWN_COMPUTER;
            case SOLDIER:
                return SPAWN_SOLDIER;
            case BASHER:
                return SPAWN_BASHER;
            case MINER:
                return SPAWN_MINER;
            case DRONE:
                return SPAWN_DRONE;
            case TANK:
                return SPAWN_TANK;
            case COMMANDER:
                return SPAWN_COMMANDER;
            case LAUNCHER:
                return SPAWN_LAUNCHER;
            default:
                throw new IllegalArgumentException();
        }
    }

    public RobotType type;
    public boolean isBuildCommand;
    public boolean isSpawnCommand;
    public boolean ack;

    private MessageType() {
        this(null, false, false);
    }

    private MessageType(RobotType type, boolean isBuildCommand, boolean isSpawnCommand) {
        this.type = type;
        this.isBuildCommand = isBuildCommand;
        this.isSpawnCommand = isSpawnCommand;
        this.ack = false;
    }

    private MessageType(boolean ack) {
        this.ack = ack;
        type = null;
        isBuildCommand = false;
        isSpawnCommand = false;
    }
}
