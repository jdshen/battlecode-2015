package attacker2_5.threads;

import battlecode.common.*;
import attacker2_5.OtherGameConstants;
import attacker2_5.controllers.Controller;
import attacker2_5.messaging.CommandMessager;
import attacker2_5.messaging.DirectionMap;
import attacker2_5.messaging.MessageType;
import attacker2_5.messaging.MessagingConstants;

/**
 * Created by jdshen on 1/11/15.
 */
public class GeneralMicro {
    private RobotController rc;
    private Controller c;
    private RobotInfoManager allies;

    private MapLocation attackPoint;
    private int attackCycle;
    private MessageType command;

    private CommandMessager messager;

    private int[] lastMessage;

    private DirectionMap map;

    public GeneralMicro(Controller c, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;

        this.allies = allies;

        messager = new CommandMessager(c);

        attackPoint = c.enemyhq;
        attackCycle = 100;
        command = MessageType.HARASS;
        lastMessage = new int[OtherGameConstants.ROBOT_MAX_ID];

        map = new DirectionMap(c, MessagingConstants.BFS_1_CHANNEL_START);
    }

    public void calcAttackLocation() {
        int roundGoIn = rc.getRoundLimit() - 250;
        if (attackCycle == 0 || Clock.getRoundNum() >= roundGoIn) {
            attackCycle = 60;
        } else {
            attackCycle--;
            return;
        }

        int tanks = allies.sizes[RobotType.TANK.ordinal()];
        int launchers = allies.sizes[RobotType.LAUNCHER.ordinal()];
        tanks = Math.max(tanks, launchers * 8 / 5);

        MapLocation teamhq = c.teamhq;
        MapLocation enemyhq = c.enemyhq;

        RobotInfo[] enemies = rc.senseNearbyRobots(c.teamhq, Controller.MAX_DIAGONAL_SQ, c.enemy);

        if ((tanks > 15 && enemies.length < 7) || Clock.getRoundNum() >= roundGoIn) {
            if (tanks > 25 || Clock.getRoundNum() >= roundGoIn) {
                command = MessageType.DESTROY;
            } else {
                command = MessageType.ASSAULT;
            }

            MapLocation[] locs = rc.senseEnemyTowerLocations();
            if (locs.length == 0) {
                attackPoint = enemyhq;
                return;
            }

            int maxDist = 0;
            MapLocation target = null;
            for (int i = locs.length - 1; i >= 0; i--) {
                int dist = locs[i].distanceSquaredTo(enemyhq);
                if (dist > maxDist) {
                    maxDist = dist;
                    target = locs[i];
                }
            }

            attackPoint = target;
            return;
        }

        command = MessageType.HARASS;

        if (enemies.length > 0) {
            MapLocation closest = null;
            int minDist = Controller.MAX_DIAGONAL_SQ;
            for (int i = Math.min(enemies.length, 10) - 1; i >= 0; i--) {
                MapLocation loc = enemies[i].location;
                int dist = loc.distanceSquaredTo(teamhq);
                if (dist < minDist) {
                    closest = loc;
                    minDist = dist;
                }
            }

            attackPoint = closest;
            return;
        }

        MapLocation[] alliedTowers = rc.senseTowerLocations();
        if (alliedTowers.length > 0) {
            attackPoint = alliedTowers[0];
            return;
        }
    }

    public void run(boolean[] taken) throws GameActionException {
        calcAttackLocation();

        MapLocation attackPoint = this.attackPoint;
        rc.setIndicatorDot(attackPoint, 255, 0, 255);

        map.broadcastBFSRequest(attackPoint);

        int msg = messager.computeMessage(command, attackPoint);

        broadcast(RobotType.BASHER, taken, msg);
        broadcast(RobotType.LAUNCHER, taken, msg);
        broadcast(RobotType.SOLDIER, taken, msg);
        broadcast(RobotType.TANK, taken, msg);
        broadcast(RobotType.COMMANDER, taken, msg);
    }

    public void broadcast(RobotType type, boolean[] taken, int msg) throws GameActionException {
        broadcast(allies.bots[type.ordinal()], allies.sizes[type.ordinal()], taken, msg);
    }

    public void broadcast(int[] ids, int size, boolean[] taken, int msg) throws GameActionException {
        // localize
        int[] lastMessage = this.lastMessage;
        int offset = MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT;
        int ID_CHAN_MOD = messager.ID_CHAN_MOD;
        int ID_CHECK_MOD = messager.ID_CHECK_MOD;
        RobotController rc = this.rc;

        for (int i = size - 1; i >= 0; i--) {
            if (Clock.getBytecodeNum() > 9000) {
                return;
            }
            int id = ids[i];
            if (taken[id]) {
                continue;
            }

            if (lastMessage[id] != msg) {
                rc.broadcast(offset + id % ID_CHAN_MOD, (id % ID_CHECK_MOD) + msg);
                lastMessage[id] = msg;
            }
        }
    }

}
