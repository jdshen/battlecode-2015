package attacker2_5.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker2_5.actors.Action;
import attacker2_5.actors.DroneHarasser;
import attacker2_5.actors.Scout;
import attacker2_5.actors.SupplyActor;
import attacker2_5.messaging.MessagingConstants;
import attacker2_5.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class DroneController extends Controller {
    private Scout scout;
    private DroneHarasser attacker;
    private SupplyActor supplyActor;
    private RobotMessager in;

    public DroneController(RobotController rc) {
        super(rc);
        scout = new Scout(this);
        attacker = new DroneHarasser(this, this.enemyhq);
        supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
        in = new RobotMessager(this, rc.getID(), MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action = scout.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
//
        if (in.readMsg()) {
            attacker.setTarget(in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(true);
                    break;
                case ASSAULT:
                    attacker.ignoreTower(in.lastLoc);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(false);
                    break;
                case DESTROY:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(false);
                    attacker.setAvoidEnemyRange(false);
                    break;
            }
        }

        action = attacker.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
