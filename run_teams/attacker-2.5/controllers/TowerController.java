package attacker2_5.controllers;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker2_5.actors.Action;
import attacker2_5.actors.TowerAttacker;


/**
 * Created by kevin on 1/7/15.
 */
public class TowerController extends Controller {
    private TowerAttacker attacker;

    public TowerController(RobotController rc) {
        super(rc);
        this.attacker = new TowerAttacker(this);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action = attacker.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
