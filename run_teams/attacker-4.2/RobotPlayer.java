package attacker4_2;
import battlecode.common.*;
import attacker4_2.controllers.*;
import attacker4_2.controllers.MinerController;

public class RobotPlayer {
	public static void run(RobotController rc) {
        if(rc.getType() == RobotType.MISSILE){
            MissileController cont = new MissileController(rc);
            while (true) {
                try {
                    cont.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rc.yield();
            }
        }
        
		Controller cont = null;
		do {
			try {
				switch (rc.getType()) {
                    case HQ:
                        cont = new HQController(rc);
                        break;
                    case TOWER:
                        cont = new TowerController(rc);
                        break;
                    case SUPPLYDEPOT:
                        cont = new StructureController(rc);
                        break;
                    case TECHNOLOGYINSTITUTE:
                        cont = new StructureController(rc);
                        break;
                    case BARRACKS:
                        cont = new StructureController(rc);
                        break;
                    case HELIPAD:
                        cont = new StructureController(rc);
                        break;
                    case TRAININGFIELD:
                        cont = new StructureController(rc);
                        break;
                    case TANKFACTORY:
                        cont = new StructureController(rc);
                        break;
                    case MINERFACTORY:
                        cont = new StructureController(rc);
                        break;
                    case HANDWASHSTATION:
                        cont = new StructureController(rc);
                        break;
                    case AEROSPACELAB:
                        cont = new StructureController(rc);
                        break;
                    case BEAVER:
                        cont = new BeaverController(rc);
                        break;
                    case COMPUTER:
                        cont = new ComputerController(rc);
                        break;
                    case SOLDIER:
                        cont = new SoldierController(rc);
                        break;
                    case BASHER:
                        cont = new BasherController(rc);
                        break;
                    case MINER:
                        cont = new MinerController(rc);
                        break;
                    case DRONE:
                        cont = new DroneController(rc);
                        break;
                    case TANK:
                        cont = new TankController(rc);
                        break;
                    case COMMANDER:
                        cont = new CommanderController(rc);
                        break;
                    case LAUNCHER:
                        cont = new LauncherController(rc);
                        break;
//                    case MISSILE:
//                        cont = new MissileController(rc);
//                        break;
                    default:
                        cont = null;
                        break;
                }
			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (cont == null);

		while (true) {
			try {
				cont.run();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rc.yield();
		}
	}
}