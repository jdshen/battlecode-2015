package attacker4_2.messaging;

import battlecode.common.*;
import attacker4_2.OtherGameConstants;
import attacker4_2.controllers.Controller;

public class DirectionMap {
    private final Controller c;
    private RobotController rc;

    public int bfsRequest;

	public final int offset;
	public final static int LOC_MASK = MessagingConstants.LOC_MASK;
    public static final int HASH = MessagingConstants.MAX_DIM;
    public static final int OFFSET = (OtherGameConstants.MAX_MAP_OFFSET / HASH + 1) * HASH;

	public Direction[] intToDir;

    public final int WIDTH = 2*GameConstants.MAP_MAX_WIDTH;
    public final int HEIGHT = 2*GameConstants.MAP_MAX_HEIGHT;

    // added to locations when being broadcasted (to make positive), subtracted off when reading
    public final int LOC_X_CONVERSION;
    public final int LOC_Y_CONVERSION;
    public final int LOC_CONVERSION;

	public DirectionMap(Controller c, int channelStart) {
        offset = channelStart + 1;
        bfsRequest = channelStart;
        intToDir = Direction.values();
        this.rc = c.rc;
        this.c = c;

        this.LOC_X_CONVERSION = - c.teamhq.x + GameConstants.MAP_MAX_WIDTH;
        this.LOC_Y_CONVERSION = - c.teamhq.y + GameConstants.MAP_MAX_HEIGHT;
        this.LOC_CONVERSION = LOC_X_CONVERSION + WIDTH * LOC_Y_CONVERSION;
    }

    public void broadcastBFSRequest(MapLocation loc) throws GameActionException {
        rc.broadcast(bfsRequest, 1 + (loc.x + loc.y * WIDTH + LOC_CONVERSION));
    }

    public MapLocation readBFSRequest() throws GameActionException {
        int data = rc.readBroadcast(bfsRequest);
        if (data == 0){
            //nothing there
            return null;
        } else{
            data--;
            return new MapLocation(data % WIDTH - LOC_X_CONVERSION,(data / WIDTH) % HEIGHT - LOC_Y_CONVERSION);
        }
    }

	/**
	 * Post a direction to target location "to" at a loc
	 */
	public void broadcastDirection(MapLocation loc, MapLocation to, Direction dir) throws GameActionException{
        int HASH = this.HASH;
        int OFFSET = this.OFFSET;
        rc.broadcast(
            offset + (loc.x + OFFSET) % HASH + ((loc.y + OFFSET) % HASH) * HASH,
            1 + (to.x + to.y * WIDTH + LOC_CONVERSION + dir.ordinal()*LOC_MASK)
        );
	}

	/**
	 * Read Direction and Location posted for a location. Returns false if there is no message
	 * @param loc
	 * @param to
	 * @return
	 * @throws GameActionException 
	 */
	public Direction readDirection(MapLocation loc, MapLocation to) throws GameActionException{
        int HASH = this.HASH;
        int OFFSET = this.OFFSET;
        int data = rc.readBroadcast(offset + (loc.x + OFFSET) % HASH + ((loc.y + OFFSET) % HASH) * HASH);
		if (data == 0){
			//nothing there
			return null;
		} else{
			data--;
            // MAGIC CONSTANT
			if (
                new MapLocation(data % WIDTH - LOC_X_CONVERSION,(data / WIDTH) % HEIGHT - LOC_Y_CONVERSION)
                    .distanceSquaredTo(to) <= 2
            ) {
                return intToDir[data/LOC_MASK];
            }

            return null;
		}
	}
	
	
}
