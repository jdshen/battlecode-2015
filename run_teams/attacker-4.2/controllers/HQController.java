package attacker4_2.controllers;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import attacker4_2.actors.*;
import attacker4_2.computation.SymmetryComputation;
import attacker4_2.threads.BuildOrderManager;
import attacker4_2.threads.EnemyInfoManager;
import attacker4_2.threads.RobotInfoManager;

public class HQController extends Controller {
    private Actor supplyActor;
    private Spawner spawner;
    private HQAttacker attacker;
    private BuildOrderManager objectives;
    private RobotInfoManager allies;
    private EnemyInfoManager enemies;

    private boolean beaverTracker = false;

    public HQController(RobotController rc) throws GameActionException {
        super(rc);
        allies  = new RobotInfoManager(this);
        enemies  = new EnemyInfoManager(this);
        spawner = new Spawner(this, RobotType.BEAVER, 3);
        attacker = new HQAttacker(this);
        objectives = new BuildOrderManager(this, allies, enemies);
        supplyActor = new StructureSupplyActor(this, 9000); // don't go over on computation
        SymmetryComputation cmp = new SymmetryComputation(this);
    }

    @Override
    public void run() throws GameActionException {
        RobotController rc = this.rc;
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        if (beaverTracker) {
            allies.addHQSpawnedBots();
            beaverTracker = false;
        }
        objectives.run();

        if (allies.sizes[RobotType.BEAVER.ordinal()] <= 2 && spawner.count <= 0) {
            spawner = new Spawner(this, RobotType.BEAVER, 3);
        }

        Action action = spawner.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        if (action.moved) {
            beaverTracker = true;
        }

        action = attacker.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        rc.setIndicatorString(1, Clock.getBytecodeNum() + "...");
        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
