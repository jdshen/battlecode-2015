package attacker4_2.actors;

import battlecode.common.*;
import attacker4_2.controllers.Controller;
import attacker4_2.messaging.MessagingConstants;
import attacker4_2.messaging.ScoutingMessager;
import attacker4_2.movement.Mover;
import attacker4_2.util.AttackerLookups;

public class Scout extends Actor {

    private final MapLocation teamhq = c.teamhq;

    public final int BLOCK_SIZE = MessagingConstants.SCOUT_BLOCK_SIZE;
    private final int MAX_OFFSET = MessagingConstants.SCOUT_OFFSET;
    private final int HASH = MessagingConstants.SCOUT_HASH;
    private final AttackerLookups lookup = new AttackerLookups();

    private Direction[] intToDir = Direction.values();

    private ScoutingMessager scouter;

    private Mover mover;

    public MapLocation target;
    private int currentTargetX;
    private int currentTargetY;

    public Scout(Controller c) {
        super(c);
        this.scouter = new ScoutingMessager(c);
        this.mover = new Mover(c);
        target = teamhq.add(intToDir[c.id % intToDir.length], BLOCK_SIZE);
        currentTargetX = ((target.x + MAX_OFFSET) / BLOCK_SIZE) % HASH;
        currentTargetY = ((target.y + MAX_OFFSET) / BLOCK_SIZE) % HASH;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }



    private boolean makeMove(boolean move) throws GameActionException {

        if (!move) {
            return false;
        }

        RobotController rc = this.rc;
        Controller c = this.c;

        int HASH = this.HASH;
        int BLOCK_SIZE = this.BLOCK_SIZE;
        int MAX_OFFSET = this.MAX_OFFSET;

        MapLocation here = rc.getLocation();
        ScoutingMessager scouter = this.scouter;
        AttackerLookups lookup = this.lookup;
        int numDirs = intToDir.length;

        int hereX = here.x;
        int hereY = here.y;
        int curX = ((hereX + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        int curY = ((hereY + MAX_OFFSET)/ BLOCK_SIZE) % HASH;

        // update scout info
        scouter.update(curX, curY);

        // update scout target if we've arrived
//        rc.setIndicatorString(0, " start of function curX and Y:" + curX +", " + curY + "  currTarget: "
//                 + currentTargetX + ", " + currentTargetY);
        if(curX == currentTargetX && curY == currentTargetY){
//            rc.setIndicatorString(2, " first target update");
            updateTarget(here, curX, curY);
        }

        int[] moveOptions = mover.getDroneInitialMoveOptions(here, true, c.teamhq);
        boolean[] trueCanMove = new boolean[numDirs];
        for(int i = numDirs - 1; i >= 0; i --){
            trueCanMove[i] = moveOptions[i] <= 1;
        }
        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared + 1, c.enemy);

        int numEns = enemies.length;
        //avoid enemies
        int idx = 0;
        boolean[] minerDirs = new boolean[numDirs];
        for (int i = numEns - 1; i >= 0; i--){
            RobotInfo en = enemies[i];
            MapLocation there = en.location;
            int attackRadius = en.type.attackRadiusSquared;

            Direction[] attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
            switch(en.type){
                case SUPPLYDEPOT:
                case TECHNOLOGYINSTITUTE:
                case BARRACKS:
                case HELIPAD:
                case TRAININGFIELD:
                case TANKFACTORY:
                case MINERFACTORY:
                case HANDWASHSTATION:
                case AEROSPACELAB:
                case COMPUTER:
                        continue;
                case MINER:
                case BEAVER:
                    // discount single miner/beaver
                    for (int j = attacked.length - 1; j >= 0; j--) {
                        idx = attacked[j].ordinal();
                        moveOptions[idx] = moveOptions[idx] + 2;
                        minerDirs[idx] = true;
                    }
                    continue;
                case MISSILE:
                    attackRadius = 10;
                    break;
                case BASHER:
                    attackRadius = 8;
                    break;
                case COMMANDER:
                    attackRadius = 24;
                    break;
                case LAUNCHER:
                    attackRadius = 15;
                    break;
                default:
                    attackRadius = 0;
                    break;
            }
            for (int j = attacked.length - 1; j >= 0; j--) {
                idx = attacked[j].ordinal();
                moveOptions[idx] = moveOptions[idx] + 2;
                trueCanMove[idx] = false;
            }
            if(attackRadius != 0){
                attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
            }
            for (int j = attacked.length - 1; j >= 0; j--) {
                idx = attacked[j].ordinal();
                moveOptions[idx] = moveOptions[idx] + 2;
            }
        }

        //ignore 1 miner/beaver
        for(int i = numDirs - 1; i >= 0; i --) {
            if(minerDirs[i]){
                moveOptions[i] = moveOptions[i] - 2;
            }
        }

        Direction dir = here.directionTo(target);
        if(mover.makeDroneSafeMove(dir, moveOptions, false)){
            return true;
        }

//        rc.setIndicatorString(2, " after making move towards target");
        if (rc.senseTerrainTile(here.add(dir)) == TerrainTile.OFF_MAP ){
//            rc.setIndicatorString(2, "offmap");
            scouter.forceUpdate((curX + dir.dx) % HASH, (curY + dir.dy) % HASH, 10000);
            updateTarget(here, curX, curY);
            dir = here.directionTo(target);
//            rc.setIndicatorString(1, "Was Off Map. Going to new target " + target + " in direction " + dir);
            if(mover.makeDroneSafeMove(dir, moveOptions, false)){
                return true;
            }
        }
//        rc.setIndicatorString(2, " units in way ");
        // we have units in the way, or enemy is in the way. mark it as visited and pick a new target
//        for(int dx = -1; dx <= 1; dx ++){
//            for(int dy = -1; dy <= 1; dy ++){
//                scouter.update((currentTargetX + dx) % width, (currentTargetY + dy) % height);
//            }
//        }
        // MARK VISITED IF BLOCKED
        dir = here.directionTo(target);
        scouter.update(currentTargetX, currentTargetY);

        updateTarget(here, curX, curY);

        // Surrounded by enemies. Just try to move away
        if(numEns > 0){
            if(mover.makeDroneSafeMove(dir.opposite(), moveOptions, true)){
                return true;
            }
            Direction[] intToDirs = Direction.values();

            for (int i = numEns -1; i >= 0; i--) {
                RobotInfo en = enemies[i];
                MapLocation there = en.location;
                dir = here.directionTo(there);
                idx = dir.ordinal();
                moveOptions[idx] = moveOptions[idx] + 6;
                idx = dir.rotateLeft().ordinal();
                moveOptions[idx] = moveOptions[idx] + 4;
                idx = dir.rotateRight().ordinal();
                moveOptions[idx] = moveOptions[idx] + 4;
                dir = dir.opposite();
                idx = dir.opposite().ordinal();
                moveOptions[idx] = moveOptions[idx] - 4;
                idx = dir.rotateLeft().ordinal();
                moveOptions[idx] = moveOptions[idx] - 2;
                idx = dir.rotateRight().ordinal();
                moveOptions[idx] = moveOptions[idx] - 2;
            }

            dir = Direction.OMNI;
            Direction curDir;
            int best = moveOptions[dir.ordinal()];
            for(int i = moveOptions.length - 1; i>=0; i--){
                curDir = intToDirs[i];
                if(!rc.canMove(curDir)){
                    trueCanMove[i] = false; //update this with can moves
                    continue;
                }
                if(moveOptions[i] < best){
                    best = moveOptions[i];
                    dir = curDir;
                }
            }
            if(dir != Direction.OMNI){
                if(mover.makeTrueDroneSafeMove(dir, trueCanMove, moveOptions)) {
                    return true;
                }
            }

            for(int i = moveOptions.length - 1; i >= 0; i--) {

                if(trueCanMove[i]){
                    rc.move(intToDirs[i]);
                    return true;
                }
            }
        }

        return false;
    }

    // update target to an adjacent block that lowest value in visited
    private void updateTarget(MapLocation here, int curX, int curY) throws GameActionException {
//        rc.setIndicatorString(1, "updating target using " + here + " " + curX + ", " + curY);

        int best = 1000000; //larger than counter should ever be
        Direction dir; Direction bestDir = Direction.OMNI;
        Direction[] intToDir = this.intToDir;
        int HASH = this.HASH;
        ScoutingMessager scouter = this.scouter;
        int x; int y; int val;
        for (int i = intToDir.length - 1; i >= 0; i --) {
            dir = intToDir[i];
            switch(dir){
                case NONE:
                    break;
                case OMNI:
                    break;
                default:
                    x = (curX + dir.dx) % HASH;
                    y = (curY + dir.dy) % HASH;
                    val = scouter.getLastScout(x,y);
                    if(val < best){
                        best = val;
                        currentTargetX = x;
                        currentTargetY = y;
                        bestDir = dir;
                    }
            }
        }

        target = here.add(bestDir, BLOCK_SIZE);
//        rc.setIndicatorString(2, "new target is " + target + " " + currentTargetX + ", " + currentTargetY);
    }

    // set the target for the scout to the block containing target loc
    public void setTarget(MapLocation loc) throws GameActionException {
        MapLocation here = rc.getLocation();
        if(here.equals(loc) || target.equals(loc)){
            // broadcasted self, don't do anything
            return;
        }
        int BLOCK_SIZE = this.BLOCK_SIZE;
        int MAX_OFFSET = this.MAX_OFFSET;
        int HASH = this.HASH;

        int curX = ((here.x  + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        int curY = ((here.y + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        int newTargetX = ((loc.x + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        int newTargetY = ((loc.y + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        if(curX == newTargetX && curY == newTargetY){
            // broadcasted same block, probably wants something like harassment.
            // set target to adjacent block in this direction
            Direction dir = here.directionTo(loc);
            currentTargetX = (curX + dir.dx) % HASH;
            currentTargetY = (curY + dir.dy) % HASH;
            target = here.add(dir, BLOCK_SIZE);
            return;
        }
        currentTargetX = newTargetX;
        currentTargetY = newTargetX;
        target = loc;
    }



}
