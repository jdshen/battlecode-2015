package attacker3_0.actors;

import battlecode.common.*;
import attacker3_0.OtherGameConstants;
import attacker3_0.controllers.Controller;
import attacker3_0.util.AttackerLookups;

/**
 * DroneAttacker - attacks thing that it can kill fastest
 *
 * Created by jdshen on 1/7/15.
 */
public class DroneHarasser extends Actor {
    private MapLocation target;

    public DroneAttacker attacker;
    public AttackerLookups lookup;

    public Direction[] intToDirs = Direction.values();
    public boolean avoidTowers;
    public boolean avoidEnemyRange;
    public MapLocation towerLoc;

    // track which dir we rotated last
    private boolean lastMoveWasClockwise = false;
    private Direction directionTowardApproachable = Direction.OMNI;

    public DroneHarasser(Controller c, MapLocation target) {
        super(c);
        this.target = target;
        attacker = new DroneAttacker(c);
        lookup = new AttackerLookups();
        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
    }

    public void setTarget(MapLocation target){
        this.target = target;
    }

    public void setAvoidTowers(boolean avoidTowers){
        this.avoidTowers = avoidTowers;
    }

    public void ignoreTower(MapLocation towerLoc){
        this.towerLoc = towerLoc;
    }

    public void setAvoidEnemyRange(boolean avoidEnemyRange){ this.avoidEnemyRange = avoidEnemyRange; }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        RobotController rc = this.rc;
        Controller c = this.c;

        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;

        if (!move && !attack) {
            // just wait
            return new Action(false, false);
        }

        // movement decision
        // try to move towards target. Prioritize staying out of range
        int[] moveOptions = getInitialMoveOptions(here);

        RobotInfo[] enemies = rc.senseNearbyRobots(25, c.enemy); // MAGIC CONSTANT
        int numEnemies = enemies.length;
        RobotInfo en;
        MapLocation there;
        Direction[] attacked;
        // Remember which enemies are approachable
        Direction[] approachable = new Direction[numEnemies];
        MapLocation[] approachableLocs = new MapLocation[numEnemies];
        int numApproachable = 0;
        int idx = 0;
        if(avoidEnemyRange) { //update moveOptions to reflect enemy attacks
            for (int i = numEnemies - 1; i >= 0; i--) {
                en = enemies[i];
                there = en.location;
                int attackRadius = en.type.attackRadiusSquared;
                switch(en.type){
                    case SUPPLYDEPOT:
                    case TECHNOLOGYINSTITUTE:
                    case BARRACKS:
                    case HELIPAD:
                    case TRAININGFIELD:
                    case TANKFACTORY:
                    case MINERFACTORY:
                    case HANDWASHSTATION:
                    case AEROSPACELAB:
                    case BEAVER:
                    case COMPUTER:
                    case MINER:
                        approachable[numApproachable] = here.directionTo(there);
                        approachableLocs[numApproachable] = there;
                        numApproachable ++;
                        continue; // ignore MINER and BEAVER attack ranges
                    default:
                        break;
                }
                if (attackRadius == 2) { // Respect the basher // MAGIC CONSTANT
                    attacked = lookup.getDirs(8, there.x - hereX, there.y - hereY);
                } else {
                    attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
                }
                for (int j = attacked.length - 1; j >= 0; j--) {
                    idx = attacked[j].ordinal();
                    moveOptions[idx] = moveOptions[idx] + 2;
                }
            }
        }
        boolean inEnemyRange = moveOptions[Direction.OMNI.ordinal()] > 1;
        if(!inEnemyRange || !avoidEnemyRange) { // ONLY ATTACK if we are hitting things that don't fight back
            Action act = attacker.act(move, attack);
            if(act.attacked){
                // Can no longer move and attack same turn
                return new Action(false, true);
            }
        }

        if(numApproachable == 0){
            directionTowardApproachable = Direction.OMNI;
        }

        if(!move) {
            return new Action(false, false);
        }

        // try to approach targets units
        if(directionTowardApproachable != Direction.OMNI){
            // we were going towards a target
            Direction fromBack = directionTowardApproachable.opposite();
            Direction fromLeft = fromBack.rotateLeft();
            Direction fromRight = fromBack.rotateRight();
            for (int i = numApproachable - 1; i >= 0; i--){
                Direction toEn = approachable[i];
                if (toEn == fromBack || toEn == fromLeft || toEn == fromRight){
                    // circled behind something
                    int distance = here.distanceSquaredTo(approachableLocs[i]);
                    if(distance > c.attackRadiusSquared) {
                        if(makeSafeMove(toEn, moveOptions, false)){
                            return new Action(true, false);
                        }
                    }
                    else {
                        if(!inEnemyRange) {
                            return new Action(false, false);
                        }
                    }
                }
                else {
                    if(makeSafeMove(toEn, moveOptions, false)){
                        return new Action(true, false);
                    }
                }
            }
            directionTowardApproachable = Direction.OMNI;
        }
        else {
            for (int i = numApproachable - 1; i >= 0; i--) {
                if (makeSafeMove(approachable[i], moveOptions, true)) {
                    if (directionTowardApproachable == Direction.OMNI) {
                        directionTowardApproachable = approachable[i];
                    }
                    return new Action(true, false);
                }
            }
        }

        // try to move toward target. Try perpendicular motion. Don't step in range.
        Direction dir = here.directionTo(target);
        if (makeSafeMove(dir, moveOptions, true)) {
            return new Action(true, false);
        }


        // if currently targetted. try retreating.
        dir = dir.opposite();
        if(makeSafeMove(dir, moveOptions, false)){
            return new Action(true, false);
        }

        // TODO shortcut to here and make better logic

        // TARGETTED EVERYWHERE - pick the best direction to run in. Anything is better than nothing
        // Penalize moving towards enemies
        // Bytecode cost: ~100 / enemy
        for (int i = numEnemies -1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            dir = here.directionTo(there);
            idx = dir.ordinal();
            moveOptions[idx] = moveOptions[idx] + 6;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] + 3;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] + 3;
            dir = dir.opposite();
            idx = dir.opposite().ordinal();
            moveOptions[idx] = moveOptions[idx] - 4;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] - 2;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] - 2;

        }

        dir = Direction.OMNI;
        Direction curDir;
        int best = moveOptions[dir.ordinal()];
        for(int i = moveOptions.length - 1; i>=0; i--){
            curDir = intToDirs[i];
            if(!rc.canMove(curDir)){
                continue;
            }
            if(moveOptions[i] < best){
                best = moveOptions[i];
                dir = curDir;
            }
        }

        if(dir != Direction.OMNI){
            rc.move(dir);
            return new Action(true, false);
        }

        // attack if we can't run
        Action act = attacker.act(move, attack);

        return new Action(false, act.attacked);
    }

    public int[] getInitialMoveOptions(MapLocation here){
        int dirsLength = intToDirs.length;
        int[] moveOptions = new int[dirsLength];

        // get tower/hq ranges and add to moveOptions
        Direction dir;
        MapLocation tmpLoc;

        if(!avoidTowers) {
            return moveOptions;
        }

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for(int i = numTowers - 1; i >= 0; i--){
            tmpLoc = towers[i];
            if(here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE){
                if(tmpLoc.equals(towerLoc)){
                    continue;
                }
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers ++;
            }
        }

        for (int i = moveOptions.length - 1; i >= 0; i--) {
            dir = intToDirs[i];
            if (dir == Direction.NONE) {
                continue;
            }
            tmpLoc = here.add(dir);
            //penalize void squares
            if(rc.senseTerrainTile(tmpLoc) == TerrainTile.VOID){
                moveOptions[i] = 1;
            }
            switch (towers.length) {
                case 6:
                case 5:
                    int thereX = c.enemyhq.x;
                    int thereY = c.enemyhq.y;
                    int delX = tmpLoc.x - thereX;
                    if(delX < 0){
                        delX = -delX;
                    }
                    int delY = tmpLoc.y - thereY;
                    if(delY < 0){
                        delY = -delY;
                    }
                    if(delX < 7 && delY < 7 && (delX + delY) < 11){
                        moveOptions[i] = 100;
                        continue;
                    }
                    break; // technically out of sight range of the hq at this point so. ...
                case 4:
                case 3:
                case 2:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                        moveOptions[i] = 100;
                        continue;
                    }
                    break;
                default:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                        moveOptions[i] = 100;
                        continue;
                    }
            }
            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    moveOptions[i] = 100; // hate towers
                    break;
                }
            }
        }

        return moveOptions;
    }

    /**
     * Makes a move in a direction, consulting an array of enemy range coutns
     * @param dir direction to move in
     * @param moveOptions int array of # of enemies that can attack square in that direction
     * @param extended whether to include perpendicular directions
     * @return
     * @throws battlecode.common.GameActionException
     */
    public boolean makeSafeMove(Direction dir, int[] moveOptions, boolean extended) throws GameActionException {
        int idx = dir.ordinal();
        if ((moveOptions[idx] == 0 || moveOptions[idx] == 1) && rc.canMove(dir)) {
            rc.move(dir);
            return true;
        }

        Direction first;
        Direction second;
        Direction left = dir.rotateLeft();
        Direction right = dir.rotateRight();
        if(lastMoveWasClockwise){
            first = right;
            second = left;
        }
        else{
            first = left;
            second = right;
        }
        idx = first.ordinal();
        if ((moveOptions[idx] == 0 || moveOptions[idx] == 1) && rc.canMove(first)) {
            rc.move(first);
            return true;
        }

        idx = second.ordinal();
        if ((moveOptions[idx] == 0 || moveOptions[idx] == 1) && rc.canMove(second)) {
            rc.move(second);
            lastMoveWasClockwise = !lastMoveWasClockwise;
            return true;
        }
        if(!extended) {
            return false;
        }

        //try going perpendicular and "away" from target (go around)
        if(lastMoveWasClockwise){
            first = right.rotateRight();
            second = left.rotateLeft();
        }
        else{
            first = left.rotateLeft();
            second = right.rotateRight();
        }

        idx = first.ordinal();
        if((moveOptions[idx] == 0 || moveOptions[idx] == 1) && rc.canMove(first)){
            rc.move(first);
            return true;
        }
        idx = second.ordinal();
        if((moveOptions[idx] == 0 || moveOptions[idx] == 1) && rc.canMove(second)){
            rc.move(second);
            lastMoveWasClockwise = !lastMoveWasClockwise;
            return true;
        }

        return false;
    }
}
