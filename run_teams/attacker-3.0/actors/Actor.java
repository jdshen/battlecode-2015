package attacker3_0.actors;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker3_0.controllers.Controller;

/**
 * Created by jdshen on 1/6/15.
 */
public abstract class Actor {
    public final Controller c;
    public final RobotController rc;

    public Actor(Controller c) {
        this.c = c;
        this.rc = c.rc;
    }

    public abstract Action act(boolean move, boolean attack) throws GameActionException;
}
