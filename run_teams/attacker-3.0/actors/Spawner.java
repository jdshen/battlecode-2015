package attacker3_0.actors;

import battlecode.common.*;
import attacker3_0.controllers.Controller;
import attacker3_0.messaging.MessageType;
import attacker3_0.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class Spawner extends Actor {
    public Direction spawnDir;
    private final RobotType unit;
    public int count;
    private RobotMessager out;
    private int rate;
    private int maxRate;

    public Spawner(Controller c, RobotType unit, RobotMessager out, int count){
        super(c);
        this.count = count;
        this.unit = unit;
        this.spawnDir = Direction.NORTH;
        if (unit == RobotType.MINER) {
            this.count = 30; // TODO FIX FIX FIX
        }

        this.maxRate = 1;
        if (unit == RobotType.BASHER) {
            maxRate = 20;
        }
        this.rate = maxRate;
        this.out = out;
    }

    public Spawner(Controller c, RobotType unit, int count) {
        this(c, unit, null, count);
    }

    public Spawner(Controller c, RobotType unit){
        this(c, unit, 1);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        boolean makeMove = makeMove(move);
        if (out != null && !makeMove) {
            out.broadcastMsg(MessageType.ACK, rc.getLocation());
        }

        return new Action(makeMove, false);
    }

    private boolean makeMove(boolean move) throws GameActionException {
        if (count == 0) {
            return false;
        }

        if (!move) {
            return false;
        }

        if (rate > 0) {
            rate--;
            return false;
        } else {
            rate = maxRate;
        }

        RobotController rc = this.rc;
        RobotType unit = this.unit;

        Direction dir = spawnDir.rotateLeft().rotateLeft().rotateLeft(); // even distribution
        for (int i = 7; i >= 0; i--) {
            if (rc.canSpawn(dir, unit)) {
                spawnDir = dir;
                rc.spawn(dir, unit);
                count--;
                if (out != null) {
                    out.broadcastMsg(MessageType.SPAWNED, rc.getLocation().add(dir));
                }
                return true;
            }
            dir = dir.rotateLeft().rotateLeft().rotateLeft();
        }
        return false;
    }
}
