package attacker3_0.actors;

import battlecode.common.*;
import attacker3_0.controllers.Controller;
import attacker3_0.messaging.MessagingConstants;
import attacker3_0.messaging.ScoutingMessager;
import attacker3_0.movement.Mover;
import attacker3_0.util.AttackerLookups;

public class Scout extends Actor {
    private final int blockSize = MessagingConstants.SCOUT_BLOCK_SIZE;

    private final MapLocation teamhq = c.teamhq;
    private final int width = (GameConstants.MAP_MAX_WIDTH+blockSize-1)/blockSize;
    private final int height = (GameConstants.MAP_MAX_HEIGHT+blockSize-1)/blockSize;
    private final AttackerLookups lookup = new AttackerLookups();

    private Direction[] intToDir = Direction.values();

    private ScoutingMessager scouter;

    private Mover mover;

    private MapLocation target;
    private int currentTargetX;
    private int currentTargetY;

    public Scout(Controller c) {
        super(c);
        this.scouter = new ScoutingMessager(c);
        this.mover = new Mover(c);
        target = teamhq.add(intToDir[c.id % intToDir.length], 20);
        currentTargetX = (target.x / blockSize) % width;
        currentTargetY = (target.y / blockSize) % height;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }



    private boolean makeMove(boolean move) throws GameActionException {
        if (!move) {
            return false;
        }

        RobotController rc = this.rc;
        Controller c = this.c;

        MapLocation here = rc.getLocation();
        int curX = (here.x / blockSize) % width;
        int curY = (here.y / blockSize) % height;

        // update scout info
        scouter.update(curX, curY);

        // update scout target if we've arrived
        if(curX == currentTargetX && curY == currentTargetY){
            updateTarget(here, curX, curY);
        }
        boolean[] canMove = mover.getCanMove(true, teamhq, true);
        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared + 1, c.enemy);

        int numEns = enemies.length;
        //avoid enemies
        for (int i = numEns - 1; i >= 0; i--){
            RobotInfo en = enemies[i];
            MapLocation there = en.location;
            int attackRadius = en.type.attackRadiusSquared;
            if (attackRadius == 2) {
                attackRadius = 8; // basher and missile respect
            }
            if (en.type == RobotType.LAUNCHER){
                attackRadius = 15; // tank range launchers
            }
            Direction[] attacked = lookup.getDirs(attackRadius, there.x - here.x, there.y - here.y);

            for (int j = attacked.length - 1; j >= 0; j--) {
                canMove[attacked[j].ordinal()] = false;
            }
        }


        Direction dir = here.directionTo(target);
//        rc.setIndicatorString(2, "make move towards target");
        if(mover.makeSafeMove(dir, canMove, false)){
            return true;
        }
        if (rc.senseTerrainTile(here.add(dir)) == TerrainTile.OFF_MAP ){
            // TODO may bug out when hitting edge of map on max map size
//            rc.setIndicatorString(2, "offmap");
            scouter.forceUpdate((curX + dir.dx) % width, (curY + dir.dy) % height, 10000);
            updateTarget(here, curX, curY);
            dir = here.directionTo(target);
            if(mover.makeSafeMove(dir, canMove, false)){
                return true;
            }
        }
//        rc.setIndicatorString(2, " units in way ");
        // we have units in the way, or enemy is in the way. mark it as visited and pick a new target
        for(int dx = -1; dx <= 1; dx ++){
            for(int dy = -1; dy <= 1; dy ++){
                scouter.update((currentTargetX + dx) % width, (currentTargetY + dy) % height);
            }
        }

        updateTarget(here, curX, curY);


        // Surrounded by enemies. Just try to move away
        if(numEns > 0 && mover.makeSafeMove(dir.opposite(), canMove, true)){
            return true;
        }

        return false;
    }

    // update target to an adjacent block that lowest value in visited
    private void updateTarget(MapLocation here, int curX, int curY) throws GameActionException {
//        rc.setIndicatorString(0, "updating target at " + here + " " + curX + ", " + curY);
        int best = 1000000; //larger than counter should ever be
        Direction dir; Direction bestDir = Direction.OMNI;
        int x; int y; int val;
        for (int i = intToDir.length - 1; i >= 0; i --) {
            dir = intToDir[i];
            switch(dir){
                case NONE:
                    break;
                case OMNI:
                    break;
                default:
                    x = (curX + dir.dx) % width;
                    y = (curY + dir.dy) % height;
                    val = scouter.getLastScout(x,y);
                    if(val < best){
                        best = val;
                        currentTargetX = x;
                        currentTargetY = y;
                        bestDir = dir;
                    }
            }
        }

        target = here.add(bestDir, blockSize);
//        rc.setIndicatorString(1, "new target is " + target);
    }

    // set the target for the scout to the block containing target loc
    public void setTarget(MapLocation loc){
        currentTargetX = (loc.x / blockSize) % width;
        currentTargetY = (loc.y / blockSize) % height;
        target = loc;
    }
}
