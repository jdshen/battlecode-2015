package attacker3_0.actors;

import battlecode.common.*;
import attacker3_0.OtherGameConstants;
import attacker3_0.controllers.Controller;
import attacker3_0.movement.Bugger;
import attacker3_0.movement.Mover;
import attacker3_0.util.AttackerLookups;

/**
 * Created by kevin on 1/12/15.
 */
public class LauncherActor extends Actor{
    private MapLocation target;
    private final Direction[] intToDirs = Direction.values();
    private Bugger bugger;
    private AttackerLookups lookup;
    private final int LAUNCH_RATE = 3; // MAGIC CONSTANT
    private int cooldown;

    private boolean avoidTowers;
    private MapLocation towerLoc;
    private Mover mover;

    // get a building location that we might be able to fire to
    private MapLocation buildingLoc = c.enemyhq;

    public LauncherActor(Controller c, MapLocation target) {
        super(c);
        this.target = target;
        this.bugger = new Bugger(c);
        this.bugger.startBug(target);
        this.lookup = new AttackerLookups();

        this.mover = new Mover(c);

        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
    }

    public void setAvoidTowers(boolean avoidTowers){ this.avoidTowers = avoidTowers; }

    public void ignoreTower(MapLocation towerLoc) { this.towerLoc = towerLoc; }

    public void setTarget(MapLocation target) {
        if (!this.target.equals(target)) {
            this.target = target;
            bugger.startBug(target);
        }
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        cooldown --;
        // Make Launch decision
        // look in sight range - shoot at best priority
        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;

        RobotInfo[] enemies = rc.senseNearbyRobots(20, c.enemy);
        int numEnemies = enemies.length;
        if(rc.getMissileCount() > 0 && cooldown <= 0) {
            if (numEnemies == 0) {
                // if no enemies in sight range, check out 1 more out
                enemies = rc.senseNearbyRobots(35, c.enemy);
                if (enemies.length > 0) {
                    launchSingle(here.directionTo(enemies[0].location));
                }
            } else {
                launchDouble(pickTargetDir(enemies, numEnemies, here));
            }
        }


        // can't attack
        if (!move) {
            // Launch at buildings
            if(cooldown <= 0 && rc.getMissileCount() > 0){
                if(here.distanceSquaredTo(buildingLoc) <= 50){
                    MapLocation[] towerLocs = rc.senseEnemyTowerLocations();
                    for(int i = towerLocs.length - 1; i >=0; i --){
                        if(towerLocs[i].equals(buildingLoc)){
                            launchSingleOnly(here.directionTo(buildingLoc));
                            cooldown --;
                            return new Action(false, false);
                        }
                    }
                    if(buildingLoc.equals(c.enemyhq)){
                        launchSingleOnly(here.directionTo(buildingLoc));
                        cooldown --;
                        return new Action(false, false);
                    }
                }
            }
            return new Action(false, false);
        }

        if (numEnemies == 0 && enemies.length != 0) {
            // already set enemeis to extended range
            numEnemies = enemies.length;
        } else{
            //use extended range for checking range
            enemies = rc.senseNearbyRobots(35, c.enemy);
            numEnemies = enemies.length;
        }

        boolean[] canMove = getInitialCanMove(here);

        // Launch at buildings
        if(cooldown <= 0 && rc.getMissileCount() > 0){
            if(here.distanceSquaredTo(buildingLoc) <= 49){
                launchSingleOnly(here.directionTo(buildingLoc));
                cooldown --;
            }
        }

        //try to move away from enemies, otherwise bug;
        // setup move options while looking at enemies
        int dirsLength = intToDirs.length;
        int[] moveOptions = new int[dirsLength];
        RobotInfo en;
        MapLocation there;
        Direction[] attacked;

        boolean[] dirsToTry = new boolean[dirsLength]; // Directions to try moving in
        boolean[] superRange = new boolean[dirsLength]; // can moves for perpendicular movement
        for (int i = dirsLength - 1; i >= 0; i--) {
            superRange[i] = canMove[i];
        }

        int idx = 0;
        for (int i = numEnemies - 1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            RobotType entype = en.type;
            int attackRadius = entype.attackRadiusSquared;
            Direction dir = here.directionTo(there);
            if (attackRadius == 2) { // Respect the basher // TODO MAGIC CONSTANT
                attacked = lookup.getDirs(8, there.x - hereX, there.y - hereY);
                superRange[dir.ordinal()] = false;
                superRange[dir.rotateLeft().ordinal()] = false;
                superRange[dir.rotateRight().ordinal()] = false;
            } else {
                dirsToTry[dir.ordinal()] = true;
                if (entype == RobotType.LAUNCHER){
                    attacked = lookup.getDirs(24, there.x - hereX, there.y - hereY);
                    for (int j = attacked.length - 1; j >= 0; j--) {
                        idx = attacked[j].ordinal();
                        superRange[idx] = false;
                        moveOptions[idx] = moveOptions[idx] + 1;
                    }
                    continue;
                }
                attacked = lookup.getDirs(24, there.x - hereX, there.y - hereY);
                for (int j = attacked.length - 1; j >= 0; j--) {
                    idx = attacked[j].ordinal();
                    superRange[idx] = false;
                }
                attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);

            }
            for (int j = attacked.length - 1; j >= 0; j--) {
                idx = attacked[j].ordinal();
                moveOptions[idx] = moveOptions[idx] + 1;
            }
        }

        Direction dir = Direction.OMNI;
        // under attack, try to retreat
        if (moveOptions[dir.ordinal()] > 0) {
            bugger.endBug();
            dir = getRetreatDir(canMove, moveOptions, enemies, here);
            if(dir != Direction.OMNI){
                rc.move(dir);
                return new Action(true, false);
            }
            return new Action(false, false);
        }

        // if enemies,
        if (numEnemies > 0){
            // try moving around enemy at range of 24
            for (int i = dirsLength - 1; i >= 0 ; i--) {
                if(dirsToTry[i]){
                    if(mover.makeSafeMove(intToDirs[i], superRange, true)){
                        bugger.endBug();
                        return new Action(true, false);
                    }
                }
            }
            return new Action(false, false);
        }

        // bug if not next to target
        if(!here.isAdjacentTo(target)) {
            if (!bugger.bugging) {
                bugger.startBug(target);
            }
            for (int i = dirsLength - 1; i >= 0; i--) {
                if (moveOptions[i] > 0) {
                    canMove[i] = false;
                }
            }
            dir = bugger.bug(canMove);
            if(rc.canMove(dir)){
                rc.move(dir);
                return new Action(true, false);
            }
            else {
                return new Action(false, false);
            }
        }

        // move onto target
        dir = here.directionTo(target);
        int dirIdx = dir.ordinal();
        if(canMove[dirIdx] && moveOptions[dirIdx] == 0){
            rc.move(dir);
            bugger.endBug();
            return new Action(true, false);
        }

        return new Action(false, false);
    }

    // Also updates buildingLoc with nearby buildings
    public boolean[] getInitialCanMove(MapLocation here){
        int dirsLength = intToDirs.length;
        boolean[] moveOptions = new boolean[dirsLength];

        // get tower/hq ranges and add to moveOptions
        Direction dir;
        MapLocation tmpLoc;
        buildingLoc = c.enemyhq;

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for(int i = numTowers - 1; i >= 0; i--){
            tmpLoc = towers[i];
            if(here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE){
                if(tmpLoc.equals(towerLoc)){
                    continue;
                }
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers ++;
            }
        }

        for (int i = moveOptions.length - 1; i >= 0; i--) {
            dir = intToDirs[i];
            if (!rc.canMove(dir)) {
                continue;
            }
            if(!avoidTowers){
                moveOptions[i] = true;
                continue;
            }
            tmpLoc = here.add(dir);
            switch (towers.length) {
                case 6:
                case 5:
                    int thereX = c.enemyhq.x;
                    int thereY = c.enemyhq.y;
                    int delX = tmpLoc.x - thereX;
                    if(delX < 0){
                        delX = -delX;
                    }
                    int delY = tmpLoc.y - thereY;
                    if(delY < 0){
                        delY = -delY;
                    }
                    if(delX < 7 && delY < 7 && (delX + delY) < 11){
                        continue;
                    }
                    break; // technically out of sight range of the hq at this point so. ...
                case 4:
                case 3:
                case 2:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                        buildingLoc = c.enemyhq;
                        continue;
                    }
                    break;
                default:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                        buildingLoc = c.enemyhq;
                        continue;
                    }
            }
            moveOptions[i] = true;

            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    buildingLoc = needCheckTowers[j];
                    moveOptions[i] = false;
                    break;
                }
            }
        }

        return moveOptions;
    }

    // takes array of enemies retreating from and
    // move options, an array representing "badness" of moving in each direction
    public Direction getRetreatDir(boolean[] canMove, int[] moveOptions, RobotInfo[] enemies, MapLocation here){
        Direction dir;
        RobotInfo en;
        int idx;
        MapLocation there;
        int numEnemies = enemies.length;
        for (int i = numEnemies -1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            dir = here.directionTo(there);
            idx = dir.ordinal();
            moveOptions[idx] = moveOptions[idx] + 2;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] + 1;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] + 1;
            dir = dir.opposite();
            idx = dir.opposite().ordinal();
            moveOptions[idx] = moveOptions[idx] - 2;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] - 1;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] - 1;
        }
        dir = Direction.OMNI;
        Direction curDir;
        int best = moveOptions[dir.ordinal()];
        for(int i = moveOptions.length - 1; i>=0; i--){
            curDir = intToDirs[i];
            if(!canMove[i]){
                continue;
            }
            if(moveOptions[i] < best){
                best = moveOptions[i];
                dir = curDir;
            }
        }
        return dir;
    }

    public Direction pickTargetDir(RobotInfo[] enemies, int numEnemies, MapLocation here){
        RobotInfo enemy = enemies[numEnemies - 1];
        RobotType type = enemy.type;

        // get number of turns to kill enemy
        int turns = (int) (enemy.health /GameConstants.MISSILE_MAXIMUM_DAMAGE  + .999);
        // get enemy damage rate
        double damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
        double score = turns/ damageRate + getBonus(type);
        MapLocation loc = enemy.location;

        for(int i = numEnemies - 2; i >= 0; i--){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / GameConstants.MISSILE_MAXIMUM_DAMAGE + .999);
            // get enemy damage rate
            damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
            double newScore = turns/damageRate + getBonus(type);
            //switch targets
            if(score > newScore){
                score = newScore;
                loc = enemy.location;
            }
        }
        return here.directionTo(loc);
    }

    public int getBonus(RobotType type){
        switch(type){
            case LAUNCHER: return -200;
            case TANK: return -100;
            case COMPUTER:
            case BEAVER:
            case SOLDIER:
            case MINER:
                return 0;
            case COMMANDER: return 50;
            case BASHER: return 50;
            case DRONE: return 1000;
            case MISSILE: return 10000;
            default:
                return 0;
        }
    }

    public void launchSingleOnly(Direction dir) throws GameActionException {
        if(rc.canLaunch(dir)){
            rc.launchMissile(dir);
            cooldown = LAUNCH_RATE;
        }
        return;
    }

    // tries to launch a single missile in dir
    public void launchSingle(Direction dir) throws GameActionException {
        if (rc.canLaunch(dir)){
            rc.launchMissile(dir);
            cooldown = LAUNCH_RATE;
            return;
        }
        Direction left = dir.rotateLeft();
        if(rc.canLaunch(left)){
            rc.launchMissile(left);
            cooldown = LAUNCH_RATE;
            return;
        }
        Direction right = dir.rotateRight();
        if(rc.canLaunch(right)){
            rc.launchMissile(right);
            cooldown = LAUNCH_RATE;
            return;
        }
    }

    // try to launch two missles non-adjacently. Defaults to 1 if fails
    public void launchDouble(Direction dir) throws GameActionException {
        if(rc.getMissileCount()>1) {
            Direction left = dir.rotateLeft();
            Direction right = dir.rotateRight();
            if (rc.canLaunch(left) && rc.canLaunch(right)) {
                rc.launchMissile(left);
                rc.launchMissile(right);
                cooldown = LAUNCH_RATE;
                return;
            }
        }
        launchSingle(dir);
    }

    /**
     * tries to launch up to 3 missiles within 1 rotation of the direction
     * @param dir Direction to launch in
     */
    public void launchMany(Direction dir) throws GameActionException {
        if (rc.canLaunch(dir)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(dir);
        }
        Direction left = dir.rotateLeft();
        if(rc.canLaunch(left)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(left);
        }
        Direction right = dir.rotateRight();
        if(rc.canLaunch(right)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(right);
        }
    }

    /**
     * Try to launch up to 5 missiles within 2 rotations of direction
     * @param dir Direction to launch in
     */
    public void launchMax(Direction dir) throws GameActionException {
        if (rc.canLaunch(dir)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(dir);
        }
        Direction left = dir.rotateLeft();
        if(rc.canLaunch(left)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(left);
        }
        Direction right = dir.rotateRight();
        if(rc.canLaunch(right)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(right);
        }
        left = left.rotateLeft();
        if(rc.canLaunch(left)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(left);
        }
        right = right.rotateRight();
        if(rc.canLaunch(right)){
            cooldown = LAUNCH_RATE;
            rc.launchMissile(right);
        }
    }

    /**
     * tries to launch a wave of missiles.
     * Will save missiles until > 3 then call launchMany
     * @param dir Direction to launch in
     */
    public void launchWave(Direction dir) throws GameActionException {
        if(rc.getMissileCount() > 2){
            launchMany(dir);
        }
    }


}
