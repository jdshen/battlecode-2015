package attacker3_0.actors;

import battlecode.common.*;
import attacker3_0.OtherGameConstants;
import attacker3_0.controllers.Controller;

/**
 * Created by jdshen on 1/11/15.
 */
public class StructureSupplyActor extends Actor {
    private final int bytecodeLimit;

    public StructureSupplyActor(Controller c, int bytecodeLimit) {
        super(c);
        this.bytecodeLimit = bytecodeLimit;
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        RobotController rc = this.rc;
        int num = Clock.getBytecodeNum();
        int count = (bytecodeLimit - num - OtherGameConstants.SENSE_NEARBY_BYTECODE) /
            OtherGameConstants.SUPPLY_TRANSFER_BYTECODE;

        if (count <= 0) {
            return new Action(false, false);
        }

        RobotInfo[] infos = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, c.team);

        double supplyLevel = rc.getSupplyLevel();
        int distToHQ = rc.getLocation().distanceSquaredTo(c.teamhq);

        for (int i = infos.length - 1; i >= 0 && count > 0; i--) {
            RobotInfo info = infos[i];

            if (info.type.isBuilding) {
                if (info.location.distanceSquaredTo(c.teamhq) <= distToHQ) {
                    continue;
                }

                if (supplyLevel - info.supplyLevel >= count) {
                    int transfer = (int) ((supplyLevel - info.supplyLevel) / count);
                    rc.transferSupplies(transfer, info.location);
                    count--;
                    supplyLevel -= transfer;
                }

                continue;
            }

            // treat beavers like buildings
            if (info.type == RobotType.BEAVER) {
                if (supplyLevel - info.supplyLevel >= count) {
                    int transfer = (int) ((supplyLevel - info.supplyLevel) / count);
                    rc.transferSupplies(transfer, info.location);
                    count--;
                    supplyLevel -= transfer;
                }
                continue;
            }

            if (supplyLevel >= count) {
                int transfer = (int) ((supplyLevel) / count);
                rc.transferSupplies(transfer, info.location);
                count--;
                supplyLevel -= transfer;
            }
        }
        return new Action(false, false);
    }
}
