package attacker3_0.actors;

import battlecode.common.RobotInfo;
import attacker3_0.controllers.Controller;

/**
 * Created by kevin on 1/15/15.
 */
public class HeavyHandsAttacker extends GenericAttacker {
    public HeavyHandsAttacker(Controller c) {
        super(c);
    }

    /***
     * Add bonus to target enemies whose weapons are coming off delay
     * @param info
     * @return
     */
    @Override
    public int getBonus(RobotInfo info){
        int bonus = 0;
        if(info.weaponDelay < 2){
            bonus += -1000;
        }
        switch(info.type){
            case HQ:
                break;
            case TOWER:
                break;
            case BEAVER:
                break;
            case COMPUTER:
                break;
            case SOLDIER:
                break;
            case BASHER:
                break;
            case MINER:
                break;
            case DRONE:
                break;
            case TANK:
                bonus = bonus - 30;
                break;
            case COMMANDER:
                bonus = bonus + 2; // better to delay things
                break;
            case MISSILE:
                return bonus;
            default:
                break;
        }
        // TODO bonus for kills?
        return bonus;
    }
}
