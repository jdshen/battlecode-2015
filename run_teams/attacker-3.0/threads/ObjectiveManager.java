package attacker3_0.threads;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker3_0.controllers.Controller;

/**
 * Created by jdshen on 1/9/15.
 */
public class ObjectiveManager {
    private static final int MAX_THREADS = 16;

    private ObjectiveThread[] threads;
    private final Controller c;
    private final RobotController rc;
    private int[] freeBots;
    private int freeBotCount;

    public ObjectiveManager(Controller c) {
        this.c = c;
        this.rc = c.rc;
        threads = new ObjectiveThread[MAX_THREADS];
        freeBots = new int[MAX_THREADS];
        freeBotCount = 0;
    }

    public int add(ObjectiveThread thread) {
        ObjectiveThread[] threads = this.threads;
        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                threads[i] = thread;
                return i;
            }
        }

        return -1;
    }

    /**
     * Only use after run has been called, otherwise a thread might take a recently released bot
     * @param num
     * @throws battlecode.common.GameActionException
     */
    public void close(int num) throws GameActionException {
        ObjectiveThread thread = threads[num];

        freeBots[freeBotCount] = thread.id;
        freeBotCount++;

        thread.close();
        threads[num] = null;
    }

    public void run(boolean[] taken) throws GameActionException {
        for (int i = freeBotCount - 1; i >= 0; i--) {
            taken[freeBots[i]] = false;
        }
        freeBotCount = 0;

//        int numThreads = 0;
        ObjectiveThread[] threads = this.threads;
        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                continue;
            }

//            numThreads++;
            ObjectiveThread thread = threads[i];
            int id = thread.id;
            if (id >= 0) {
                taken[id] = true;
                boolean done = rc.canSenseRobot(id) ? thread.run(rc.senseRobot(id)) : thread.run(null);
                if (done) {
                    if (thread.isFree()) {
                        freeBots[freeBotCount] = id;
                        freeBotCount++;
                    }
                    threads[i] = null;
                }
            }
        }
//        rc.setIndicatorString(2, "numThreads: " + numThreads);

        for (int i = threads.length - 1; i >= 0; i--) {
            if (threads[i] == null) {
                continue;
            }

            ObjectiveThread thread = threads[i];
            thread.find(taken);
        }
    }

}
