package attacker3_0.computation;

import battlecode.common.*;
import attacker3_0.controllers.Controller;
import attacker3_0.messaging.MessagingConstants;

/**
 * Compute and broadcast # allies onto ally computation channels
 *
 * For a unit type, (number of units + 1) is broadcast to ALLY_COMPUTATION_CHANNEL_START + ordinal(unit-type) + 1
 *
 * Broadcasts a 1 onto ALLY_COMPUTATION_CHANNEL_START to signify data is fresh
 * Broadcasts a 2 onto ALLY_COMPUTATION_CHANNEL_START to signify count incomplete
 */
public class AllyCountComputation extends Computation {

    public final int FLAG_CHANNEL = MessagingConstants.ALLY_COMPUTATION_CHANNEL_START;
    public final int DATA_CHANNEL_START = MessagingConstants.ALLY_COMPUTATION_CHANNEL_START + 1;

    public final int numTypes = RobotType.values().length;

    public AllyCountComputation(Controller c) {
        super(c);
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        int start = Clock.getBytecodeNum();
        int prebroadcastLimit = limit - 650;
        int stoppedFlag = 1;

        int[] counts = new int[numTypes];
        RobotInfo[] allies = rc.senseNearbyRobots(c.MAX_DIAGONAL_SQ, c.team);

        for (int i = allies.length - 1; i >= 0; i --){
            //stop counting early
            if(Clock.getBytecodeNum() - start > prebroadcastLimit){
                stoppedFlag = 2;
                break;
            }
            int ord = allies[i].type.ordinal();
            counts[ord] += 1;
        }

        // broadcast counts
        rc.broadcast(FLAG_CHANNEL, stoppedFlag);
        for (int i = numTypes-1; i >= 0; i --){
            rc.broadcast(DATA_CHANNEL_START + i, counts[i]);
        }
        return false;
    }
}
