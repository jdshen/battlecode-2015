package attacker3_0.computation;

import battlecode.common.*;
import attacker3_0.controllers.Controller;
import attacker3_0.messaging.DirectionMap;
import attacker3_0.movement.Mover;
import attacker3_0.computation.symmetry.Symmetry;

/**
 * Created by jdshen on 1/23/15.
 */
public class BFS extends Computation {
    private static final int MAX_STATES = 60000; // >> 120 * 120 * 3

    private final Symmetry symmetry;

    public int queueStart;
    public int queueEnd;
    public int[] queueX;
    public int[] queueY;
    public int[] queueR;
    public MapLocation[] queueD;
    public boolean[][] seen;
    public DirectionMap map;

    private static final int SEEN_HASH = 2 + Math.max(GameConstants.MAP_MAX_WIDTH, GameConstants.MAP_MAX_HEIGHT);

    // dirs[j][i] = 1 + MAP_LOC_CONVERSION + Mover.OPP_DIR[j][i].ordinal()*MAP_LOC_MASK
    public int[][] dirs;

    public int[][] computeDirs() {
        int MAP_LOC_CONVERSION = map.LOC_CONVERSION;
        int MAP_LOC_MASK = map.LOC_MASK;

        int[][] dirs = new int[3][3];
        for (int i = 0; i <= 2; i++) {
            for (int j = 0 ; j <= 2; j++) {
                dirs[j][i] = 1 + MAP_LOC_CONVERSION + Mover.OPP_DIR[j][i].ordinal() * MAP_LOC_MASK;
            }
        }
        return dirs;
    }

    public BFS(Controller c, int offset, Symmetry symmetry) {
        super(c);
        this.symmetry = symmetry;

        queueStart = 0;
        queueEnd = 0;
        queueX = new int[MAX_STATES];
        queueY = new int[MAX_STATES];
        queueR = new int[MAX_STATES];
        queueD = new MapLocation[MAX_STATES];
        seen = new boolean[SEEN_HASH * 2][SEEN_HASH * 2];
        map = new DirectionMap(c, offset);
        dirs = computeDirs();
    }

    public void add(MapLocation loc) throws GameActionException {
        int x = loc.x;
        int y = loc.y;
        queueX[queueEnd] = x;
        queueY[queueEnd] = y;
        queueR[queueEnd] = 1;
        queueD[queueEnd] = loc;
        map.broadcastDirection(loc, loc, Direction.OMNI);
        queueEnd++;
    }

    public boolean compute(int limit) throws GameActionException {
        int start = Clock.getBytecodeNum();
        int num = 0;

        // localize map
        DirectionMap map = this.map;

        //localize statics
        final TerrainTile NORMAL = TerrainTile.NORMAL;
        final TerrainTile UNKNOWN = TerrainTile.UNKNOWN;
        int LOC_X_CONVERSION = map.LOC_X_CONVERSION + 1;
        int LOC_Y_CONVERSION = map.LOC_Y_CONVERSION + 1;

//        int MAP_LOC_CONVERSION = map.LOC_CONVERSION;
        int MAP_HASH = map.HASH;
        int MAP_OFFSET = map.OFFSET;
//        int MAP_LOC_MASK = map.LOC_MASK;
        int offset = map.offset;
        int MAP_WIDTH = map.WIDTH;
        Symmetry symmetry = this.symmetry;

        //localize pointers
        int[] queueX = this.queueX;
        int[] queueY = this.queueY;
        int[] queueR = this.queueR;
        MapLocation[] queueD = this.queueD;
        boolean[][] seen = this.seen;
//        Direction[][] dirs = Mover.OPP_DIR;
        int[][] dirs = this.dirs;
        RobotController rc = this.rc;

        //localize variables
        int queueStart = this.queueStart;
        int queueEnd = this.queueEnd;

        while (Clock.getBytecodeNum() - start < limit) {
            num++;
            if (queueStart == queueEnd) {
                //unlocalize
                this.queueStart = queueStart;
                this.queueEnd = queueEnd;
                return true;
            }
            // pop
            int x = queueX[queueStart];
            int y = queueY[queueStart];
            int r = queueR[queueStart];
            MapLocation d = queueD[queueStart];
            queueStart++;
            r--;

            // stay same
            if (r > 0) {
                queueX[queueEnd] = x;
                queueY[queueEnd] = y;
                queueR[queueEnd] = r;
                queueD[queueEnd] = d;
                queueEnd++;
                continue;
            }

            // manual loop unrolling
            {
//                int i = -1;
//                int j = -1;
                if (!seen[x - 1 + LOC_X_CONVERSION][y - 1 + LOC_Y_CONVERSION]) {
                    int nx = x - 1;
                    int ny = y - 1;
                    seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[0][0]
                        );

                        //diag
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = ny;
                        queueR[queueEnd] = 3;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = -1;
//                int j = 0;
                if (!seen[x - 1 + LOC_X_CONVERSION][y + LOC_Y_CONVERSION]) {
                    int nx = x - 1;
                    seen[nx + LOC_X_CONVERSION][y + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, y));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, y)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((y + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[1][0]
                        );

                        // adj
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = y;
                        queueR[queueEnd] = 2;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = -1;
//                int j = 1;
                if (!seen[x - 1 + LOC_X_CONVERSION][y + 1 + LOC_Y_CONVERSION]) {
                    int nx = x - 1;
                    int ny = y + 1;
                    seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[2][0]
                        );

                        //diag
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = ny;
                        queueR[queueEnd] = 3;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = 0;
//                int j = -1;
                if (!seen[x + LOC_X_CONVERSION][y - 1 + LOC_Y_CONVERSION]) {
                    int ny = y - 1;
                    seen[x + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(x, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(x, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (x + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[0][1]
                        );

                        // adj
                        queueX[queueEnd] = x;
                        queueY[queueEnd] = ny;
                        queueR[queueEnd] = 2;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = 0;
//                int j = 1;
                if (!seen[x + LOC_X_CONVERSION][y + 1 + LOC_Y_CONVERSION]) {
                    int ny = y + 1;
                    seen[x + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(x, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(x, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (x + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[2][1]
                        );

                        // adj
                        queueX[queueEnd] = x;
                        queueY[queueEnd] = ny;
                        queueR[queueEnd] = 2;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = 1;
//                int j = -1;
                if (!seen[x + 1 + LOC_X_CONVERSION][y - 1 + LOC_Y_CONVERSION]) {
                    int nx = x + 1;
                    int ny = y - 1;
                    seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;

                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[0][2]
                        );

                        //diag
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = ny;
                        queueR[queueEnd] = 3;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = 1;
//                int j = 0;
                if (!seen[x + 1 + LOC_X_CONVERSION][y + LOC_Y_CONVERSION]) {
                    int nx = x + 1;
                    seen[nx + LOC_X_CONVERSION][y + LOC_Y_CONVERSION] = true;
                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, y));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, y)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((y + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[1][2]
                        );

                        // adj
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = y;
                        queueR[queueEnd] = 2;
                        queueD[queueEnd] = d;
                        queueEnd++;
                    }
                }
            }

            {
//                int i = 1;
//                int j = 1;
                if (!seen[x + 1 + LOC_X_CONVERSION][y + 1 + LOC_Y_CONVERSION]) {
                    int nx = x + 1;
                    int ny = y + 1;
                    seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;

                    TerrainTile t =  rc.senseTerrainTile(new MapLocation(nx, ny));
                    if (t == NORMAL ||
                        (t == UNKNOWN && symmetry != null && rc.senseTerrainTile(symmetry.get(nx, ny)) == NORMAL)
                    ) {
                        rc.broadcast(
                            offset + (nx + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % MAP_HASH) * MAP_HASH,
                            d.x + d.y * MAP_WIDTH + dirs[2][2]
                        );

                        //diag
                        queueX[queueEnd] = nx;
                        queueY[queueEnd] = ny;
                        queueD[queueEnd] = d;
                        queueR[queueEnd] = 3;
                        queueEnd++;
                    }
                }
            }

            // original loops
//
//            for (int i = -1; i <= 1; i++) {
//                for (int j = -1; j <= 1; j++) {
//                    int nx = x + i;
//                    int ny = y + j;
//                    if (!seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION]) {
//                        seen[nx + LOC_X_CONVERSION][ny + LOC_Y_CONVERSION] = true;
//                        MapLocation next = new MapLocation(nx, ny);
//                        if (rc.senseTerrainTile(next) == NORMAL) {
//                            rc.broadcast(
//                                offset + (nx + MAP_OFFSET) % MAP_HASH + ((ny + MAP_OFFSET) % HASH) * MAP_HASH,
//                                1 + (d.x + d.y * MAP_WIDTH + MAP_LOC_CONVERSION + dirs[j+1][i+1].ordinal()*MAP_LOC_MASK)
//                            );
////                            map.broadcastDirection(next, d, dirs[j+1][i+1]);
//
//                            if (i * j == 0) {
//                                // adj
//                                queueX[queueEnd] = nx;
//                                queueY[queueEnd] = ny;
//                                queueR[queueEnd] = 2;
//                                queueD[queueEnd] = d;
//                                queueEnd++;
//                            } else {
//                                //diag
//                                queueX[queueEnd] = nx;
//                                queueY[queueEnd] = ny;
//                                queueR[queueEnd] = 3;
//                                queueD[queueEnd] = d;
//                                queueEnd++;
//                            }
//                        }
//                    }
//                }
//            }

        }
		rc.setIndicatorString(0, "bfs: " + num);

        // unlocalize
        this.queueStart = queueStart;
        this.queueEnd = queueEnd;

        return false;
    }
}
