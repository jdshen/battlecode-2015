package attacker3_0.computation.symmetry;

/**
 * Created by jdshen on 1/24/15.
 */
public enum SymmetryType {
    X_REFLECTION, Y_REFLECTION, NEG_DIAGONAL, POS_DIAGONAL, ROTATION
}
