package attacker4_3.threads;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import attacker4_3.OtherGameConstants;
import attacker4_3.controllers.Controller;

/**
 * Created by jdshen on 1/22/15.
 */
public class SpawnThreadManager {
    private final RobotController rc;
    private final Controller c;
    private final RobotInfoManager allies;

    public int[][] threads; // thread id's
    public int[] sizes; // number of each thread

    public ObjectiveManager obj;

    public static final int length = RobotType.values().length;

    public SpawnThreadManager(Controller c, ObjectiveManager obj, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;
        this.allies = allies;
        this.obj = obj;
        threads = new int[length][OtherGameConstants.MAX_UNITS_TYPE];
        sizes = new int[length];
    }

    /**
     * Only call after obj is run
     * @param type
     * @param num
     * @throws GameActionException
     */
    public void setNumThread(RobotType type, int num) throws GameActionException {
        int i = type.ordinal();
        int[] threadList = threads[i];
        int size = sizes[i];
        ObjectiveManager obj = this.obj;

        if (size > num) {
            for (int j = num; j < size; j++) {
                // delete
                obj.close(threadList[j]);
            }
            sizes[i] = num;
        } else if (size < num) {
            RobotInfoManager allies = this.allies;
            Controller c = this.c;
            for (int j = size; j < num; j++) {
                // add
                int id = obj.add(new SpawnThread(c, type, allies));
                if (id == -1) {
                    sizes[i] = j;
                    return;
                }

                threadList[j] = id;
            }
            sizes[i] = num;
        }
    }
}
