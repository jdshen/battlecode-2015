package attacker4_3.threads;

import battlecode.common.*;
import attacker4_3.OtherGameConstants;
import attacker4_3.computation.MapSize;
import attacker4_3.controllers.Controller;
import attacker4_3.messaging.MessageType;

public class BuildOrderManager {
    private final Controller c;
    private MapSize mapSize;
    private final RobotController rc;
    private final ObjectiveManager obj;
    private final SpawnThreadManager spawns;

    private ObjectiveThread mainThread;
    private BuildingLocator locator;
    private boolean[] taken;

    private GeneralMicro micro;
    public RobotInfoManager allies;
    private EnemyInfoManager enemies;

    // launcher main line
    private static final RobotType[] mainLine = new RobotType[]{
        RobotType.MINERFACTORY,
    };

    private int mainLineIndex;

    public BuildThread trainingThread;
    public BuildThread techThread;
    public BuildThread helipadThread;
    public BuildThread aerospaceThread;
    public BuildThread supplyThread;

    public BuildOrderManager(Controller c, RobotInfoManager allies, EnemyInfoManager enemies, MapSize mapSize) {
        this.c = c;
        this.rc = c.rc;
        this.allies = allies;
        this.enemies = enemies;
        this.mapSize = mapSize;

        this.obj = new ObjectiveManager(c);
        this.locator = new BuildingLocator(c);
        this.taken = new boolean[OtherGameConstants.ROBOT_MAX_ID];
        mainLineIndex = 0;
        this.micro = new GeneralMicro(c, allies, enemies);
        this.spawns = new SpawnThreadManager(c, obj, allies);

        RobotType type = mainLine[mainLineIndex];
        MapLocation loc = locator.next(type);
        mainThread = new BuildThread(c, loc, type, allies);
        obj.add(mainThread);
        obj.add(new ComputationThread(c, MessageType.BFS_1, allies));
    }

    public void run() throws GameActionException {
        RobotInfo[] allEnemies = rc.senseNearbyRobots(c.teamhq, Controller.MAX_DIAGONAL_SQ, c.enemy);
        enemies.addBots(allEnemies);

        allies.clearBots();
        enemies.addDeletions();
        enemies.clearBots();

        obj.run(taken);

        // build threads
        if (mainLineIndex < mainLine.length) {
            if (mainThread.isSuccess()) {
                mainLineIndex++;
            }

            if (mainLineIndex < mainLine.length) {
                if (mainThread.isDone()) {
                    RobotType type = mainLine[mainLineIndex];
                    MapLocation loc = locator.next(type);
                    mainThread = new BuildThread(c, loc, type, allies);
                    obj.add(mainThread);
                }
            } else {
                initDynamicBuild();
            }
        } else {
            dynamicBuild();
        }

        // localize
        RobotType DRONE = RobotType.DRONE;
        int DRONE_ORD = RobotType.DRONE.ordinal();
        RobotType SUPPLYDEPOT = RobotType.SUPPLYDEPOT;
        int[] sizes = allies.sizes;
        int[] totals = allies.totals;
        SpawnThreadManager spawns = this.spawns;
        int MINER_ORD = RobotType.MINER.ordinal();

        // set num threads, called after obj.run

        int droneKillScore = enemies.killScore[DRONE_ORD];
        int droneNum = sizes[DRONE_ORD];
        int droneDeaths = (allies.totals[DRONE_ORD] - droneNum);

        int depots = sizes[RobotType.SUPPLYDEPOT.ordinal()];
        int supplyDiff = getSupply(depots + 1) - getSupply(depots);

        int droneTrueCost = (DRONE.oreCost + DRONE.supplyUpkeep * SUPPLYDEPOT.oreCost / supplyDiff);
        int droneDeathScore = droneDeaths * droneTrueCost;

//        int theirDroneDeaths = enemies.totals[DRONE_ORD] - enemies.sizes[DRONE_ORD];

//        rc.setIndicatorString(0, "drone kills: " + droneKillScore + ", drone deaths: " + droneDeaths + " vs " + theirDroneDeaths);
//        rc.setIndicatorString(2, "num = " + ((droneKillScore - droneDeathScore) / droneTrueCost + 2) + ", drone true cost = " + droneTrueCost);

        int initialDrones = (mapSize.height + mapSize.width + 1) / 40 + 200 /(mapSize.height + mapSize.width + 1) ;
        rc.setIndicatorString(0, mapSize.height + "," + mapSize.width);
        boolean scout = micro.harassCommand == MessageType.SCOUT;
        if (allies.totals[DRONE_ORD] < initialDrones) {
            spawns.setNumThread(DRONE, sizes[RobotType.HELIPAD.ordinal()]);
        } else if (scout) {
            if (droneNum < 2) {
                spawns.setNumThread(DRONE, sizes[RobotType.HELIPAD.ordinal()]);
            } else {
                spawns.setNumThread(DRONE, 0);
            }
        } else {
            if (droneNum < 3 || (droneNum < Math.min(10, (droneKillScore - droneDeathScore) / droneTrueCost + 2))) {
                spawns.setNumThread(DRONE, sizes[RobotType.HELIPAD.ordinal()]);
            } else {
                spawns.setNumThread(DRONE, 0);
            }
        }

        int initialMinerNum = (mapSize.height + mapSize.width + 1) / 12 + 25;
        if (totals[MINER_ORD] < initialMinerNum) {
            spawns.setNumThread(RobotType.MINER, sizes[RobotType.MINERFACTORY.ordinal()]);
        } else if (sizes[MINER_ORD] < 10) {
            spawns.setNumThread(RobotType.MINER, sizes[RobotType.MINERFACTORY.ordinal()]);
        } else if (totals[MINER_ORD] - sizes[MINER_ORD] < enemies.totals[MINER_ORD] - enemies.sizes[MINER_ORD]
            && sizes[MINER_ORD] < initialMinerNum
        ) {
            spawns.setNumThread(RobotType.MINER, sizes[RobotType.MINERFACTORY.ordinal()]);
        } else {
            spawns.setNumThread(RobotType.MINER, 0);
        }


        spawns.setNumThread(RobotType.LAUNCHER, sizes[RobotType.AEROSPACELAB.ordinal()]);

        if (sizes[RobotType.COMMANDER.ordinal()] < 1) {
            spawns.setNumThread(RobotType.COMMANDER, sizes[RobotType.TRAININGFIELD.ordinal()]);
        }

        if (sizes[RobotType.COMPUTER.ordinal()] < 1) {
            spawns.setNumThread(RobotType.COMPUTER, sizes[RobotType.TECHNOLOGYINSTITUTE.ordinal()]);
        } else {
            spawns.setNumThread(RobotType.COMPUTER, 0);
        }

        micro.run(taken);
    }

    public int getSupply(int depots) {
        if (depots < OtherGameConstants.SUPPLY_LOOKUP.length) {
            return OtherGameConstants.SUPPLY_LOOKUP[depots] + OtherGameConstants.SUPPLY_BASE;
        } else {
            return (int) (Math.pow(depots, GameConstants.SUPPLY_GEN_EXPONENT) * GameConstants.SUPPLY_GEN_BASE
                    + OtherGameConstants.SUPPLY_BASE);
        }
    }



    // first call
    public void initDynamicBuild() {
        // Helipad thread
        {
            RobotType type = RobotType.HELIPAD;
            MapLocation loc = locator.next(type);
            helipadThread = new BuildThread(c, loc, type, allies);
            obj.add(helipadThread);
        }

        // Technology institute thread
        {
            RobotType type = RobotType.TECHNOLOGYINSTITUTE;
            MapLocation loc = locator.next(type);
            techThread = new BuildThread(c, loc, type, allies);
            obj.add(techThread);
        }
    }

    public void dynamicBuild() {
        if (helipadThread.isDone()) {
            if (allies.sizes[RobotType.HELIPAD.ordinal()] >= 1) {
                if (aerospaceThread == null || aerospaceThread.isDone()) {
                    {
                        RobotType type = RobotType.AEROSPACELAB;
                        MapLocation loc = locator.next(type);
                        aerospaceThread = new BuildThread(c, loc, type, allies);
                        obj.add(aerospaceThread);
                    }

                    if (rc.getTeamOre() > RobotType.AEROSPACELAB.oreCost * 2) {
                        RobotType type = RobotType.AEROSPACELAB;
                        obj.add(new BuildThread(c, locator.next(type), type, allies));
                    }
                }
            } else {
                RobotType type = RobotType.HELIPAD;
                MapLocation loc = locator.next(type);
                helipadThread = new BuildThread(c, loc, type, allies);
                obj.add(helipadThread);
            }
        }

        if (techThread.isDone()) {
            if (allies.sizes[RobotType.TECHNOLOGYINSTITUTE.ordinal()] >= 1) {
                if (trainingThread == null) {
                    RobotType type = RobotType.TRAININGFIELD;
                    MapLocation loc = locator.next(type);
                    trainingThread = new BuildThread(c, loc, type, allies);
                    obj.add(trainingThread);
                }
            } else {
                RobotType type = RobotType.TECHNOLOGYINSTITUTE;
                MapLocation loc = locator.next(type);
                techThread = new BuildThread(c, loc, type, allies);
                obj.add(techThread);
            }
        }

        if (supplyThread == null || supplyThread.isDone()) {
            supplyThread = null;
            // check supply
            RobotType[] types = RobotType.values();
            int[] sizes = allies.sizes;
            int supply = 0;
            for (int i = types.length - 1; i >= 0; i--) {
                RobotType type = types[i];
                int size = sizes[i];
                supply += size * type.supplyUpkeep;
            }

            int depots = sizes[RobotType.SUPPLYDEPOT.ordinal()];
            int curSupply;
            if (depots < OtherGameConstants.SUPPLY_LOOKUP.length) {
                curSupply = OtherGameConstants.SUPPLY_LOOKUP[depots] + OtherGameConstants.SUPPLY_BASE;
            } else {
                curSupply =
                    (int) (Math.pow(depots, GameConstants.SUPPLY_GEN_EXPONENT) * GameConstants.SUPPLY_GEN_BASE
                        + OtherGameConstants.SUPPLY_BASE);
            }

//            rc.setIndicatorString(2, supply + ", " + curSupply);
            // TODO 5 / 7  = MAGIC CONSTANT
            if (supply * 7 > curSupply * 5) {
                {
                    RobotType type = RobotType.SUPPLYDEPOT;
                    MapLocation loc = locator.next(type);
                    supplyThread = new BuildThread(c, loc, type, allies);
                    obj.add(supplyThread);
                }

                if (rc.getTeamOre() > RobotType.SUPPLYDEPOT.oreCost * 2) {
                    RobotType type = RobotType.SUPPLYDEPOT;
                    MapLocation loc = locator.next(type);
                    obj.add(new BuildThread(c, loc, type, allies));
                }


                if (rc.getTeamOre() > RobotType.SUPPLYDEPOT.oreCost * 3) {
                    RobotType type = RobotType.SUPPLYDEPOT;
                    MapLocation loc = locator.next(type);
                    obj.add(new BuildThread(c, loc, type, allies));
                }
            }
        }
    }

}
