package attacker4_3.messaging;

import battlecode.common.*;
import attacker4_3.controllers.Controller;


/**
 * Class for broadcasting to and reading from multiple id-channels
 *
 * Can also post/read messages to specific non-id channels
 */
public class CommandMessager {
    private RobotController rc;
    private Controller c;

    public static MessageType[] msgs = MessageType.values();
    public MapLocation lastLoc;
    public MessageType lastMsg;

    public final int ID_CHAN_MOD = MessagingConstants.ID_CHAN_MOD;
    public final int ID_MSG_MASK = MessagingConstants.ID_MSG_MASK;
    public final int ID_CHECK_MOD = MessagingConstants.ID_CHECK_MOD;
    public final int LOC_MASK = MessagingConstants.LOC_MASK;
    public final int WIDTH = 2*GameConstants.MAP_MAX_WIDTH;
    public final int HEIGHT = 2*GameConstants.MAP_MAX_HEIGHT;

    // added to locations when being broadcasted (to make positive), subtracted off when reading
    public final int LOC_X_CONVERSION;
    public final int LOC_Y_CONVERSION;
    public final int LOC_CONVERSION;

    public CommandMessager(Controller c){
        this.rc = c.rc;
        this.c = c;
        this.LOC_X_CONVERSION = - c.teamhq.x + GameConstants.MAP_MAX_WIDTH;
        this.LOC_Y_CONVERSION = - c.teamhq.y + GameConstants.MAP_MAX_HEIGHT;
        this.LOC_CONVERSION = LOC_X_CONVERSION + WIDTH * LOC_Y_CONVERSION;
    }

    /**
     * Attempt to read a channel associated with specific robot. Which channel type (input/output of robot) determined by offset.
     * Expects an id check in the message.
     * Returns whether the channel had data
     * Saves the actual map location (converted back from broadcast form) as lastLoc
     * @param id ID of target
     * @param offset Offset of channel type of target robot, according to MessagingConstants
     * @throws GameActionException
     * @return whether something was received
     */
    public boolean readIDChannel(int id, int offset) throws GameActionException{
        int chan = id % ID_CHAN_MOD;
        int data = rc.readBroadcast(offset + chan);
        if(data == 0){
            return false;
        }else{
            if(data % ID_MSG_MASK == (1 + (id % ID_CHECK_MOD))){
                data = data/ID_MSG_MASK;
                lastMsg = msgs[data/(LOC_MASK)];
                lastLoc = new MapLocation(
                    (data % WIDTH) - LOC_X_CONVERSION,
                    ((data / WIDTH) % HEIGHT) - LOC_Y_CONVERSION
                );
                return true;
            }
            return false;
        }
    }

    /**
     * Broadcast to channel associated with specified robot. Which channel type (input/output) determined by offset from comms protocol.
     * Puts in id tag check in the message.
     * @param id ID of robot
     * @param offset Offset for channel type (input1, output1, etc)
     * @param msg Message type
     * @param loc the actual map location - converted to positive in-line
     * @throws GameActionException
     */
    public void broadcastIDChannel(int id, int offset, MessageType msg, MapLocation loc) throws GameActionException{
        int chan = id % ID_CHAN_MOD;
        rc.broadcast(
            offset + chan,
            1 + (id % ID_CHECK_MOD) + ID_MSG_MASK*(loc.x + loc.y * WIDTH + LOC_CONVERSION + msg.ordinal()*LOC_MASK)
        );
    }

    public void broadcastIDChannel(int id, int offset, int msgLoc) throws GameActionException {
        rc.broadcast(offset + id % ID_CHAN_MOD, (id % ID_CHECK_MOD) + msgLoc);
    }

    /**
     * Computes msgLoc for use in broadcastIDChannel with 3 arguments
     * Use when sending the same message to lots of bots
     * @param msg
     * @param loc
     * @return
     */
    public int computeMessage(MessageType msg, MapLocation loc) {
        return ID_MSG_MASK*(loc.x + loc.y * WIDTH + LOC_CONVERSION + msg.ordinal()*LOC_MASK) + 1;
    }

    /**
     * clear channel (set to 0) for id input/output channel
     * @param id
     * @param offset
     * @throws GameActionException
     */
    public void clearIDChannel(int id, int offset) throws GameActionException{
        rc.broadcast(offset + (id % ID_CHAN_MOD), 0 );
    }

    /**
     * Broadcast to a general channel. No id-checks
     * @param channel Global channel to broadcast to.
     * @param msg Messagetype
     * @param loc the actual Location - converted to positive in-line
     * @throws GameActionException
     */
    public void broadcastChannel(int channel, MessageType msg, MapLocation loc) throws GameActionException{
        int x = loc.x + LOC_X_CONVERSION;
        int y = loc.y + LOC_Y_CONVERSION;
        rc.broadcast(channel, 1 + (x + y*WIDTH) + msg.ordinal()*LOC_MASK);
    }

    public boolean readChannel(int channel) throws GameActionException{
        int data = rc.readBroadcast(channel);
        if(data != 0){
            data--;
            lastMsg = msgs[data/(LOC_MASK)];
            lastLoc = new MapLocation(
                    (data % WIDTH) - LOC_X_CONVERSION,
                    ((data / WIDTH) % HEIGHT) - LOC_Y_CONVERSION
            );
            return true;
        }
        return false;
    }
}
