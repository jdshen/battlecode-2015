package attacker4_3.messaging;

import battlecode.common.*;
import attacker4_3.OtherGameConstants;
import attacker4_3.controllers.Controller;

/**
 * Created by kevin on 1/21/15.
 */
public class ScoutingMessager {
    private RobotController rc;

    public final int SCOUT_CHANNEL_START = MessagingConstants.SCOUT_CHANNEL_START;
    public final int BLOCK_SIZE = MessagingConstants.SCOUT_BLOCK_SIZE;
    private final int MAX_OFFSET = MessagingConstants.SCOUT_OFFSET;
    private final int HASH = MessagingConstants.SCOUT_HASH;
    public ScoutingMessager(Controller c){
        this.rc = c.rc;
    }

    // Update that a location is scouted - these values should be modded already
    public void update(int xLoc, int yLoc) throws GameActionException {
        rc.broadcast(SCOUT_CHANNEL_START + xLoc + yLoc*HASH, Clock.getRoundNum());
    }

    public void forceUpdate(int xLoc, int yLoc, int val) throws GameActionException {
        rc.broadcast(SCOUT_CHANNEL_START + xLoc + yLoc*HASH, val);
    }

    // input values should be blocked and modded already
    public int getLastScout(int xLoc, int yLoc) throws  GameActionException {
        return rc.readBroadcast(SCOUT_CHANNEL_START + xLoc + yLoc*HASH);
    }

    public int getLastScout(MapLocation loc) throws GameActionException {

        int xLoc = ((loc.x + MAX_OFFSET)/ BLOCK_SIZE) % HASH;
        int yLoc = ((loc.y + MAX_OFFSET)/ BLOCK_SIZE) % HASH;

        return rc.readBroadcast(SCOUT_CHANNEL_START + xLoc + yLoc*HASH);
    }
}
