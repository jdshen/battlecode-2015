package attacker4_3.computation;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import attacker4_3.controllers.Controller;
import attacker4_3.messaging.MessagingConstants;
import attacker4_3.computation.symmetry.*;
import attacker4_3.util.FastLocSet;

/**
 * Created by jdshen on 1/23/15.
 */
public class SymmetryComputation extends Computation {
    public Symmetry symmetry;
    public Symmetry rotation;
    public Symmetry reflection;

    // newX = x * multX + addX
    // newY = y * multY + addY

    public SymmetryComputation(Controller c) throws GameActionException {
        super(c);
        if (computeHQAndTowers()) {
            rc.broadcast(MessagingConstants.SYMMETRY_COMPUTATION_CHANNEL, 1 + symmetry.getType().ordinal());
        }
    }

    public boolean computeHQAndTowers() {
        Controller c = this.c;
        int ex = c.enemyhq.x;
        int ey = c.enemyhq.y;
        int tx = c.teamhq.x;
        int ty = c.teamhq.y;

        if (ex == tx) {
            reflection = new XReflection(ey + ty);
        } else if (ey == ty) {
            reflection = new YReflection(ex + tx);
        } else if (ex - ty == tx - ey) {
            reflection = new PosDiagonalReflection(ex - ty);
        } else if (ex + ty == tx + ey) {
            reflection = new NegDiagonalReflection(ex + ty);
        } else {
            symmetry = new Rotation(ex + tx, ey + ty);
            return true;
        }
        rotation = new Rotation(ex + tx, ey + ty);

        FastLocSet enemyTowerSet = new FastLocSet();
        boolean[][] has = enemyTowerSet.has;
        int HASH = FastLocSet.HASH;
        int OFFSET = FastLocSet.OFFSET;
        MapLocation[] enemyTowers = rc.senseEnemyTowerLocations();
        for (int i = enemyTowers.length - 1; i >= 0; i--) {
            has[(enemyTowers[i].x + OFFSET) % HASH][(enemyTowers[i].y + OFFSET) % HASH] = true;
        }

        MapLocation[] towers = rc.senseTowerLocations();
        Symmetry reflection = this.reflection;
        Symmetry rotation = this.rotation;
        for (int i = towers.length - 1; i >= 0; i--) {
            {
                MapLocation loc = reflection.get(towers[i]);
                if (!has[(loc.x + OFFSET) % HASH][(loc.y + OFFSET) % HASH]) {
                    // is rotation
                    symmetry = rotation;
                    return true;
                }
            }

            {
                MapLocation loc = rotation.get(towers[i]);
                if (!has[(loc.x + OFFSET) % HASH][(loc.y + OFFSET) % HASH]) {
                    // is reflection
                    symmetry = reflection;
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        // TODO do more than just HQ and towers
        return true;
    }

}
