package attacker4_3.computation.symmetry;

import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/23/15.
 */
public class XReflection implements Symmetry {
    private int centerY2;

    public XReflection(int centerY2) {
        this.centerY2 = centerY2;
    }

    @Override
    public SymmetryType getType() {
        return SymmetryType.X_REFLECTION;
    }

    @Override
    public MapLocation get(MapLocation loc) {
        return new MapLocation(loc.x, centerY2 - loc.y);
    }

    @Override
    public MapLocation get(int x, int y) {
        return new MapLocation(x, centerY2 - y);
    }
}
