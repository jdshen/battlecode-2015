package attacker4_3.computation;

import battlecode.common.*;
import attacker4_3.controllers.Controller;
import attacker4_3.messaging.DirectionMessager;
import attacker4_3.movement.Mover;

/**
 * Created by jdshen on 1/22/15.
 */
public class CompressedBFS extends Computation {
    private int MAX_STATES = 60000;

    public int queueStart;
    public int queueEnd;
    public int[] queueX;
    public int[] queueY;
    public int[] queueR;
    public boolean[][] seen;
    public DirectionMessager map;

    public MapLocation loc;
    private static final int HASH = 2 + Math.max(GameConstants.MAP_MAX_WIDTH, GameConstants.MAP_MAX_HEIGHT);

    public CompressedBFS(Controller c, MapLocation loc, int offset) throws GameActionException {
        super(c);
        queueStart = 0;
        queueEnd = 0;
        queueX = new int[MAX_STATES];
        queueY = new int[MAX_STATES];
        queueR = new int[MAX_STATES];
        seen = new boolean[HASH * 2][HASH * 2];
        map = new DirectionMessager(rc, offset);

        int x = loc.x;
        int y = loc.y;
        queueX[queueEnd] = x;
        queueY[queueEnd] = y;
        queueR[queueEnd] = 1;
        map.broadcastLocation(loc);
        queueEnd++;
    }

    @Override
    public boolean compute(int limit) throws GameActionException {
        int start = Clock.getBytecodeNum();
        int num = 0;

        //localize statics

        //localize pointers
        int[] queueX = this.queueX;
        int[] queueY = this.queueY;
        int[] queueR = this.queueR;
        boolean[][] seen = this.seen;
        Direction[][] dirs = Mover.OPP_DIR;
        DirectionMessager map = this.map;

        //localize variables
        int queueStart = this.queueStart;
        int queueEnd = this.queueEnd;
        final int HASH = this.HASH;
        final TerrainTile NORMAL = TerrainTile.NORMAL;
        MapLocation d = loc;
        RobotController rc = this.rc;

        // messaging things
        int HASH2 = DirectionMessager.HASH;
        int OFFSET = DirectionMessager.OFFSET;
        int offset = map.offset;
        int[] loMasks = DirectionMessager.loMasks;
        int[] dirToInt = DirectionMessager.dirToInt;

        while (Clock.getBytecodeNum() - start < limit) {
            num++;
            if (queueStart == queueEnd) {
                //unlocalize
                this.queueStart = queueStart;
                this.queueEnd = queueEnd;
                return true;
            }
            // pop
            int x = queueX[queueStart];
            int y = queueY[queueStart];
            int r = queueR[queueStart];
            queueStart++;

            r--;

            // stay same
            if (r > 0) {
                queueX[queueEnd] = x;
                queueY[queueEnd] = y;
                queueR[queueEnd] = r;
                queueEnd++;
                continue;
            }

            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    int nx = x + i;
                    int ny = y + j;
                    if (!seen[nx % HASH + HASH][ny % HASH + HASH]) {
                        seen[nx % HASH + HASH][ny % HASH + HASH] = true;
                        MapLocation next = new MapLocation(nx, ny);
                        if (rc.senseTerrainTile(next) == NORMAL) {
                            map.broadcastDirection(next, dirs[j+1][i+1]);

                            if (i * j == 0) {
                                // adj
                                queueX[queueEnd] = nx;
                                queueY[queueEnd] = ny;
                                queueR[queueEnd] = 2;
                                queueEnd++;
                            } else {
                                //diag
                                queueX[queueEnd] = nx;
                                queueY[queueEnd] = ny;
                                queueR[queueEnd] = 3;
                                queueEnd++;
                            }
                        }
                    }
                }
            }

        }
		rc.setIndicatorString(0, "bfs: " + num);

        // unlocalize
        this.queueStart = queueStart;
        this.queueEnd = queueEnd;

        return false;
    }
}
