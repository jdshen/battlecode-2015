package attacker4_3.controllers;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker4_3.actors.Action;
import attacker4_3.actors.Actor;
import attacker4_3.actors.ObjectiveListener;
import attacker4_3.actors.StructureSupplyActor;

/**
 * Initial controller for all structures, replace with finer tuned ones
 * if necessary
 */
public class StructureController extends Controller {
    private Actor supplyActor;
    private ObjectiveListener objectives;

    public StructureController(RobotController rc) {
        super(rc);
        objectives = new ObjectiveListener(this);
        supplyActor = new StructureSupplyActor(this, 1500); // dont go over on computation
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        Action action;
        action = objectives.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
