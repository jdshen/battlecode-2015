package attacker4_3.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import attacker4_3.actors.Action;
import attacker4_3.actors.DroneHarasser;
import attacker4_3.actors.Scout;
import attacker4_3.actors.SupplyActor;
import attacker4_3.messaging.MessagingConstants;
import attacker4_3.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class DroneController extends Controller {
    private Scout scout;
    private DroneHarasser attacker;
    private SupplyActor supplyActor;
    private RobotMessager in;

    private MapLocation scoutTarget;

    public DroneController(RobotController rc) {
        super(rc);
        scout = new Scout(this);
        attacker = new DroneHarasser(this, this.enemyhq);
        supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
        in = new RobotMessager(this, rc.getID(), MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        scoutTarget = enemyhq;
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        Action action;
        if (updateAttack()) {
            action = attacker.act(move, attack);
            move = move && !action.moved;
            attack = attack && !action.moved;
        } else {
            action = scout.act(move, attack);
            move = move && !action.moved;
            attack = attack && !action.moved;
        }

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }

    public boolean updateAttack() throws GameActionException {
        if (in.readMsg()) {
            attacker.setTarget(in.lastLoc);
            rc.setIndicatorString(0, in.lastMsg + " " + in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(true);
                    return true;
                case ASSAULT:
                    attacker.ignoreTower(in.lastLoc);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(false);
                    return true;
                case DESTROY:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(false);
                    attacker.setAvoidEnemyRange(false);
                    return true;
                case SCOUT:
                    attacker.ignoreTower(this.teamhq);
                    attacker.setAvoidTowers(true);
                    attacker.setAvoidEnemyRange(true);
                    if (in.lastLoc.equals(this.enemyhq)) {
                        scout.setTarget(rc.getLocation());
                    } else if (!in.lastLoc.equals(scoutTarget)) {
                        scoutTarget = in.lastLoc;
                        scout.setTarget(in.lastLoc);
                    }
                    return false;
            }
        }
        return false;
    }
}
