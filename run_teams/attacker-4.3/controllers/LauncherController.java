package attacker4_3.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker4_3.actors.Action;
import attacker4_3.actors.BasherActor;
import attacker4_3.actors.LauncherActor;
import attacker4_3.actors.SupplyActor;
import attacker4_3.messaging.MessagingConstants;
import attacker4_3.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class LauncherController extends Controller {
    private final SupplyActor supplyActor;
    private final RobotMessager in;
    private LauncherActor actor;

    public LauncherController(RobotController rc) {
        super(rc);
        this.actor = new LauncherActor(this, enemyhq);

        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        if (in.readMsg()) {
            actor.setTarget(in.lastLoc);
            switch (in.lastMsg) {
//                case HARASS:
//                    actor.ignoreTower(this.teamhq);
//                    actor.setAvoidTowers(true);
//                    break;
//                case ASSAULT:
//                    actor.ignoreTower(in.lastLoc);
//                    actor.setAvoidTowers(true);
//                    break;
//                case DESTROY:
//                    actor.ignoreTower(this.teamhq);
//                    actor.setAvoidTowers(false);
//                    break;
            }
        }

        Action action = actor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved && ! action.attacked;
        attack = attack && !action.moved && ! action.attacked;
    }
}
