package attacker4_3.controllers;

import battlecode.common.*;

public abstract class Controller {
    //shortcuts for basic information
    public final int id;
    public final Team team;
    public final Team enemy;
    public final MapLocation teamhq;
    public final MapLocation enemyhq;
    public final RobotController rc;
    public final RobotType type;
    public long[] teamMemory;


    //shortcuts for type
    public final double attackPower;
    public final int attackDelay;
    public final int moveDelay;
    public final int loadingDelay;
    public final int cooldownDelay;
    public final int bytecodeLimit;
    public final int attackRadiusSquared;
    public final int sensorRadiusSquared;
    public final int supplyUpkeep;
    public final double maxHealth;


    //shortcuts for commonly used values
    public static final int HQ_MAX_ATTACK_RADIUS_SQ = GameConstants.HQ_BUFFED_ATTACK_RADIUS_SQUARED;
    public static final int SPLASH_RADIUS_SQ = 2;
    public static final int MAX_DIAGONAL_SQ = getMaxDiagonalSq();

    private static int getMaxDiagonalSq() {
        int height = GameConstants.MAP_MAX_HEIGHT;
        int width = GameConstants.MAP_MAX_WIDTH;
        return height * height + width * width;
    }

    public Controller(RobotController rc) {
        team = rc.getTeam();
        enemy = team.opponent();
        teamhq = rc.senseHQLocation();
        enemyhq = rc.senseEnemyHQLocation();
        id = rc.getID();
        type = rc.getType();
        teamMemory = rc.getTeamMemory();
        this.rc = rc;

        attackDelay = type.attackDelay;
        moveDelay = type.movementDelay;
        loadingDelay = type.loadingDelay;
        cooldownDelay = type.cooldownDelay;

        attackPower = type.attackPower;
        supplyUpkeep = type.supplyUpkeep;
        sensorRadiusSquared = type.sensorRadiusSquared;
        attackRadiusSquared = type.attackRadiusSquared;
        maxHealth = type.maxHealth;
        bytecodeLimit = type.bytecodeLimit;

        //MAGICAL CONSTANT
        switch(id % 11){
            case 0:
            case 6:
            case 5:
            case 7:
                rc.setIndicatorString(0, "hipp0");
                break;
            case 8:
            case 3:
            case 4:
            case 1:
                rc.setIndicatorString(0, "0tter");
                break;
            default:
                rc.setIndicatorString(0, "ANIM0L");
                break;
        }
    }

    public abstract void run() throws GameActionException;

}
