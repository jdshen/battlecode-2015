package attacker4_3.actors;

import battlecode.common.*;
import attacker4_3.OtherGameConstants;
import attacker4_3.controllers.Controller;
import attacker4_3.movement.Mover;
import attacker4_3.util.AttackerLookups;

/**
 * DroneAttacker - attacks thing that it can kill fastest
 *
 * Created by jdshen on 1/7/15.
 */
public class DroneHarasser extends Actor {
    private MapLocation target;

    public DroneAttacker attacker;
    public AttackerLookups lookup;

    public Direction[] intToDirs = Direction.values();
    public boolean avoidTowers;
    public boolean avoidEnemyRange;
    public MapLocation towerLoc;

    private Direction lastMissileDirection = Direction.NONE;
    private int lastMissileRound = 0;

    private Mover mover;

    private Direction directionTowardApproachable = Direction.OMNI;

    public DroneHarasser(Controller c, MapLocation target) {
        super(c);
        this.target = target;
        attacker = new DroneAttacker(c);
        lookup = new AttackerLookups();
        this.avoidTowers = true;
        this.towerLoc = c.teamhq;
        this.mover = new Mover(c);
    }

    public void setTarget(MapLocation target){
        this.target = target;
    }

    public void setAvoidTowers(boolean avoidTowers){
        this.avoidTowers = avoidTowers;
    }

    public void ignoreTower(MapLocation towerLoc){
        this.towerLoc = towerLoc;
    }

    public void setAvoidEnemyRange(boolean avoidEnemyRange){ this.avoidEnemyRange = avoidEnemyRange; }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        lastMissileRound --;
        RobotController rc = this.rc;
        Controller c = this.c;

        MapLocation here = rc.getLocation();
        int hereX = here.x;
        int hereY = here.y;

//        rc.setIndicatorString(1, "no move top");
        if (!move && !attack) {
            // just wait
            return new Action(false, false);
        }

        // movement decision
        // try to move towards target. Prioritize staying out of range
        int[] moveOptions = mover.getDroneInitialMoveOptions(here, avoidTowers, towerLoc);
        boolean[] trueCanMove = new boolean[intToDirs.length];
        for(int i = intToDirs.length - 1; i >= 0; i --){
            trueCanMove[i] = moveOptions[i] <= 1;
        }

        boolean missiles = false;
        RobotInfo[] enemies = rc.senseNearbyRobots(35, c.enemy); // MAGIC CONSTANT
        int numEnemies = enemies.length;
        RobotInfo en;
        MapLocation there;
        Direction[] attacked;
        // Remember which enemies are approachable
        Direction[] approachable = new Direction[numEnemies];
        MapLocation[] approachableLocs = new MapLocation[numEnemies];
        int numApproachable = 0;
        int idx = 0;
        if(avoidEnemyRange) { //update moveOptions to reflect enemy attacks
            for (int i = numEnemies - 1; i >= 0; i--) {
                en = enemies[i];
                there = en.location;
                int attackRadius = en.type.attackRadiusSquared;

                switch(en.type){
                    case SUPPLYDEPOT:
                    case TECHNOLOGYINSTITUTE:
                    case BARRACKS:
                    case HELIPAD:
                    case TRAININGFIELD:
                    case TANKFACTORY:
                    case MINERFACTORY:
                    case HANDWASHSTATION:
                    case AEROSPACELAB:
                    case BEAVER:
                    case COMPUTER:
                    case MINER:
                        approachable[numApproachable] = here.directionTo(there);
                        approachableLocs[numApproachable] = there;
                        numApproachable ++;
                        continue; // ignore MINER and BEAVER attack ranges
                    case LAUNCHER:
                        if (en.missileCount > 0) {
                            attacked = lookup.getDirs(24, there.x - hereX, there.y - hereY);
                        } else {
                            attacked = new Direction[0];
                        }
                        break;
                    case MISSILE:
                        lastMissileDirection = here.directionTo(there);
                        lastMissileRound = Clock.getRoundNum();
                        attacked = lookup.getDirs(15, there.x - hereX, there.y - hereY);
                        break;
                    case BASHER:
                        attacked = lookup.getDirs(8, there.x - hereX, there.y - hereY);
                        break;
                    case COMMANDER:
                        attacked = lookup.getDirs(24, there.x - hereX, there.y - hereY);
                        break;
                    default:
                        attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
                        for (int j = attacked.length - 1; j >= 0; j--) {
                            idx = attacked[j].ordinal();
                            moveOptions[idx] = moveOptions[idx] + 4;
                            trueCanMove[idx] = false;
                        }
                        continue;
                }
                for (int j = attacked.length - 1; j >= 0; j--) {
                    idx = attacked[j].ordinal();
                    moveOptions[idx] = moveOptions[idx] + 2;
                }
                attacked = lookup.getDirs(attackRadius, there.x - hereX, there.y - hereY);
                for (int j = attacked.length - 1; j >= 0; j--) {
                    idx = attacked[j].ordinal();
                    moveOptions[idx] = moveOptions[idx] + 2;
                    trueCanMove[idx] = false;
                }
            }
        }
        boolean inEnemyRange = moveOptions[Direction.OMNI.ordinal()] > 1;
        if(!inEnemyRange || !avoidEnemyRange) { // ONLY ATTACK if we are hitting things that don't fight back
            Action act = attacker.act(move, attack);
            if(act.attacked){
                // Can no longer move and attack same turn
                return new Action(false, true);
            }
        }

        if(numApproachable == 0){
            directionTowardApproachable = Direction.OMNI;
        }

//        rc.setIndicatorString(1, "no move");
        if(!move) {
            return new Action(false, false);
        }

        // try to approach targets units
        if(directionTowardApproachable != Direction.OMNI){
            // we were going towards a target
            Direction fromBack = directionTowardApproachable.opposite();
            Direction fromLeft = fromBack.rotateLeft();
            Direction fromRight = fromBack.rotateRight();
            for (int i = numApproachable - 1; i >= 0; i--){
                Direction toEn = approachable[i];
                if (toEn == fromBack || toEn == fromLeft || toEn == fromRight){
                    // circled behind something
                    int distance = here.distanceSquaredTo(approachableLocs[i]);
                    if(distance > c.attackRadiusSquared) {
                        if(mover.makeDroneSafeMove(toEn, moveOptions, false)){
                            return new Action(true, false);
                        }
                    }
                    else {
                        if(!inEnemyRange) {
                            return new Action(false, false);
                        }
                    }
                }
                else {
                    if(mover.makeDroneSafeMove(toEn, moveOptions, false)){
                        return new Action(true, false);
                    }
                }
            }
            directionTowardApproachable = Direction.OMNI;
        }
        else {
            for (int i = numApproachable - 1; i >= 0; i--) {
                if (mover.makeDroneSafeMove(approachable[i], moveOptions, true)) {
                    if (directionTowardApproachable == Direction.OMNI) {
                        directionTowardApproachable = approachable[i];
                    }
                    return new Action(true, false);
                }
            }
        }

        if(lastMissileRound > Clock.getRoundNum() - 7){
            idx = lastMissileDirection.ordinal();
            moveOptions[idx] += 2;
            if(lastMissileRound > Clock.getRoundNum() - 5){
                idx = lastMissileDirection.rotateLeft().ordinal();
                moveOptions[idx] += 2;
                idx = lastMissileDirection.rotateRight().ordinal();
                moveOptions[idx] += 2;
            }
        }

//        rc.setIndicatorString(1, "to target");
        // try to move toward target. Try perpendicular motion. Don't step in range.
        Direction dir = here.directionTo(target);
        if (mover.makeDroneSafeMove(dir, moveOptions, true)) {
            return new Action(true, false);
        }


        // if currently targetted. try retreating.
        dir = dir.opposite();
//        rc.setIndicatorString(1, "retreating");
        if(mover.makeDroneSafeMove(dir, moveOptions, false)){
            return new Action(true, false);
        }

        // TODO shortcut to here and make better logic

        // TARGETTED EVERYWHERE - pick the best direction to run in. Anything is better than nothing
        // Penalize moving towards enemies
        // Bytecode cost: ~100 / enemy
        for (int i = numEnemies -1; i >= 0; i--) {
            en = enemies[i];
            there = en.location;
            dir = here.directionTo(there);
            idx = dir.ordinal();
            moveOptions[idx] = moveOptions[idx] + 6;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] + 4;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] + 4;
            dir = dir.opposite();
            idx = dir.opposite().ordinal();
            moveOptions[idx] = moveOptions[idx] - 4;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] - 2;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] - 2;
        }

        dir = Direction.OMNI;
        Direction curDir;
        int best = moveOptions[dir.ordinal()];
        for(int i = moveOptions.length - 1; i>=0; i--){
            curDir = intToDirs[i];
            if(!rc.canMove(curDir)){
                trueCanMove[i] = false; //update this with can moves
                continue;
            }
            if(moveOptions[i] < best){
                best = moveOptions[i];
                dir = curDir;
            }
        }


//        rc.setIndicatorString(1, "super retreating");

        if(dir != Direction.OMNI){
            if(mover.makeTrueDroneSafeMove(dir, trueCanMove, moveOptions)) {
                return new Action(true, false);
            }
        }

//        rc.setIndicatorString(1, "just trying every direction");
        for(int i = moveOptions.length - 1; i >= 0; i--) {
            if(trueCanMove[i]){
                rc.move(intToDirs[i]);
                return new Action(true, false);
            }
        }
        // attack if we can't run
        Action act = attacker.act(move, attack);

        return new Action(false, act.attacked);
    }



}
