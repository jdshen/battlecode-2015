package attacker2_3.threads;

import battlecode.common.*;
import attacker2_3.controllers.Controller;
import attacker2_3.messaging.MessageType;
import attacker2_3.messaging.MessagingConstants;
import attacker2_3.messaging.RobotMessager;

public class BuildThread extends ObjectiveThread {
    private final RobotType type;
    private final RobotInfoManager manager;
    private MapLocation loc;
    private MessageType message;
    private RobotMessager in;
    private RobotMessager out;

    private MapLocation building; // location of building
    private boolean connected; // connected to a beaver

    public BuildThread(Controller c, MapLocation loc, RobotType type, RobotInfoManager manager) {
        super(c);
        this.type = type;
        // TODO possibly replace this with something that generates map locations
        this.message = MessageType.getBuildMessage(type);
        this.loc = loc;
        this.connected = false;
        this.id = -1;
        this.manager = manager;
    }

    @Override
    public boolean run(RobotInfo info) throws GameActionException {
        if (info == null) {
            connected = false;
            return false;
        }

        MapLocation buildingLocation = info.buildingLocation;
        if (buildingLocation == null && building != null) {
            // robot not building, check building to see if done
            RobotInfo buildingInfo = rc.senseRobotAtLocation(building);

            if (buildingInfo != null && buildingInfo.team == c.team && buildingInfo.type == type) {
                // building completed, done
                success = true;
                manager.addBot(type, buildingInfo.ID);
            }

            // TODO retry with another location
            // either way, done with the thread
            close();
            return true;
        }

        if (buildingLocation != null) {
            // make sure the building location is up to date
            if (building != buildingLocation) {
                building = buildingLocation;
            }
        }

        // track bot for acknowledgement
        if (out.readMsg()) {
            if (out.lastMsg.ack) {
                out.clearChannel();
                return false;
            }

            if (out.lastMsg == MessageType.NEARBY_ENEMY) {
                out.clearChannel();
                // robot might die, try building somewhere else
                // TODO retry with another location
                close();
                return true;
            }

            // no ack, find another bot
            in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
            connected = false;
            return false;
        } else {
            // bad message, treat as no ack
            in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
            connected = false;
            return false;
        }
    }

    @Override
    public void find(boolean[] taken) throws GameActionException{
        if (connected) {
            return;
        }

        int[] bots = manager.bots[RobotType.BEAVER.ordinal()];
        int size = manager.sizes[RobotType.BEAVER.ordinal()];
        RobotController rc = this.rc;
        for (int i = size - 1; i >= 0; i--) {
            int id = bots[i];

            if (taken[id] || !rc.canSenseRobot(id)) {
                continue;
            }

            taken[i] = true;
            connected = true;
            this.id = id;
            in = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
            out = new RobotMessager(c, id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
            in.broadcastMsg(message, loc);
            return;
        }
    }

    @Override
    public void close() throws GameActionException {
        in.broadcastMsg(MessageType.STOP_ACTION, rc.getLocation());
        out.clearChannel();
        done = true;
    }
}
