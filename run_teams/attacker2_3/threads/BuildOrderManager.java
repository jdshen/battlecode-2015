package attacker2_3.threads;

import battlecode.common.*;
import attacker2_3.OtherGameConstants;
import attacker2_3.controllers.Controller;

public class BuildOrderManager {
    private final Controller c;
    private final RobotController rc;
    private final ObjectiveManager obj;
    private final SpawnThreadManager spawns;

    private ObjectiveThread mainThread;
    private BuildingLocator locator;
    private boolean[] taken;

    private GeneralMicro micro;
    public RobotInfoManager allies;

    // launcher main line
    private static final RobotType[] mainLine = new RobotType[]{
        RobotType.MINERFACTORY,
        RobotType.HELIPAD,
        RobotType.AEROSPACELAB,
        RobotType.AEROSPACELAB,
        RobotType.SUPPLYDEPOT,
    };

    private int mainLineIndex;

    public BuildThread aerospaceThread;
    public BuildThread supplyThread;

    public BuildOrderManager(Controller c, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;
        this.allies = allies;

        this.obj = new ObjectiveManager(c);
        this.locator = new BuildingLocator(c);
        this.taken = new boolean[OtherGameConstants.ROBOT_MAX_ID];
        mainLineIndex = 0;
        this.micro = new GeneralMicro(c, allies);
        this.spawns = new SpawnThreadManager(c, obj, allies);

        RobotType type = mainLine[mainLineIndex];
        MapLocation loc = locator.next(type);
        mainThread = new BuildThread(c, loc, type, allies);
        obj.add(mainThread);
    }

    public void run() throws GameActionException {
        allies.clearBots();

        obj.run(taken);

        // build threads
        if (mainLineIndex < mainLine.length) {
            if (mainThread.isSuccess()) {
                mainLineIndex++;
            }

            if (mainLineIndex < mainLine.length) {
                if (mainThread.isDone()) {
                    RobotType type = mainLine[mainLineIndex];
                    MapLocation loc = locator.next(type);
                    mainThread = new BuildThread(c, loc, type, allies);
                    obj.add(mainThread);
                }
            } else {
                initDynamicBuild();
            }
        } else {
            dynamicBuild();
        }

        // set num threads, called after obj.run
        spawns.setNumThread(RobotType.MINER, allies.sizes[RobotType.MINERFACTORY.ordinal()]);
        spawns.setNumThread(RobotType.LAUNCHER, allies.sizes[RobotType.AEROSPACELAB.ordinal()]);

        micro.run(taken);
    }

    // first call
    public void initDynamicBuild() {
        // Aerospace thread
        {
            RobotType type = RobotType.AEROSPACELAB;
            MapLocation loc = locator.next(type);
            aerospaceThread = new BuildThread(c, loc, type, allies);
            obj.add(aerospaceThread);
        }

        // Supply thread
        {
            RobotType type = RobotType.SUPPLYDEPOT;
            MapLocation loc = locator.next(type);
            supplyThread = new BuildThread(c, loc, type, allies);
            obj.add(supplyThread);
        }
    }

    public void dynamicBuild() {
        if (aerospaceThread.isDone()) {
            RobotType type = RobotType.AEROSPACELAB;
            MapLocation loc = locator.next(type);
            aerospaceThread = new BuildThread(c, loc, type, allies);
            obj.add(aerospaceThread);
        }

        if (supplyThread == null || supplyThread.isDone()) {
            supplyThread = null;
            // check supply
            RobotType[] types = RobotType.values();
            int[] sizes = allies.sizes;
            int supply = 0;
            for (int i = types.length - 1; i >= 0; i--) {
                RobotType type = types[i];
                int size = sizes[i];
                supply += size * type.supplyUpkeep;
            }

            int depots = sizes[RobotType.SUPPLYDEPOT.ordinal()];
            int curSupply;
            if (depots < OtherGameConstants.SUPPLY_LOOKUP.length) {
                curSupply = OtherGameConstants.SUPPLY_LOOKUP[depots] + OtherGameConstants.SUPPLY_BASE;
            } else {
                curSupply =
                    (int) (Math.pow(depots, GameConstants.SUPPLY_GEN_EXPONENT) * GameConstants.SUPPLY_GEN_BASE
                        + OtherGameConstants.SUPPLY_BASE);
            }

            // TODO 3 / 5  = MAGIC CONSTANT
            if (supply * 5  < curSupply * 3) {
                RobotType type = RobotType.SUPPLYDEPOT;
                MapLocation loc = locator.next(type);
                supplyThread = new BuildThread(c, loc, type, allies);
                obj.add(supplyThread);
            }
        }
    }

}
