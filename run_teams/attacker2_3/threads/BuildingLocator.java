package attacker2_3.threads;

import battlecode.common.*;
import attacker2_3.controllers.Controller;

/**
 * Created by jdshen on 1/10/15.
 */
public class BuildingLocator {
    private final RobotController rc;
    private final Controller c;
    private final MapLocation here;

    private static final int[] X_LOCS = new int[] {
        -3, -3, -3, -3, -3, -2, -1, 0, 1, 2, 3, 3, 3, 3, 3, 2, 1, 0, -1, -2,
        2, 1, 0, -1, -2, -2, -2, -2, -2, -1, 0, 1, 2, 2, 2, 2,
    };

    private static final int[] Y_LOCS = new int[] {
        -2, -1, 0, 1, 2, 3, 3, 3, 3, 3, 2, 1, 0, -1, -2, -3, -3, -3, -3, -3,
        2, 2, 2, 2, 2, 1, 0, -1, -2, -2, -2, -2, -2, -1, 0, 1,
    };

    private int currentLoc;

    public BuildingLocator(Controller c) {
        this.c = c;
        this.rc = c.rc;
        this.here = rc.getLocation();
        currentLoc = 0;
    }

    public MapLocation next(RobotType type) {
        RobotController rc = this.rc;
        int[] X_LOCS = BuildingLocator.X_LOCS;
        int[] Y_LOCS = BuildingLocator.Y_LOCS;
        int inc = 1;
        for (int i = (currentLoc + inc) % X_LOCS.length; i != currentLoc; i = (i + inc) % X_LOCS.length) {
            MapLocation loc = here.add(X_LOCS[i], Y_LOCS[i]);
            if (rc.isPathable(RobotType.BEAVER, loc)) {
                currentLoc = i;
                return loc;
            }
        }

        return null;
    }
}
