package attacker2_3.threads;

import battlecode.common.*;
import attacker2_3.OtherGameConstants;
import attacker2_3.controllers.Controller;

/**
 * Created by jdshen on 1/14/15.
 */
public class RobotInfoManager {
    private final Controller c;
    private final RobotController rc;
    public int[][] bots; // bot ids, indexed by types
    public int[] sizes; // current sizes of bots
    public int[] totals; // lifetime totals of bots
    public boolean[] hash;
    public static final int length = RobotType.values().length;
    public int type;

    public RobotInfoManager(Controller c) {
        bots = new int[length][OtherGameConstants.MAX_UNITS_TYPE];
        sizes = new int[length];
        totals = new int[length];
        hash = new boolean[OtherGameConstants.ROBOT_MAX_ID];
        this.c = c;
        this.rc = c.rc;
        type = 0;
    }

    public void addSpawnedBots(MapLocation loc) {
        RobotInfo[] infos = rc.senseNearbyRobots(loc, 2, c.team);
        for (int i = infos.length - 1; i >= 0; i--) {
            RobotInfo info = infos[i];
            if (!hash[info.ID] && !info.type.isBuilding) {
                addBot(info.type, info.ID);
            }
        }
    }

    public void addHQSpawnedBots() {
        RobotInfo[] infos = rc.senseNearbyRobots(c.teamhq, 8, c.team);
        for (int i = infos.length - 1; i >= 0; i--) {
            RobotInfo info = infos[i];
            if (!hash[info.ID] && !info.type.isBuilding) {
                addBot(info.type, info.ID);
            }
        }
    }

    public void addBot(RobotType type, int id) {
        int i = type.ordinal();
        bots[i][sizes[i]++] = id;
        hash[id] = true;
        totals[i]++;
    }

    public void clearBots() {
        // Clear on a rotating basis
        if (Clock.getRoundNum() % 2 != 0) {
            return; // skip a turn
        }

        int i = type;
        boolean[] hash = this.hash;
        int[][] bots = this.bots;
        RobotController rc = this.rc;

        int size = sizes[i];

        int[] newBots = bots[i];
        int newSize = 0;

        for (int j = 0; j < size; j++) {
            int id = newBots[j];

            if (rc.canSenseRobot(id)) {
                // add the bot in
                newBots[newSize++] = id;
            } else {
                // clear the bot's hash
                hash[id] = false;
            }
        }

        // store new bots
        sizes[i] = newSize;

        type = (type + 1) % length;
    }
}
