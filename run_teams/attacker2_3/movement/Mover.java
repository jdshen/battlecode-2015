package attacker2_3.movement;

import battlecode.common.*;
import attacker2_3.OtherGameConstants;
import attacker2_3.controllers.Controller;
import attacker2_3.util.AttackerLookups;

/**
 * Helper class for moving. Contains no state (except RobotController/Controller)
 * @author Jeffrey
 *
 */
public class Mover {
	public RobotController rc;
	public Controller c;

    public AttackerLookups lookup;

    private boolean lastMoveWasClockwise = true;

    public Mover (Controller c) {
		this.c = c;
		this.rc = c.rc;
        this.lookup = new AttackerLookups();
	}

    public boolean[] getCanMove(boolean avoidTowers, MapLocation towerLoc, boolean avoidHQ){
        Direction[] intToDirs = Direction.values();
        int numDirs = intToDirs.length;
        boolean[] canMoves = new boolean[numDirs];

        for (int i = numDirs - 1; i >= 0; i--){
            if(rc.canMove(intToDirs[i])){
                canMoves[i] = true;
            }
        }

        if(!avoidTowers){
            return canMoves;
        }

        MapLocation here = rc.getLocation();
        Direction dir;
        MapLocation tmpLoc;

        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for (int i = numTowers - 1; i >= 0; i--) {
            tmpLoc = towers[i];
            if (here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE) {
                if (tmpLoc.equals(towerLoc)) {
                    continue;
                }
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers++;
            }
        }

        for (int i = numDirs - 1; i >= 0; i--) {
            dir = intToDirs[i];
            if(!canMoves[i]){
                continue;
            }
            tmpLoc = here.add(dir);
            if(avoidHQ){
                switch (towers.length) {
                    case 6:
                    case 5:
                        int thereX = c.enemyhq.x;
                        int thereY = c.enemyhq.y;
                        int delX = tmpLoc.x - thereX;
                        if (delX < 0) {
                            delX = -delX;
                        }
                        int delY = tmpLoc.y - thereY;
                        if (delY < 0) {
                            delY = -delY;
                        }
                        if (delX < 7 && delY < 7 && (delX + delY) < 11) {
                            canMoves[i] = false;
                            continue;
                        }
                        break; // technically out of sight range of the hq at this point so. ...
                    case 4:
                    case 3:
                    case 2:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                            canMoves[i] = false;
                            continue;
                        }
                        break;
                    default:
                        if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                            canMoves[i] = false;
                            continue;
                        }
                }
            }
            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if(tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    canMoves[i] = false;
                    break;
                }
            }
        }
        return canMoves;
    }

    /**
     * Edit canMove so that it takes into account enemy range, keep tank attack radius distance
     */
    public void avoidEnemies(boolean[] canMove) {
        // check enemy ranges
        RobotInfo[] enemies = rc.senseNearbyRobots(OtherGameConstants.TANK_SPLASH_RADIUS_SQ, c.enemy);
        int ignoreRadius = OtherGameConstants.TANK_UNSPLASH_RADIUS_SQ;
        int numEnemies = enemies.length;
        MapLocation here = rc.getLocation();
        AttackerLookups lookup = this.lookup;

        for (int i = numEnemies - 1; i >= 0; i--) {
            RobotInfo en = enemies[i];
            MapLocation there = en.location;
            int attackRadius = en.type.attackRadiusSquared;
            if (attackRadius <= ignoreRadius) {
                continue;
            }
            Direction[] attacked = lookup.getDirs(attackRadius, there.x - here.x, there.y - here.y);

            for (int j = attacked.length - 1; j >= 0; j--) {
                canMove[attacked[j].ordinal()] = false;
            }
        }
    }

    public boolean makeMove(Direction dir, boolean[] canMove) throws GameActionException {
        if (canMove[dir.ordinal()]) {
            rc.move(dir);
            return true;
        }

        Direction left = dir.rotateLeft();
        if (canMove[left.ordinal()]) {
            rc.move(left);
            return true;
        }

        Direction right = dir.rotateRight();
        if (canMove[right.ordinal()]) {
            rc.move(right);
            return true;
        }

        return false;
    }

    // defaults to rc.canMove
	public Direction makeMove(Direction dir) {
		if (rc.canMove(dir)) {
			return dir;
		}
		Direction left = dir.rotateLeft();
		if (rc.canMove(left)) {
			return left;
		}

		Direction right = dir.rotateRight();
		if (rc.canMove(right)) {
			return right;
		}
		return Direction.NONE;
	}

    /**
     * Makes a move in a direction, consulting an array of enemy range coutns
     * @param dir direction to move in
     * @param moveOptions int array of whether you can move in that direction
     * @param extended whether to include perpendicular directions
     * @return
     * @throws GameActionException
     */
    public boolean makeSafeMove(Direction dir, boolean[] moveOptions, boolean extended) throws GameActionException {
        int idx = dir.ordinal();
        if (moveOptions[idx]) {
            rc.move(dir);
            return true;
        }

        Direction first;
        Direction second;
        Direction left = dir.rotateLeft();
        Direction right = dir.rotateRight();
        if(lastMoveWasClockwise){
            first = right;
            second = left;
        }
        else{
            first = left;
            second = right;
        }
        idx = first.ordinal();
        if (moveOptions[idx]) {
            rc.move(first);
            return true;
        }

        idx = second.ordinal();
        if (moveOptions[idx]) {
            rc.move(second);
            lastMoveWasClockwise = !lastMoveWasClockwise;
            return true;
        }
        if(!extended) {
            return false;
        }

        //try going perpendicular and "away" from target (go around)
        if(lastMoveWasClockwise){
            first = right.rotateRight();
            second = left.rotateLeft();
        }
        else{
            first = left.rotateLeft();
            second = right.rotateRight();
        }

        idx = first.ordinal();
        if(moveOptions[idx]){
            rc.move(first);
            return true;
        }
        idx = second.ordinal();
        if(moveOptions[idx]){
            rc.move(second);
            lastMoveWasClockwise = !lastMoveWasClockwise;
            return true;
        }

        return false;
    }
}
