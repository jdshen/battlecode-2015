package attacker2_3.controllers;

import battlecode.common.*;
import attacker2_3.actors.Action;

/**
 * Created by jdshen on 1/6/15.
 */
public class MissileController {

    private RobotController rc;
    private Team enemy;
    private Team team;


    public MissileController(RobotController rc) {
        this.rc = rc;
        this.team = rc.getTeam();
        this.enemy = team.opponent();
    }

    public void run() throws GameActionException {
        boolean move = rc.isCoreReady();

        if(!move){
            return;
        }
        MapLocation here = rc.getLocation();

        RobotInfo[] enemies = rc.senseNearbyRobots(24, enemy);
        int numEnemies = enemies.length;
        Direction bestDir = Direction.OMNI;
        // no enemies in sight
        if(numEnemies == 0){
            RobotInfo[] allies = rc.senseNearbyRobots(8, team);
            if(allies.length == 0){
                return;
            }
            for (int i = allies.length - 1; i >= 0; i--){
                bestDir = allies[i].location.directionTo(here);
                if(allies[i].type == RobotType.LAUNCHER){
                    makeMove(bestDir);
                    return;
                }
            }
            makeMove(bestDir);
            return;
        }

        int adjCount = 0;
        MapLocation there;
        int bestDistance = 30;
        int currentDistance;
        for (int i = numEnemies - 1; i >= 0; i--) {
            there = enemies[i].location;
            if(enemies[i].type == RobotType.MISSILE){
                if(bestDir == Direction.OMNI){
                    bestDir = here.directionTo(there);
                }
                continue;
            }
            currentDistance = here.distanceSquaredTo(there);
            if(currentDistance < bestDistance){
                bestDistance = currentDistance;
                bestDir = here.directionTo(there);
            }
            if(currentDistance <= 2){
                adjCount += 3;
            }
        }

        //Make explode decisionss
        if(adjCount > 0){
            RobotInfo[] allies = rc.senseNearbyRobots(2, team);
            int numAllies = allies.length * 3;
            for (int i = allies.length - 1; i >= 0; i--) {
                if(allies[i].type == RobotType.MISSILE){
                    numAllies --;
                }
            }
            if (numAllies < adjCount){
                rc.explode();
                return;
            }
        }

        makeMove(bestDir);
    }

    public void makeMove(Direction dir) throws GameActionException {
        if (rc.canMove(dir)) {
            rc.move(dir);
            return;
        }

        Direction left = dir.rotateLeft();
        if (rc.canMove(left)) {
            rc.move(left);
            return;
        }

        Direction right = dir.rotateRight();
        if (rc.canMove(right)) {
            rc.move(right);
            return;
        }
        if(!dir.isDiagonal()){
            left = left.rotateLeft();
            if (rc.canMove(left)) {
                rc.move(left);
                return;
            }
            right = right.rotateRight();
            if (rc.canMove(right)) {
                rc.move(right);
                return;
            }
        }
    }
}
