package attacker2_3.controllers;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import attacker2_3.actors.*;
import attacker2_3.threads.BuildOrderManager;
import attacker2_3.threads.RobotInfoManager;

public class HQController extends Controller {
    private Actor supplyActor;
    private Spawner spawner;
    private HQAttacker attacker;
    private BuildOrderManager objectives;
    private RobotInfoManager allies;

    private boolean beaverTracker = false;

    public HQController(RobotController rc) {
        super(rc);
        allies  = new RobotInfoManager(this);
        spawner = new Spawner(this, RobotType.BEAVER, 3);
        attacker = new HQAttacker(this);
        objectives = new BuildOrderManager(this, allies);
        supplyActor = new StructureSupplyActor(this, 9000); // don't go over on computation
    }

    @Override
    public void run() throws GameActionException {
        RobotController rc = this.rc;
        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();

        if (beaverTracker) {
            allies.addHQSpawnedBots();
            beaverTracker = false;
        }
        objectives.run();

        Action action = spawner.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        if (action.moved) {
            beaverTracker = true;
        }

        action = attacker.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;

        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
