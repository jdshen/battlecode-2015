package attacker2_3.actors;

import battlecode.common.*;
import attacker2_3.OtherGameConstants;
import attacker2_3.controllers.Controller;
import attacker2_3.messaging.MessageType;
import attacker2_3.movement.Bugger;

public class Miner extends Actor {
    public double fraction;
    public double maxOre;
    //MAGIC NUMBER - How many times better the mining rate has to be to move
    private final double MINING_IMPROVEMENT_FACTOR = 4;

    //MAGIC NUMBER - number of turns to not move in a retreat direction after seeing an enemy
    private final int COWARDICE = 4;

    // stuff for retreating
    public int seenEnemyCoolDown = 0;
    public Direction lastEnemyDir;
    private Direction[] intToDirs = Direction.values();

    // for bugging
    private boolean updated = false;
    private double targetOreRate;
    private boolean bugging;
    public Bugger bugger;

    public Miner(Controller c) {
        super(c);
        this.bugger = new Bugger(c);
        if (c.type == RobotType.BEAVER) {
            fraction = 1.0 / GameConstants.BEAVER_MINE_RATE;
            maxOre = GameConstants.BEAVER_MINE_MAX;
        } else {
            fraction = 1.0/ GameConstants.MINER_MINE_RATE;
            maxOre = GameConstants.MINER_MINE_MAX;
        }
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        return new Action(makeMove(move), false);
    }

    public boolean makeMove(boolean move) throws GameActionException {
        MapLocation here = rc.getLocation();

        RobotController rc = this.rc;
        Controller c = this.c;

        RobotInfo[] enemies = rc.senseNearbyRobots(c.sensorRadiusSquared, c.enemy);
        if (!move) {
            //Look farther out for resource targets on off turns
            // Detect enemies
            if (enemies.length != 0) {
                updateLastEnemyDir(enemies);
                updated = false;
                return false; //don't bother bugging when enemy is around
            }
            if(!updated) {
                updateBug();
                updated = true;
            }
            return false;
        }
        updated = false;

        boolean[] canMove = getCanMove();
        //If enemies, try to run away
        if (enemies.length != 0) {
            updateLastEnemyDir(enemies);
            if(makeSafeMove(lastEnemyDir.opposite(), canMove)){
                bugging = false;
                return true;
            }
            //GIVE UP AND MINE
            rc.mine();
            return true;
        }
        seenEnemyCoolDown = seenEnemyCoolDown - 1;

        //Check towers and hq range and canMoves

        // Mining/movement behavior
        Direction bestDir = Direction.OMNI;
        double bestOre = rc.senseOre(here);
        double bestRate = bestOre*fraction*MINING_IMPROVEMENT_FACTOR;

        if (bestRate  > maxOre) { // contains the improvement factor to discourage moving
            rc.mine();
            return true;
        }

        // search for higher rates
        // MAGIC CONSTANT = 5
        Direction next;
        for (Direction dir = Direction.NORTH; (next = dir.rotateRight()) != Direction.NORTH; dir = next) {
            if(seenEnemyCoolDown > 0){
                if(dir == lastEnemyDir){
                    continue;
                }
                if(dir == lastEnemyDir.rotateLeft()){
                    continue;
                }
                if(dir == lastEnemyDir.rotateRight()){
                    continue;
                }
            }
            MapLocation loc = here.add(dir);
            if (canMove[dir.ordinal()]) {
                double curOre = rc.senseOre(loc);
                double curRate = curOre *fraction;
                if(curRate > maxOre){
                    bestDir = dir;
                    break;
                }
                if (curRate > bestRate) {
                    bestDir = dir;
                    bestRate = curRate;
                }
            }
        }
        // We were bugging. Keep bugging unless an adjacent place is good
        if(bugging){
            // stop bugging if we found something better, or close enough
            if(bestRate > targetOreRate || here.isAdjacentTo(bugger.to)){
                bugging = false;
                bugger.endBug();
                if(bestDir == Direction.OMNI) {
                    rc.mine();
                    return true;
                }
                //checked canMove earlier?
                rc.move(bestDir);
                return true;
            }
            // get move directions
            bestDir = bugger.bug(canMove);
            if(rc.canMove(bestDir)){
                rc.move(bestDir);
                return true;
            }
            rc.mine();
            return true;
        }

        Direction dir = bestDir;
        if (dir == Direction.OMNI) {
            if (rc.senseOre(rc.getLocation()) < 0.01) {
                dir = c.teamhq.directionTo(c.enemyhq);
                for (int i = 7; i >= 0; i--) {
                    if (rc.canMove(dir)) {
                        rc.move(dir);
                        return true;
                    }
                    dir = dir.rotateLeft();
                }
            }
            rc.mine();
            return true;
        } else if (rc.canMove(dir)) {
            rc.move(dir);
            return true;
        } else {
            if (rc.senseOre(rc.getLocation()) < 0.01) {
                dir = c.teamhq.directionTo(c.enemyhq);
                for (int i = 7; i >= 0; i--) {
                    if (rc.canMove(dir)) {
                        rc.move(dir);
                        return true;
                    }
                    dir = dir.rotateLeft();
                }
            }
            rc.mine();
            return true;
        }
    }

    //Check to see if we should start bugging to some location and do so
    public void updateBug() throws GameActionException {
        //exit if already bugging
        if(bugging){
            return;
        }
        //check nearby ore and see if bugging farther out is worth
        MapLocation here = rc.getLocation();
        double bestOre = rc.senseOre(here);
        double bestRate = bestOre*fraction*MINING_IMPROVEMENT_FACTOR;

        Direction next;
        boolean shouldBug = false;
        MapLocation bugLocation = here;
        for (Direction dir = Direction.NORTH; (next = dir.rotateRight()) != Direction.NORTH; dir = next) {
            if(seenEnemyCoolDown > 0){
                if(dir == lastEnemyDir){
                    continue;
                }
                if(dir == lastEnemyDir.rotateLeft()){
                    continue;
                }
                if(dir == lastEnemyDir.rotateRight()){
                    continue;
                }
            }
            MapLocation loc = here.add(dir);
            double curOre = rc.senseOre(loc);
            double curRate = curOre *fraction;
            if(curRate > maxOre){
                shouldBug = false;
                break;
            }
            if (curRate > bestRate) {
                bestRate = curRate;
                shouldBug = false;
            }

            loc = loc.add(dir);
            if (rc.senseRobotAtLocation(loc) == null) {
                curOre = rc.senseOre(loc);
                curRate = curOre * fraction;
                if(curRate > maxOre){
                    curRate = maxOre;
                }
                curRate = curRate/MINING_IMPROVEMENT_FACTOR;
                if (curRate > bestRate) {
                    shouldBug = true;
                    bugLocation = loc;
                    bestRate = curRate;
                    bestOre = curOre;
                }
            }
            loc = loc.add(dir);
            if (rc.senseRobotAtLocation(loc) == null) {
                curOre = rc.senseOre(loc);
                curRate = curOre * fraction;
                if(curRate > maxOre){
                    curRate = maxOre;
                }
                curRate = curRate/MINING_IMPROVEMENT_FACTOR/MINING_IMPROVEMENT_FACTOR;
                if (curRate > bestRate) {
                    shouldBug = true;
                    bugLocation = loc;
                    bestRate = curRate;
                    bestOre = curOre;
                }
            }
        }

        if(shouldBug){ //if we should bug, start bugging
            targetOreRate = bestOre*fraction;
            if(targetOreRate > maxOre){
                targetOreRate = maxOre;
            }
            targetOreRate = targetOreRate / MINING_IMPROVEMENT_FACTOR;
            bugging = true;
            bugger.startBug(bugLocation);
        }
    }

    public void updateLastEnemyDir(RobotInfo[] enemies){
        MapLocation here = rc.getLocation();
        seenEnemyCoolDown = COWARDICE;
        int dirsLength = intToDirs.length;
        int[] moveOptions = new int[dirsLength];
        Direction dir = Direction.OMNI;
        int idx;
        for (int i = enemies.length -1; i >= 0; i--) {
            dir = here.directionTo(enemies[i].location);
            idx = dir.ordinal();
            moveOptions[idx] = moveOptions[idx] + 3;
            idx = dir.rotateLeft().ordinal();
            moveOptions[idx] = moveOptions[idx] + 2;
            idx = dir.rotateRight().ordinal();
            moveOptions[idx] = moveOptions[idx] + 2;
        }
        idx = 0;
        for (int i = dirsLength - 1; i >= 0; i--){
            if(moveOptions[i] > idx){
                idx = moveOptions[i];
                dir = intToDirs[i];
            }
        }
        lastEnemyDir = dir;
    }

    public boolean[] getCanMove(){
        MapLocation here = rc.getLocation();
        int dirsLength = intToDirs.length;
        MapLocation tmpLoc;
        MapLocation[] towers = rc.senseEnemyTowerLocations();
        int numTowers = towers.length;
        MapLocation[] needCheckTowers = new MapLocation[numTowers];
        int numCheckTowers = 0;
        for(int i = numTowers - 1; i >= 0; i--){
            tmpLoc = towers[i];
            if(here.distanceSquaredTo(tmpLoc) <= OtherGameConstants.TOWER_SPLASH_RANGE){
                needCheckTowers[numCheckTowers] = tmpLoc;
                numCheckTowers ++;
            }
        }
        boolean[] canMove = new boolean[dirsLength];

        for(int i = dirsLength - 1; i >= 0; i--){
            Direction dir = intToDirs[i];
            if(!rc.canMove(dir)){
                continue;
            }
            if(seenEnemyCoolDown > 0){ //enemy cd
                if(dir == lastEnemyDir){
                    continue;
                }
                if(dir == lastEnemyDir.rotateLeft()){
                    continue;
                }
                if(dir == lastEnemyDir.rotateRight()){
                    continue;
                }
            }
            tmpLoc = here.add(dir);
            switch (numTowers) {
                case 6:
                case 5:
                    int thereX = c.enemyhq.x;
                    int thereY = c.enemyhq.y;
                    int delX = tmpLoc.x - thereX;
                    if(delX < 0){
                        delX = -delX;
                    }
                    int delY = tmpLoc.y - thereY;
                    if(delY < 0){
                        delY = -delY;
                    }
                    if(delX < 7 && delY < 7 && (delX + delY) < 11){
                        continue;
                    }
                    break; // technically out of sight range of the hq at this point so. ...
                case 4:
                case 3:
                case 2:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 36) {
                        continue;
                    }
                    break;
                default:
                    if (tmpLoc.distanceSquaredTo(c.enemyhq) < 25) {
                        continue;
                    }
            }
            canMove[i] = true;
            for (int j = numCheckTowers - 1; j >= 0; j--) {
                if (tmpLoc.distanceSquaredTo(needCheckTowers[j]) < 25) {
                    canMove[i] = false;
                    break;
                }
            }
        }
        return canMove;
    }

    public boolean makeSafeMove(Direction dir, boolean[] canMove) throws GameActionException {
        if (canMove[dir.ordinal()]) {
            rc.move(dir);
            return true;
        }

        Direction left = dir.rotateLeft();
        if (canMove[left.ordinal()]) {
            rc.move(left);
            return true;
        }

        Direction right = dir.rotateRight();
        if (canMove[right.ordinal()]) {
            rc.move(right);
            return true;
        }

        return false;
    }
}
