package attacker2_3.actors;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import attacker2_3.controllers.Controller;
import attacker2_3.messaging.MessageType;
import attacker2_3.messaging.MessagingConstants;
import attacker2_3.messaging.RobotMessager;

/**
 * Created by jdshen on 1/9/15.
 */
public class ObjectiveListener extends Actor {
    private Actor actor;
    private MessageType objective;
    private MapLocation objectiveLoc;
    private RobotMessager in;
    private RobotMessager out;

    public ObjectiveListener(Controller c) {
        super(c);
        actor = null;
        objective = null;
        objectiveLoc = null;
        in = new RobotMessager(c, c.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        out = new RobotMessager(c, c.id, MessagingConstants.ID_OFFSET_BOT_TO_GLOBAL);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        // read messages
        if (in.readMsg()) {
            MessageType msg = in.lastMsg;
            MapLocation loc = in.lastLoc;
            rc.setIndicatorString(0, msg + " " + loc);

            if (msg != objective || !(loc.equals(objectiveLoc))) {
                if (msg.isBuildCommand) {
                    actor = new Builder(c, msg.type, loc, out, 1);
                    objective = msg;
                    objectiveLoc = loc;
                } else if (msg.isSpawnCommand) {
                    actor = new Spawner(c, msg.type, out, -1);
                    objective = msg;
                    objectiveLoc = loc;
                } else if (msg == MessageType.STOP_ACTION) {
                    actor = null;
                    objective = null;
                    objectiveLoc = null;
                } else {
                    // ignore msg
                    // TODO what to do when no msg
                }
            }

            if (objective != null && actor != null) {
                Action action = actor.act(move, attack);
                return new Action(true, action.attacked);
            }
        }

        objective = null;
        actor = null;
        return new Action(false, false);
    }
}
