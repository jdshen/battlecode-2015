package attacker2_3.actors;

import battlecode.common.*;
import attacker2_3.controllers.Controller;
import attacker2_3.messaging.KillMessager;

/**
 * Created by kevin on 1/11/15.
 */
public class GenericAttacker extends Actor {

    private KillMessager kill;

    public GenericAttacker(Controller c) {
        super(c);
        kill = new KillMessager(rc);
    }

    @Override
    public Action act(boolean move, boolean attack) throws GameActionException {
        kill.updatePass();
        if(!attack){
            return new Action(false, false);
        }


        //ATTACK LOGIC
        //get enemies in range
        int attackRadius = c.attackRadiusSquared;
        double attackPower = c.attackPower;

        RobotInfo[] enemies = rc.senseNearbyRobots(attackRadius, c.enemy);
        if(enemies.length == 0){
            return new Action(false, false);
        }

        //look for "best" target
        // currently (enemy-hp)/(enemy-dmg-rate) TODO better targetting
        RobotInfo enemy = enemies[0];
        RobotType type = enemy.type;
        RobotInfo bestEnemy = enemy;

        // get number of turns to kill enemy
        int turns = (int) (enemy.health / attackPower + .999);
        // get enemy damage rate
        double damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
        double score = turns/ damageRate + getBonus(enemy);

        for(int i = 1; i < enemies.length; i++){
            enemy = enemies[i];
            type = enemy.type;
            // get number of turns to kill enemy
            turns = (int) (enemy.health / attackPower + .999);
            // get enemy damage rate
            damageRate = (type.attackPower / (0.01+type.attackDelay)) + 1;
            double newScore = turns/damageRate + getBonus(enemy);
            //switch targets
            if(score > newScore) {
                score = newScore;
                bestEnemy = enemy;
            }
        }

        rc.attackLocation(bestEnemy.location);
        if(bestEnemy.health <= attackPower && bestEnemy.type != RobotType.MISSILE){
            kill.broadcastKill(bestEnemy.ID);
        }
        return new Action(false, true);
    }

    public int getBonus(RobotInfo info){
        return 0;
    }
}
