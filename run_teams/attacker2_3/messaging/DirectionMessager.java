package attacker2_3.messaging;

import battlecode.common.*;
import attacker2_3.OtherGameConstants;


/**
 * For posting to / reading from maps with directions to path towards some target location
 *
 */
public class DirectionMessager {

    private RobotController rc;

    public final int locationChannelX;
    public final int locationChannelY;
    public final int offset;

    private static final int HASH = MessagingConstants.MAX_DIM;
    private static final int OFFSET = (OtherGameConstants.MAX_MAP_OFFSET / HASH + 1) * HASH;

    public Direction[] intToDir = new Direction[]{
            null,
            Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
            Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST,
    };
    public final static int[] loMasks = getNines(1);
    public final static int[] hiMasks = getNines(9);

    // helper method for getting array of powers of nine
    public static int[] getNines(int init){
        int[] neins = new int[9];
        int x = init;
        for(int i = 0; i < 9; i ++){
            neins[i] = x;
            x = x*9;
        }
        return neins;
    }


    // initialize a DirectionMessager for a given start channel (see MessagingConstants)
    public DirectionMessager(RobotController rc, int channelStart){
        locationChannelX = channelStart;
        locationChannelY = channelStart + 1;
        offset = channelStart + 2;

        this.rc = rc;
    }

    /**
     * Broadcast onto the location channel the offset from HQ that this map goes to.
     * @param loc broadcast-ready offset from HQ (has no negative vals)
     * @throws GameActionException
     */
    public void broadcastLocation(MapLocation loc) throws GameActionException{
        rc.broadcast(locationChannelX, loc.x);
        rc.broadcast(locationChannelY, loc.y);
    }

    /**
     * Get the location the direction map goes to
     * @return broadcast-ready offset from HQ (has no negative vals)
     * @throws GameActionException
     */
    public MapLocation readLocation() throws GameActionException{
        int x = rc.readBroadcast(locationChannelX);
        int y = rc.readBroadcast(locationChannelY);
        return new MapLocation(x, y);
    }

    /**
     * Post a direction to target location "to" at a loc
     */
    public void broadcastDirection(MapLocation loc, Direction dir) throws GameActionException{
        int x = (loc.x + OFFSET) % HASH;
        int y = (loc.y + OFFSET) % HASH;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*HASH;
        int data = rc.readBroadcast(channel);

        // mask out the other and clear only this bit
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];

        int dirvalue = 0;
        switch(dir){
            case NORTH:
                dirvalue = 1;
                break;
            case NORTH_EAST:
                dirvalue = 2;
                break;
            case EAST:
                dirvalue = 3;
                break;
            case SOUTH_EAST:
                dirvalue = 4;
                break;
            case SOUTH:
                dirvalue = 5;
                break;
            case SOUTH_WEST:
                dirvalue = 6;
                break;
            case WEST:
                dirvalue = 7;
                break;
            case NORTH_WEST:
                dirvalue = 8;
                break;
            default:
                dirvalue = 0;
                break;
        }


        data = data + (-(data % hiMask) / loMask + dirvalue)*loMask;

        rc.broadcast(channel, data);
    }

    /**
     * clear a location
     * @param loc
     * @throws GameActionException
     */
    public void clearDirection(MapLocation loc) throws GameActionException{
        int x = (loc.x + OFFSET) % HASH;
        int y = (loc.y + OFFSET) % HASH;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*HASH;
        int data = rc.readBroadcast(channel);

        // mask out the other and clear only this bit
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];
        data = data + (-(data % hiMask)/loMask)*loMask;

        rc.broadcast(channel, data);
    }


    /**
     * Read Direction posted for a location
     * @param loc
     * @return
     * @throws GameActionException
     */
    public Direction readDirection(MapLocation loc) throws GameActionException{
        int x = (loc.x + OFFSET) % HASH;
        int y = (loc.y + OFFSET) % HASH;
        int yMod = y % 9;
        // calculate which channel holds this loc
        int channel = offset + x + (y/9)*HASH;
        int data = rc.readBroadcast(channel);

        // mask out the other locs and get only this loc
        int hiMask = hiMasks[yMod];
        int loMask = loMasks[yMod];
        data = (data % hiMask)/loMask;

//        if(data == 0){
//            //nothing there
//            return null;
//        }else{
//            data--;
//            return intToDir[data];
//        }
        return intToDir[data];
    }

}
