package attacker3_2.messaging;

import battlecode.common.*;
import attacker3_2.controllers.Controller;

/**
 * Created by kevin on 1/21/15.
 */
public class ScoutingMessager {
    private RobotController rc;

    public final int SCOUT_CHANNEL_START = MessagingConstants.SCOUT_CHANNEL_START;
    public final int BLOCK_SIZE = MessagingConstants.SCOUT_BLOCK_SIZE;
    private final int width = (GameConstants.MAP_MAX_WIDTH+BLOCK_SIZE-1)/BLOCK_SIZE;
    private final int height = (GameConstants.MAP_MAX_HEIGHT+BLOCK_SIZE-1)/BLOCK_SIZE;

    public ScoutingMessager(Controller c){
        this.rc = c.rc;
    }

    // Update that a location is scouted - these values should be modded already
    public void update(int xLoc, int yLoc) throws GameActionException {
        rc.broadcast(SCOUT_CHANNEL_START + xLoc + yLoc*width, Clock.getRoundNum());
    }

    public void forceUpdate(int xLoc, int yLoc, int val) throws GameActionException {
        rc.broadcast(SCOUT_CHANNEL_START + xLoc + yLoc*width, val);
    }

    // input values should be blocked and modded already
    public int getLastScout(int xLoc, int yLoc) throws  GameActionException {
        return rc.readBroadcast(SCOUT_CHANNEL_START + xLoc + yLoc*width);
    }

    public int getLastScout(MapLocation loc) throws GameActionException {

        int xLoc = (loc.x / BLOCK_SIZE) % width;
        int yLoc = (loc.y / BLOCK_SIZE) % height;

        return rc.readBroadcast(SCOUT_CHANNEL_START + xLoc + yLoc*width);
    }
}
