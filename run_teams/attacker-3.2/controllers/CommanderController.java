package attacker3_2.controllers;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.RobotController;
import attacker3_2.actors.Action;
import attacker3_2.actors.CommanderFighter;
import attacker3_2.actors.SupplyActor;
import attacker3_2.messaging.MessagingConstants;
import attacker3_2.messaging.RobotMessager;

/**
 * Created by jdshen on 1/6/15.
 */
public class CommanderController extends Controller {

    private CommanderFighter actor;
    private SupplyActor supplyActor;
    private RobotMessager in;

    public CommanderController(RobotController rc) {
        super(rc);
        in = new RobotMessager(this, this.id, MessagingConstants.ID_OFFSET_GLOBAL_TO_BOT);
        this.actor = new CommanderFighter(this, this.enemyhq);
        this.supplyActor = new SupplyActor(this, GameConstants.FREE_BYTECODES);
    }

    @Override
    public void run() throws GameActionException {
        if (in.readMsg()) {
            actor.setTarget(in.lastLoc);
            switch (in.lastMsg) {
                case HARASS:
                    actor.ignoreTower(this.teamhq);
                    actor.setAvoidTowers(true);
                    actor.setAvoidHQ(true);
                    break;
                case ASSAULT:
                    actor.ignoreTower(in.lastLoc);
                    actor.setAvoidTowers(true);
                    actor.setAvoidHQ(true);
                    break;
                case DESTROY:
                    actor.ignoreTower(this.teamhq);
                    actor.setAvoidTowers(false);
                    actor.setAvoidHQ(false);
                    break;
            }
        }

        boolean move = rc.isCoreReady();
        boolean attack = rc.isWeaponReady();
        Action action = actor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;


        action = supplyActor.act(move, attack);
        move = move && !action.moved;
        attack = attack && !action.moved;
    }
}
