package attacker3_2.computation.symmetry;

import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/23/15.
 */
public interface Symmetry {
    public SymmetryType getType();
    public MapLocation get(MapLocation loc);
    public MapLocation get(int x, int y);
}
