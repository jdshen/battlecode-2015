package attacker3_2.computation.symmetry;

/**
 * Created by jdshen on 1/24/15.
 */
public enum SymmetryType {
    X_REFLECTION, Y_REFLECTION, NEG_DIAGONAL, POS_DIAGONAL, ROTATION
}
