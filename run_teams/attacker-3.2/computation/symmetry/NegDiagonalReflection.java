package attacker3_2.computation.symmetry;

import battlecode.common.MapLocation;

/**
 * Created by jdshen on 1/23/15.
 */
public class NegDiagonalReflection implements Symmetry {
    private int centerXY;

    public NegDiagonalReflection(int centerXY) {
        this.centerXY = centerXY;
    }

    @Override
    public SymmetryType getType() {
        return SymmetryType.NEG_DIAGONAL;
    }

    @Override
    public MapLocation get(MapLocation loc) {
        return new MapLocation(centerXY - loc.y, centerXY - loc.x);
    }

    @Override
    public MapLocation get(int x, int y) {
        return new MapLocation(centerXY - y, centerXY - x);
    }
}
