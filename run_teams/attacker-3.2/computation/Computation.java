package attacker3_2.computation;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import attacker3_2.controllers.Controller;

public abstract class Computation {
    public final Controller c;
    public final RobotController rc;

    public Computation(Controller c) {
        this.c = c;
        this.rc = c.rc;
    }

    public abstract boolean compute(int limit) throws GameActionException;
}
