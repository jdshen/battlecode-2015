package attacker3_2.threads;

import battlecode.common.*;
import attacker3_2.OtherGameConstants;
import attacker3_2.controllers.Controller;
import attacker3_2.messaging.MessageType;

public class BuildOrderManager {
    private final Controller c;
    private final RobotController rc;
    private final ObjectiveManager obj;
    private final SpawnThreadManager spawns;

    private ObjectiveThread mainThread;
    private BuildingLocator locator;
    private boolean[] taken;

    private GeneralMicro micro;
    public RobotInfoManager allies;

    // launcher main line
    private static final RobotType[] mainLine = new RobotType[]{
        RobotType.MINERFACTORY,
        RobotType.HELIPAD,
        RobotType.AEROSPACELAB,
        RobotType.TECHNOLOGYINSTITUTE,
    };

    private int mainLineIndex;

    public BuildThread trainingThread;
    public BuildThread aerospaceThread;
    public BuildThread supplyThread;

    public BuildOrderManager(Controller c, RobotInfoManager allies) {
        this.c = c;
        this.rc = c.rc;
        this.allies = allies;

        this.obj = new ObjectiveManager(c);
        this.locator = new BuildingLocator(c);
        this.taken = new boolean[OtherGameConstants.ROBOT_MAX_ID];
        mainLineIndex = 0;
        this.micro = new GeneralMicro(c, allies);
        this.spawns = new SpawnThreadManager(c, obj, allies);

        RobotType type = mainLine[mainLineIndex];
        MapLocation loc = locator.next(type);
        mainThread = new BuildThread(c, loc, type, allies);
        obj.add(mainThread);
        obj.add(new ComputationThread(c, MessageType.BFS_1, allies));
    }

    public void run() throws GameActionException {
        allies.clearBots();

        obj.run(taken);

        // build threads
        if (mainLineIndex < mainLine.length) {
            if (mainThread.isSuccess()) {
                mainLineIndex++;
            }

            if (mainLineIndex < mainLine.length) {
                if (mainThread.isDone()) {
                    RobotType type = mainLine[mainLineIndex];
                    MapLocation loc = locator.next(type);
                    mainThread = new BuildThread(c, loc, type, allies);
                    obj.add(mainThread);
                }
            } else {
                initDynamicBuild();
            }
        } else {
            dynamicBuild();
        }

        // set num threads, called after obj.run
        boolean scout = micro.harassCommand == MessageType.SCOUT;
        if (scout) {
            if (allies.sizes[RobotType.DRONE.ordinal()] < 3) {
                spawns.setNumThread(RobotType.DRONE, allies.sizes[RobotType.HELIPAD.ordinal()]);
            } else {
                spawns.setNumThread(RobotType.DRONE, 0);
            }
        } else {
            if (allies.sizes[RobotType.DRONE.ordinal()] < 5) {
                spawns.setNumThread(RobotType.DRONE, allies.sizes[RobotType.HELIPAD.ordinal()]);
            } else {
                spawns.setNumThread(RobotType.DRONE, 0);
            }
        }

        spawns.setNumThread(RobotType.MINER, allies.sizes[RobotType.MINERFACTORY.ordinal()]);
        spawns.setNumThread(RobotType.LAUNCHER, allies.sizes[RobotType.AEROSPACELAB.ordinal()]);

//        if (allies.sizes[RobotType.COMMANDER.ordinal()] < 1) {
//            spawns.setNumThread(RobotType.COMMANDER, allies.sizes[RobotType.TRAININGFIELD.ordinal()]);
//        }

        if (allies.sizes[RobotType.COMPUTER.ordinal()] < 1) {
            spawns.setNumThread(RobotType.COMPUTER, allies.sizes[RobotType.TECHNOLOGYINSTITUTE.ordinal()]);
        } else {
            spawns.setNumThread(RobotType.COMPUTER, 0);
        }

        micro.run(taken);
    }

    // first call
    public void initDynamicBuild() {
        // Training thread
//        {
//            RobotType type = RobotType.TRAININGFIELD;
//            MapLocation loc = locator.next(type);
//            trainingThread = new BuildThread(c, loc, type, allies);
//            obj.add(trainingThread);
//        }

        // Aerospace thread
        {
            RobotType type = RobotType.AEROSPACELAB;
            MapLocation loc = locator.next(type);
            aerospaceThread = new BuildThread(c, loc, type, allies);
            obj.add(aerospaceThread);
        }

        // Supply thread
        {
            RobotType type = RobotType.SUPPLYDEPOT;
            MapLocation loc = locator.next(type);
            supplyThread = new BuildThread(c, loc, type, allies);
            obj.add(supplyThread);
        }
    }

    public void dynamicBuild() {
        if (aerospaceThread.isDone()) {
            RobotType type = RobotType.AEROSPACELAB;
            MapLocation loc = locator.next(type);
            aerospaceThread = new BuildThread(c, loc, type, allies);
            obj.add(aerospaceThread);
        }

        if (supplyThread == null || supplyThread.isDone()) {
            supplyThread = null;
            // check supply
            RobotType[] types = RobotType.values();
            int[] sizes = allies.sizes;
            int supply = 0;
            for (int i = types.length - 1; i >= 0; i--) {
                RobotType type = types[i];
                int size = sizes[i];
                supply += size * type.supplyUpkeep;
            }

            int depots = sizes[RobotType.SUPPLYDEPOT.ordinal()];
            int curSupply;
            if (depots < OtherGameConstants.SUPPLY_LOOKUP.length) {
                curSupply = OtherGameConstants.SUPPLY_LOOKUP[depots] + OtherGameConstants.SUPPLY_BASE;
            } else {
                curSupply =
                    (int) (Math.pow(depots, GameConstants.SUPPLY_GEN_EXPONENT) * GameConstants.SUPPLY_GEN_BASE
                        + OtherGameConstants.SUPPLY_BASE);
            }

            rc.setIndicatorString(2, supply + ", " + curSupply);
            // TODO 5 / 7  = MAGIC CONSTANT
            if (supply * 7 > curSupply * 5) {
                RobotType type = RobotType.SUPPLYDEPOT;
                MapLocation loc = locator.next(type);
                supplyThread = new BuildThread(c, loc, type, allies);
                obj.add(supplyThread);
            }
        }
    }

}
