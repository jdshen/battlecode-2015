# Battlecode 2015 - anim0ls #

Here is the code for our 2015 bot, anim0ls. All our snapshots are under run_teams so that intellij wouldn't confuse them for current code. In order to run our old snapshots, use run_build.xml. Some snapshots were not saved because they had bugs, but are still in jar form under /jars.

### Tournament Bots ###

* **harass-2.1:** Our sprint bot - made drones and tanks. The post-sprint release broke our bot, so we decided not to save a version here.
* **attacker-2.3:** Our seeding bot - made launchers. Contains a navigation and a supply bug. 
* **attacker-4.5:** Our final bot - made launchers + drones + commanders. Contains a round limit bug.
* **team027:** Our latest version - contains a micro update that uses broadcasting for missile targeting and a post-deadline fix for our round limit bug. It was never submitted.

### General information ###

* **actors:** Contains all code that performs a move or attack action.
* **computation:** Contains all our computation. See BFS for highly optimized code that finds a fairly accurate shortest path, with the assumption that diagonals are weighted 1.5.
* **controllers:** Contains the top level for each of our unit types.
* **messaging:** Contains all our messaging code. See RobotMessager and CommandMessager for id messaging. We often inlined messaging code for efficiency.
* **movement:** Contains movement helper classes. See Bugger for all of our default navigation.
* **threads:** Contains all our strategy and hq decision code. See BuildOrderManager for the top level strategy, GeneralMicro where we assign attack points and issue commands, RobotInfoManager/EnemyInfoManager for managing allied ids and enemy info, ObjectiveManager where we maintain specific build/spawn/compute threads.
* **util:** All of our general utilities, mostly hash sets. Again, we inlined where convenient.